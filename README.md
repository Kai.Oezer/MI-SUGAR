# MI-SUGAR

![MI-SUGAR logo](https://raw.githubusercontent.com/robo-fish/MI-SUGAR/master/Images.xcassets/about.imageset/about.png)

MI-SUGAR is a simple electronic circuit sketching and analysis app for macOS, written in Objective-C. For information on how to use MI-SUGAR, please visit the [__MI-SUGAR product page__](https://robo.fish/wiki/index.php?title=MI-SUGAR).

## History

The project dates back to the end of 2002. I had just started dabbling in the waters of Cocoa programming and saw an opportunity in the lack of a native circuit design application for Mac OS X. Web technology was awful back then. For real work, you used native apps.

As an inexperienced programmer, I made many architectural and code maintainability mistakes with MI-SUGAR. I had written tons of code already before realizing that creating a subclass for each schematic element type wasn't scaling. Also, the binary file format of MI-SUGAR created a dependency on Apple's Foundation framework, which is a poor choice for an application whose users prefer standardized document formats. I wasn't using a version control tool at that time, making refactoring extremely painful. I, therefore, decided to abandon the project in 2004. Today we have technologies like JSON and Git that almost every programmer learns about early on in their career and that would have prevented me from driving MI-SUGAR into a dead end.

Sometime later I published the source code. In 2018 I brushed up the code a litte and created a repository [on GitHub](https://github.com/robo-fish/MI-SUGAR). In 2021 I moved the project to GitLab and updated the code again so it builds with Xcode 13 and runs on macOS 11.

At the end of 2007 I started working on [Volta](https://github.com/robo-fish/Volta), the successor of MI-SUGAR. I recommend using Volta instead of MI-SUGAR. Occasional updates to MI-SUGAR are only because of Objective-C nostalgia.

## Build Dependencies

### libxml2

libxml2 is used to parse subcircuit shape definitions. The header and library search paths set for libxml2 assume that you have installed libxml2 via [Homebrew](https://brew.sh).

## Circuit Simulator

The underlying circuit simulator is a modified version of SPICE 3F5, which I made to build and run on Mac OS X. This binary is built for PowerPC and 32-bit Intel Macs. Without an emulator it won't run on today's 64-bit Intel Macs or Apple Silicon Macs.

## Subcircuits

The folder 'Subcircuits' contains example subcircuit files. To use them, you need to copy them to the "robo.fish.mi-sugar/Subcircuits" folder in your _Application Support_ folder, as shown:

```bash
mkdir "${HOME}/Application Support/robo.fish.mi-sugar"
cp -R MI-SUGAR/Subcircuits "${HOME}/Application Support/robo.fish.mi-sugar/"
```

## Note On The File Format

One of the major flaws of MI-SUGAR is the binary file format that requires using the Foundation framework.

## Note On Coding Style

Indentation is done with spaces, not tabs, because this works better with multi-line Objective-C message calls.

## Copyright and License

The copyrights for all source code and images in this repository belong to Kai Oezer. The source code is provided under the terms of the GNU General Public License version [GNU General Public License version 2](https://www.gnu.org/licenses/gpl-2.0.html).

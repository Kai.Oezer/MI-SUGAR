/**
*
*   Copyright Kai Özer, 2003-2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import "MI_TextView.h"

@implementation MI_TextView
{
  IBOutlet MI_TextViewLineNumbering* lineNumberingView;
}

- (instancetype) init
{
    if (self = [super init])
        self.dropHandler = nil;
    return self;
}

- (void) dealloc
{
  [self unregisterDraggedTypes];
}

- (void) awakeFromNib
{
	[self registerForDraggedTypes:[self readablePasteboardTypes]];
	//NSLog([[self readablePasteboardTypes] description]);
}

- (MI_TextViewLineNumbering*) lineNumberingView
{
    return lineNumberingView;
}

- (void) drawRect:(NSRect)rect
{
    [lineNumberingView setNeedsDisplay:YES];
    [super drawRect:rect];
}

- (NSArray<NSPasteboardType>*) readablePasteboardTypes
{
	return @[NSPasteboardTypeString, NSPasteboardTypeFileURL];
}

- (void) mouseDown:(NSEvent*)theEvent
{
    [[self window] makeFirstResponder:self];
    [super mouseDown:theEvent];
}    

// MARK: NSDraggingDestination conformance

- (BOOL) prepareForDragOperation:(id <NSDraggingInfo>)sender
{
    //NSLog(@"prepare for drag");
    return YES;
}

- (BOOL) performDragOperation:(id <NSDraggingInfo>)sender
{
    //NSLog(@"perform drag op");
    [self.dropHandler processDrop:sender];
    return YES;
}

- (void) concludeDragOperation:(id <NSDraggingInfo>)sender
{
    //NSLog(@"dragging concluded");
}

- (NSDragOperation) dragOperationForDraggingInfo:(id <NSDraggingInfo>)dragInfo
                                            type:(NSString*)type
{
  NSPasteboard *pboard = [dragInfo draggingPasteboard];
  if ([[pboard types] indexOfObject:NSPasteboardTypeFileURL] == NSNotFound)
    return NSDragOperationNone;
  else
    return NSDragOperationCopy;
}

- (NSDragOperation) draggingEntered:(id <NSDraggingInfo>)sender
{
  NSPasteboard *pboard = [sender draggingPasteboard];
  if ([[pboard types] indexOfObject:NSPasteboardTypeFileURL] == NSNotFound)
    return NSDragOperationNone;
  else
    return NSDragOperationLink;
}

- (void) draggingExited:(id <NSDraggingInfo>)sender
{
    //NSLog(@"dragging exited");
}

@end

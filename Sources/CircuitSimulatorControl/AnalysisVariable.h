/**
*
*   Copyright Kai Özer, 2003-2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import <Foundation/Foundation.h>
#import "ComplexNumber.h"

NS_ASSUME_NONNULL_BEGIN

/// Wraps the data for one analysis variable parsed from a simulator output.
///
/// Analysis variables may hold multiple arrays of values and, therefore,
/// can also be used when the circuit analysis type is DC and there is
/// a second sweep variable.
@interface AnalysisVariable : NSObject

- (instancetype) initWithName:(NSString*)varName;

- (long) numberOfSets; // typically this is one (1)

- (long) numberOfValuesPerSet; // the length of the equally-large value arrays

- (NSString*) name;

@property double scaleFactor;

///  If this AnalysisVariable contains ``ComplexNumber`` values,
///  the returned value depends on ``floatRepresentation``.
- (double) averageValue;

- (void) calculateAverageValue;	// call only after all values are set

- (void) findMinMax;	// find the maximum and minimum of all values of all sets. call only after all values are set.

- (double) maximum; // return maximum of all values of all sets

- (double) minimum; // return minimum of all values of all sets

- (double) realMinimum; // return minimum of real part

- (double) realMaximum; // return maximum of real part

- (double) imaginaryMinimum; // return minimum of imaginary part

- (double) imaginaryMaximum; // return maximum of imaginary part

@property (getter=isScalingAroundAverage) BOOL scalingAroundAverage;

- (int) type;

@property (readonly) BOOL isComplex; ///< whether the values are complex numbers

@property ComplexNumberFloatRepresentation floatRepresentation;

/// Convenience method that internally calls `addValue:toSet:`
- (void) addDoubleValue:(double)value
                  toSet:(NSUInteger)setIndex;

/// Automatically creates a new set of values
/// if `setIndex` exceeds the number of existing sets.
- (void) addValue:(NSNumber*)value
            toSet:(NSUInteger)setIndex;

/// - Returns: The value of the given set at the given index.
///
/// If the value is a complex number, ``floatRepresentation`` is used to determine the result.
- (double) doubleValueAtIndex:(NSUInteger)valueIndex
                       forSet:(NSUInteger)setIndex;

- (NSNumber*) valueAtIndex:(NSUInteger)valueIndex
                    forSet:(NSUInteger)setIndex;

/// - Returns: NSNumber objects with `double` value.
///
/// The returned array is not a copy. Do not release it!
- (NSArray<NSNumber*>*) valuesOfSet:(int)setIndex;

@end

NS_ASSUME_NONNULL_END

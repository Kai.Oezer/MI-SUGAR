/**
*
*   Copyright Kai Özer, 2003-2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import <Foundation/Foundation.h>
#import "AnalysisVariable.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, AnalysisType) {
	AnalysisTypeDC = 0,
	AnalysisTypeAC,
	AnalysisTypeTRAN,
	AnalysisTypeDISTO
} NS_SWIFT_NAME(AnalysisType);

@interface ResultsTable : NSObject

@property AnalysisType analysisType;

@property NSMutableArray<AnalysisVariable*>* variables;

/// The title string for the circuit analysis that generated the data.
@property (nullable) NSString* analysisTitle;

/// The title string for the circuit.
@property (nullable) NSString* circuitTitle;

- (void) addVariable:(AnalysisVariable*)variable;

/// - Returns: the analysis variable at the specified index
- (AnalysisVariable*) variableAtIndex:(long)varIndex;

@end

NS_ASSUME_NONNULL_END

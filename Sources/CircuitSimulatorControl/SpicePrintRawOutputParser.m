/**
*
*   Copyright Kai Özer, 2004-2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import "SpicePrintRawOutputParser.h"
#import "ResultsTable.h"
#import "AnalysisVariable.h"

@implementation SpicePrintRawOutputParser

+ (NSArray<ResultsTable*>*) parseFile:(NSString*)filePath
                                error:(NSError**)error
{
  NSString* fileContents = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:error];
  if ( fileContents == nil )
  {
    return [NSArray<ResultsTable*> array];
  }
  return [SpicePrintRawOutputParser parse:fileContents error:error];
}


+ (NSArray<ResultsTable*>*) parse:(NSString*)input
                            error:(NSError**)error
{
  NSUInteger lineStart, nextLineStart, lineEnd;
  NSString* nextLine = nil;
  NSString* plotName = nil;
  NSString* circuitName = nil;
  int numVars = 0, numPoints = 0;
  ResultsTable* table = nil;
  NSMutableArray<ResultsTable*>* resultsTables = [NSMutableArray<ResultsTable*> arrayWithCapacity:1];
  lineStart = 0;
  // Scanning the string line by line
  while (lineStart < ([input length] - 1))
  {
    [input getLineStart:&lineStart end:&nextLineStart contentsEnd:&lineEnd forRange:NSMakeRange(lineStart, 1)];
    nextLine = [input substringWithRange:NSMakeRange(lineStart, lineEnd - lineStart)];
    lineStart = nextLineStart;

    if ([nextLine hasPrefix:@"Title: "])
    {
      circuitName = [nextLine substringFromIndex:7];
      table = [[ResultsTable alloc] init];
      [table setCircuitTitle:circuitName];
    }
    if ([nextLine hasPrefix:@"Plotname: "])
    {
      plotName = [nextLine substringFromIndex:10];
      [table setAnalysisTitle:plotName];
    }
    else if ([nextLine hasPrefix:@"No. Variables: "])
    {
      numVars = [[nextLine substringFromIndex:15] intValue];
    }
    else if ([nextLine hasPrefix:@"No. Points: "])
    {
      numPoints = [[nextLine substringFromIndex:12] intValue];
    }
    else if ([nextLine isEqualToString:@"Variables:"])
    {
      for (int j = 0; j < numVars; j++)
      {
        [input getLineStart:&lineStart
                        end:&nextLineStart
                contentsEnd:&lineEnd
                   forRange:NSMakeRange(lineStart, 1)];
        nextLine = [input substringWithRange:NSMakeRange(lineStart, lineEnd - lineStart)];
        lineStart = nextLineStart;
        NSArray<NSString*>* variableTypeData = [nextLine componentsSeparatedByString:@"\t"];
        [table addVariable: [[AnalysisVariable alloc] initWithName:[variableTypeData objectAtIndex:2]]];
      }
    }
    else if ([nextLine isEqualToString:@"Values:"])
    {
      double value;
      double real;
      double imaginary;
      double firstSweepValue = 0.0; // used to detect the start of a new set
      int set = 0;
      NSString* valueCandidate;

      for (int n = 0; n < numPoints; n++)
      {
        for (int m = 0; m < numVars; m++)
        {
          [input getLineStart:&lineStart
                          end:&nextLineStart
                  contentsEnd:&lineEnd
                     forRange:NSMakeRange(lineStart, 1)];
          nextLine = [input substringWithRange:NSMakeRange(lineStart, lineEnd - lineStart)];
          lineStart = nextLineStart;
          // for m == 0 the line starts with the index number of the point
          if (m == 0)
          {
            // Parse the value of the sweep variable
            NSArray<NSString*>* comps = [nextLine componentsSeparatedByString:@"\t"];
            valueCandidate = [comps lastObject];
            if ([valueCandidate rangeOfString:@","].location == NSNotFound)
            {
              value = [valueCandidate doubleValue];
            }
            else
            {
                // Sweep variable values are not allowed to be complex
                value = [[valueCandidate substringToIndex:[valueCandidate rangeOfString:@","].location] doubleValue];
            }
            // Checking if a new set has started
            if (n == 0)
            {
              firstSweepValue = value;
              [[table variableAtIndex:0] addDoubleValue:value toSet:set];
            }
            else
            {
              [[table variableAtIndex:0] addDoubleValue:value toSet:((value == firstSweepValue) ? ++set : set)];
            }
          }
          else
          {
            valueCandidate = [nextLine stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSUInteger const separatorLocation = [valueCandidate rangeOfString:@","].location;
            if (separatorLocation == NSNotFound)
            {
              value = [valueCandidate doubleValue];
              [[table variableAtIndex:m] addDoubleValue:value toSet:set];
            }
            else // complex number
            {
              real = [[valueCandidate substringToIndex:separatorLocation] doubleValue];
              imaginary = [[valueCandidate substringFromIndex:(separatorLocation + 1)] doubleValue];
              [[table variableAtIndex:m] addValue:[[ComplexNumber alloc] initWithReal:real imaginary:imaginary] toSet:set];
            }
          }
        }
      }
      /* Sort the table
      if ([table needsSorting])
          [table sort]; */
      // Add the constructed table to the array of tables
      [resultsTables addObject:table];
    }
  }
  return resultsTables;
}

@end

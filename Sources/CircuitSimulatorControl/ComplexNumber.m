/**
*
*   Copyright Kai Özer, 2003-2018
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import "ComplexNumber.h"
#include <math.h>

@implementation ComplexNumber
{
@private
	double _magnitude;
}

- (instancetype) initWithReal:(double)realPart
          imaginary:(double)imaginaryPart
{
  if (self = [super init])
  {
    _real = realPart;
    _imaginary = imaginaryPart;
		_magnitude = -1.0; // delaying the expensive calculation until needed
  }
  return self;
}

- (double) magnitude
{
	if (_magnitude < 0.0) {
		_magnitude = sqrt((_real * _real) + (_imaginary * _imaginary));
	}
	return _magnitude;
}

// MARK: NSNumber overrides

- (const char*) objCType
{
	// declaring that this type represents a `double` value
	return "d";
}

- (double) doubleValue
{
	return [self magnitude];
}

- (NSString*) debugDescription
{
	return [NSString stringWithFormat:@"%g, %g", _real, _imaginary];
}

@end

/**
*
*   Copyright Kai Özer, 2003-2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import "SpiceSimulator.h"

NSString* const SpiceSimulatorSimulationDidFinishNotification = @"SpiceSimulatorSimulationDidFinishNotification";

typedef NS_ENUM(NSUInteger, SimulatorState) {
  SimulatorStateStopped = 0,
  SimulatorStateRunning,
  SimulatorStateAborting
};

// This implementation uses POSIX system calls for interprocess communication
// instead of Cocoa's object-oriented wrappers (NSTask, NSPipe, etc.).
@implementation SpiceSimulator
{
@private
  NSTask* _simulationTask;
  NSPipe* _dataPipe;
  SimulatorState _state;
  SpiceSimulatorType _simulatorType;
  NSString* _simulatorExecutablePath;
  NSMutableArray<NSData*>* _outputChunks; ///< stores output data as it becomes available
}

static NSUInteger numberOfExpectedChunks = 100;


- (instancetype) initWithSimulatorType:(SpiceSimulatorType)type
                        executablePath:(NSString*)filePath
{
  if (![[NSFileManager defaultManager] isExecutableFileAtPath:filePath]) {
    return nil;
  }
  if (self = [super init]) {
    _simulatorType = type;
    _simulatorExecutablePath = filePath;
    _simulationTask = nil;
    _state = SimulatorStateStopped;
    _outputChunks = [NSMutableArray<NSData*> arrayWithCapacity:numberOfExpectedChunks];
  }
  return self;
}

- (void) launch
{
  if (self.input == nil) {
    NSLog(@"Attempt to run simulator without input.");
    return;
  }

  [_outputChunks removeAllObjects];
  _dataPipe = [[NSPipe alloc] init];

  /* Creating a temporary file with the contents of the text area... */
  NSString* tmpFile = [NSString stringWithFormat:@"/tmp/%d.misugar.in", [[NSProcessInfo processInfo] processIdentifier]];
  [self.input writeToFile:tmpFile atomically:NO encoding:NSASCIIStringEncoding error:nil];

  _simulationTask = [[NSTask alloc] init];
  [_simulationTask setLaunchPath:_simulatorExecutablePath];
  if ((_simulatorType == SpiceSimulatorTypeSpice) || (_simulatorType == SpiceSimulatorTypeNgspice))
  {
      // setting up a SPICE command
      [_simulationTask setArguments:[NSArray arrayWithObjects:
                                  @"-b", // batch mode
                                  //@"-r", // generate raw output file
                                  //@"/tmp/misugar.out" // output file
                                  tmpFile, // input file
                                  nil]];
  }
  else
  {
      // setting up a Gnucap command
      [_simulationTask setArguments:[NSArray arrayWithObjects:
                                   @"-b", // batch mode
                                   tmpFile, // input file
                                   nil]];
  }
  // Set up the environment for unbuffered data piping
  NSMutableDictionary<NSString*,NSString*>* environment =
    [NSMutableDictionary dictionaryWithDictionary:
       [[NSProcessInfo processInfo] environment]];
  [environment setObject:@"YES"
                  forKey:@"NSUnbufferedIO"];
  [_simulationTask setEnvironment:environment];
  // Redirect standard output and standard error to the pipe input
  [_simulationTask setStandardError:[_dataPipe fileHandleForWriting]];
  [_simulationTask setStandardOutput:[_dataPipe fileHandleForWriting]];
  // Setting up simulation output handler
  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(simulationOutputAvailable:)
             name:NSFileHandleReadCompletionNotification
           object:[_dataPipe fileHandleForReading]];
  // Setting up simulation end handler
  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(endSimulation:)
             name:NSTaskDidTerminateNotification
           object:_simulationTask];
  _state = SimulatorStateRunning;
  // Launch sub-process
  [_simulationTask launch];
  [[_dataPipe fileHandleForReading] readInBackgroundAndNotify];
}

- (void) simulationOutputAvailable:(NSNotification*)aNotification
{
  if (_state == SimulatorStateAborting) {
    return;
  }

  NSData* rawData = [[aNotification userInfo] objectForKey:NSFileHandleNotificationDataItem];

  if ((rawData != nil) && ([rawData length] > 0)) {
    [_outputChunks addObject:rawData];
  }
  else if (![_simulationTask isRunning]) {
    _dataPipe = nil;
      return;
  }
  [[_dataPipe fileHandleForReading] readInBackgroundAndNotify];
}

- (void) endSimulation:(NSNotification*)aNotification
{
  if (_simulationTask == [aNotification object])
  {
    [self _cleanUpSimulationInputFile];
  }
  _state = SimulatorStateStopped;

  NSUInteger const numReceivedChunks = [_outputChunks count];
  if (numReceivedChunks > numberOfExpectedChunks) {
    numberOfExpectedChunks = numReceivedChunks;
  }

  NSMutableString* output = [NSMutableString stringWithCapacity:8192]; // reserving 8 KB
  for (NSData* chunk in _outputChunks) {
    NSString* snippet = [[NSString alloc] initWithData:chunk encoding:NSUTF8StringEncoding];
    [output appendString:snippet];
  }

  [[NSNotificationCenter defaultCenter] postNotificationName:SpiceSimulatorSimulationDidFinishNotification
                                                      object:self
                                                    userInfo:@{@"Output" : output}];
}

- (void) abort
{
  if ((_state == SimulatorStateRunning) && (_simulationTask != nil))
  {
    [_simulationTask terminate];
    [self _cleanUpSimulationInputFile];
  }
}

- (void) _cleanUpSimulationInputFile
{
  NSString* inputFilePath = [NSString stringWithFormat:@"/tmp/%d.misugar.in", [[NSProcessInfo processInfo] processIdentifier]];
  [[NSFileManager defaultManager] removeItemAtPath:inputFilePath error:nil];
}

@end

/**
*
*   Copyright Kai Özer, 2003-2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import "SpicePrintOutputParser.h"
#import "ResultsTable.h"
#import "ComplexNumber.h"

#define DASHES @"------------------"

NSString* const SpiceParserErrorDomain = @"SpiceParserErrorDomain";

@implementation SpicePrintOutputParser

typedef NS_ENUM(NSUInteger, ParsingState) {
	ParsingStateExpectingCircuitTitle,         ///< the initial state
	ParsingStateExpectingNumberOfDataRows,     ///< after reading the circuit name
	ParsingStateExpectingDatasetHeader,        ///< the dataset header consists of one line for the circuit title and one line for the analysis title
	ParsingStateExpectingNumberOfDataRowsOrDatasetHeader,
	ParsingStateExpectingDatasetHeaderAnalysisTitle,
	ParsingStateExpectingDataTableHeader,
	ParsingStateExpectingDataTableRow,
	ParsingStateFinishedDataTableRows,
};

#define makeRegex(x) [NSRegularExpression regularExpressionWithPattern:x options:0 error:nil]
#define firstMatch(x, line) [x firstMatchInString:line options:0 range:NSMakeRange(0, [line length])]
#define allMatches(x, line) [x matchesInString:line options:0 range:NSMakeRange(0, [line length])]
#define trimmed(x) [x stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]

static NSRegularExpression* pattern_circuitName = nil;
static NSRegularExpression* pattern_numberOfDataRows = nil;
static NSRegularExpression* pattern_whitespaceSeparatedTokens = nil;
static NSRegularExpression* pattern_dataRow = nil;
static NSRegularExpression* pattern_value = nil;
static NSDictionary* decimalParsingLocale = nil;

+ (void) initialize
{
	if (self == [SpicePrintOutputParser self]) // prevents multiple initialization
	{
		pattern_circuitName = makeRegex(@"^Circuit: (.*)$");
		pattern_numberOfDataRows = makeRegex(@"^No\\. of Data Rows : (\\d*)\\s*$");
		pattern_whitespaceSeparatedTokens = makeRegex(@"\\S+");
		pattern_dataRow = makeRegex(@"^(\\d+)\\s+[+-]?\\d+\\.\\d+e[+-]\\d+");
		pattern_value = makeRegex(@"[+-]?\\d+\\.\\d+e[+-]\\d+,?"); // with optional trailing comma

		decimalParsingLocale = @{ NSLocaleDecimalSeparator : @"." };
	}
}

+ (NSArray<ResultsTable*>*) parse:(NSString*)input
                            error:(NSError* __autoreleasing _Nullable *)error
{
	__block ParsingState state = ParsingStateExpectingCircuitTitle;
	__block NSString* circuitTitle = nil;
	__block ResultsTable* currentTable = nil;
	__block NSUInteger currentExpectedNumDataRows = 0;
	__block let result = [NSMutableArray<ResultsTable*> new];

	// Stores the expected number of data rows for a dataset.
	// When the stack is empty, no more datasets are expected.
	__block let expectedDatasetsStack = [NSMutableArray<NSNumber*> new];

	BOOL (^processState_NumberOfDataRows)(NSString*) = ^BOOL(NSString* line) {
		let extractedNumber = [self extractNumDataRowsFrom:line];
		let lineContainsNumberOfDataRows = extractedNumber >= 0;
		if (!lineContainsNumberOfDataRows) {
			return NO;
		}
		currentExpectedNumDataRows = extractedNumber;
		[expectedDatasetsStack addObject:[NSNumber numberWithInteger:extractedNumber]];
		state = ParsingStateExpectingNumberOfDataRowsOrDatasetHeader;
		return YES;
	};

	void (^processState_DatasetHeader)(NSString*) = ^void(NSString* line) {
		NSAssert(circuitTitle != nil, @"The circuit title must have already been parsed at this point.");
		let trimmedString = [line stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
		if ([trimmedString isEqualToString:circuitTitle]) {
			currentTable = [ResultsTable new];
			currentTable.circuitTitle = circuitTitle;
			state = ParsingStateExpectingDatasetHeaderAnalysisTitle;
		}
	};

	[input enumerateLinesUsingBlock:^(NSString* line, BOOL* stop) {
		if ([line length] == 0) {
			return;
		}
		if (state == ParsingStateExpectingCircuitTitle) {
			let extractedCircuitTitle = [self extractCircuitNameFrom:line];
			if (extractedCircuitTitle == nil) { return; }
			circuitTitle = extractedCircuitTitle;
			state = ParsingStateExpectingNumberOfDataRows;
		}
		else if (state == ParsingStateExpectingNumberOfDataRows) {
			processState_NumberOfDataRows(line);
		}
		else if (state == ParsingStateExpectingDatasetHeader) {
			processState_DatasetHeader(line);
		}
		else if (state == ParsingStateExpectingNumberOfDataRowsOrDatasetHeader) {
			if (!processState_NumberOfDataRows(line)) {
				processState_DatasetHeader(line);
			}
		}
		else if (state == ParsingStateExpectingDatasetHeaderAnalysisTitle) {
			let trimmedString = [line stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
			currentTable.analysisTitle = trimmedString;
			state = ParsingStateExpectingDataTableHeader;
		}
		else if (state == ParsingStateExpectingDataTableHeader) {
			let variableNames = [self extractTableStartFrom:line];
			if (variableNames == nil) { return; }
			for (NSString* name in variableNames) {
				let variable = [[AnalysisVariable alloc] initWithName:name];
				[currentTable.variables addObject:variable];
			}
			state = ParsingStateExpectingDataTableRow;
		}
		else if (state == ParsingStateExpectingDataTableRow) {
			let dataRowValues = [self extractDataRowValuesFrom:line error:error];
			if (dataRowValues == nil) {
				return;
			}
			let numValues = [dataRowValues count];
			if (numValues != currentTable.variables.count + 1) {
				*error = [NSError errorWithDomain:SpiceParserErrorDomain code:SpiceParserErrorCodeNumberOfValuesNotMatchingNumberOfVariables userInfo:@{@"Line" : line}];
				*stop = YES;
				return;
			}
			let index = [[dataRowValues firstObject] integerValue];
			if (index >= currentExpectedNumDataRows) {
				*error = [NSError errorWithDomain:SpiceParserErrorDomain code:SpiceParserErrorCodeDataRowIndexOverflow userInfo:@{@"Line" : line}];
				*stop = YES;
				return;
			}
			for (int i = 1; i < numValues; i++) {
				let variable = [currentTable variableAtIndex:(i - 1)];
				[variable addValue:[dataRowValues objectAtIndex:i] toSet:0];
			}
			if (index == currentExpectedNumDataRows - 1) {
				[expectedDatasetsStack removeLastObject];
				if ([expectedDatasetsStack count] == 0) {
					state = ParsingStateFinishedDataTableRows;
				} else {
					currentExpectedNumDataRows = [[expectedDatasetsStack lastObject] integerValue];
					state = ParsingStateExpectingDatasetHeader;
				}
				[result addObject:currentTable];
				currentTable = nil;
			}
		}
	}];

	return result;
}

+ (nullable NSString*) extractCircuitNameFrom:(NSString*)line
{
	let match = firstMatch(pattern_circuitName, line);
	if ((match == nil) || (match.range.location == NSNotFound)) {
		return nil;
	}
	NSAssert([match numberOfRanges] == 2, @"There should be a range for the capture group that contains the name.");
	NSRange nameRange = [match rangeAtIndex:1];
	return trimmed([line substringWithRange:nameRange]);
}

/// - Returns: a non-negative integer extracted from the given line,
/// or `-1` if the given input string is not an integer representation.
+ (NSInteger) extractNumDataRowsFrom:(NSString*)line
{
	let match = firstMatch(pattern_numberOfDataRows, line);
	if ((match == nil) || (match.range.location == NSNotFound)) {
		return -1;
	}
	NSAssert([match numberOfRanges] == 2, @"There should be a range for the capture group that contains the number of rows.");
	return [[line substringWithRange:[match rangeAtIndex:1]] integerValue];
}

+ (NSArray<NSString*>*) extractTableStartFrom:(NSString*)line
{
	if ([line hasPrefix:@"Index "]) {
		NSString* variableNamesString = [line substringFromIndex:6];
		let matches = allMatches(pattern_whitespaceSeparatedTokens, variableNamesString);
		if ([matches count] > 0) {
			let result = [NSMutableArray arrayWithCapacity:[matches count]];
			for (NSTextCheckingResult* match in matches) {
				if (match.range.location != NSNotFound) {
					NSString* variableName = [variableNamesString substringWithRange:[match range]];
					[result addObject:variableName];
				}
			}
			return result;
		}
	}
	return nil;
}

+ (NSArray<NSNumber*>*) extractDataRowValuesFrom:(NSString*)line
                                           error:(NSError**)error
{
	let match = firstMatch(pattern_dataRow, line);
	if ((match == nil) || (match.range.location == NSNotFound) || (match.numberOfRanges < 2)) {
		return nil;
	}

	let indexNumberRange = [match rangeAtIndex:1];
	NSAssert(indexNumberRange.location != NSNotFound, @"");
	let valuesString = [line substringFromIndex:(indexNumberRange.location + indexNumberRange.length)];

	let valueMatches = allMatches(pattern_value, valuesString);
	NSAssert(valueMatches.count > 0, @"There must be at least one match. Otherwise the previously searched pattern would not have matched.");
	if ([valueMatches count] == 0) {
		*error = [NSError errorWithDomain:SpiceParserErrorDomain code:SpiceParserErrorCodeGeneric userInfo:@{@"line": line}];
		return nil;
	}

	let result = [NSMutableArray<NSNumber*> arrayWithCapacity:(valueMatches.count + 1)];
	let indexNumberValue = [[line substringWithRange:indexNumberRange] integerValue];
	[result addObject:[NSNumber numberWithInteger:indexNumberValue]];

	NSDecimalNumber* complexNumberRealPart = nil;
	for (NSTextCheckingResult* valueMatch in valueMatches) {
		NSAssert(valueMatch.range.location != NSNotFound, @"There was a match, so there must be a valid range.");
		var valueString = [valuesString substringWithRange:valueMatch.range];
		if ([valueString hasSuffix:@","]) {
			if (complexNumberRealPart != nil) {
				*error = [NSError errorWithDomain:SpiceParserErrorDomain code:SpiceParserErrorCodeMalformedComplexNumber userInfo:@{@"line": line}];
				return nil;
			}
			valueString = [valueString substringToIndex:[valueString length] - 1];
			complexNumberRealPart = [NSDecimalNumber decimalNumberWithString:valueString locale:decimalParsingLocale];
		} else {
			NSNumber* number = [NSDecimalNumber decimalNumberWithString:valueString locale:decimalParsingLocale];
			if (complexNumberRealPart != nil) {
				number = [[ComplexNumber alloc] initWithReal:[complexNumberRealPart doubleValue] imaginary:[number doubleValue]];
				complexNumberRealPart = nil;
			}
			[result addObject:number];
		}
	}
	return result;
}

// MARK: -

// The output of the PRINT command of SPICE produces text data in table form.
// The table for an analysis type starts with a line containing the name of the analysis.
// This line is followed by a line with dashes.
// Then comes the table header with the names of the columns.
// The first column is the "Index" column with the running index of the current data set.
// Then comes the names of the X-axis (abscissa) variable ->
//     "time" for transient analysis, "frequency" for AC analysis, "sweep" or "v-sweep" for DC analysis.
// The table header line is followed by a line of dashes.
// Then the data starts with index = 0.
// The table does not run uninterrupted until the end, though. For better orientation
//     SPICE inserts empty rows at about every 50th data set and repeats the table header
//     plus the following line of dashes before continuing.
+ (NSArray<ResultsTable*>*) legacyParse:(NSString*)input
                            error:(NSError**)error
{
    NSMutableArray<ResultsTable*>* resultsTables = [[NSMutableArray<ResultsTable*> alloc] initWithCapacity:1];
    NSMutableArray<NSString*>* lines = [[NSMutableArray<NSString*> alloc] initWithCapacity:1];
    NSMutableArray<NSString*>* variableNames = [[NSMutableArray<NSString*> alloc] initWithCapacity:1];
    NSMutableArray<NSString*>* values = [[NSMutableArray<NSString*> alloc] initWithCapacity:5];
    NSUInteger lineStart = 0, nextLineStart = 0, lineEnd = 0, numLines = 0;
    NSString* thisLine = nil;
    int i = 0; // index of current processed line
    int indexCounter = 0; // index of the sample point
    int setCounter = 0; // current set of the value (for parametric variables)
    BOOL success = YES;
    BOOL analysisAlreadyExists = NO;
    BOOL sweepIsComplex = NO; // indicates whether the sweep variable is complex-valued
    ResultsTable* table = nil;
    NSString* circuitName = nil;
    NSInteger variableIndexOffset = 0;
    double sweepStartValue = 0.0;

    // put the lines into an array
    while (lineStart < [input length])
    {
        [input getLineStart:&lineStart
                        end:&nextLineStart
                contentsEnd:&lineEnd
                   forRange:NSMakeRange(lineStart, 1)];
        [lines addObject:[input substringWithRange:NSMakeRange(lineStart, lineEnd - lineStart)]];
        lineStart = nextLineStart;
    }
    numLines = [lines count];

    // Extract circuit name
    for (i = 0; i < numLines; i++)
        if ([[lines objectAtIndex:i] hasPrefix:@"Circuit: "])
        {
            circuitName = [[lines objectAtIndex:i] substringFromIndex:[@"Circuit: " length]];
            break;
        }
    if (i == numLines)
        success = NO;

    while (success)
    {
        // Find the (next) line with all dashes
        for (; i < numLines; i++)
            if ([[lines objectAtIndex:i] hasPrefix:DASHES])
                break;
        if (i >= numLines)
            break;

        if ([[lines objectAtIndex:(i-1)] hasPrefix:@"Index "] && table)
        {
            NSString* sweepValueString;
            double sweepValue; // the final value assigned to the sweep variable at the current sample point
            NSString *realPart; // the real part of the complex-values sweep variable
            int varIndex; // used to iterate over all sample values of the variables
            int k;

            [values removeAllObjects];
            if (indexCounter == 0)
            {
                // This is the first time we get a list of variable names
                // Constructing the AnalysisVariable objects...
                NSString* names;
                NSUInteger endIndex;
                // read the names of the variables
                names = [[[lines objectAtIndex:(i-1)] substringFromIndex:[@"Index " length]]
                    stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                for (;;)
                {
                  endIndex = [names rangeOfString:@" "].location;
                  if (endIndex == NSNotFound)
                  {
                    [variableNames addObject:[NSString stringWithString:names]];
                    break;
                  }
                  [variableNames addObject:[names substringToIndex:endIndex]];
                  names = [[names substringFromIndex:(endIndex + 1)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                }
                // Check if the results table already has some variables.
                variableIndexOffset = analysisAlreadyExists ? ([table.variables count] - 1) : 0;
                // Add the variables to the results table. Skip the sweep variable if analysis already exists.
                for (endIndex = analysisAlreadyExists ? 1 : 0; endIndex < [variableNames count]; endIndex++)
                {
                    [table addVariable:[[AnalysisVariable alloc] initWithName:
                        [variableNames objectAtIndex:endIndex]]];
                }
                [variableNames removeAllObjects];
            }
            for(i++; i < numLines; i++) // Go on scanning the values
            {
                // Check if the end of this value block is reached
                if ([[[lines objectAtIndex:i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0)
                    break;

                // Put all table entries for the current sample point into one array.
                // Note that an entry may span multiple lines.
                [values removeAllObjects];
                thisLine = [lines objectAtIndex:i];
                while ( ([[thisLine stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] > 0) &&
                        (![thisLine isEqualToString:@"\f"]) && // Mac OS X 10.3 somehow inserts Form Feed ('\f') characters
                        (![thisLine hasPrefix:[NSString stringWithFormat:@"%d", indexCounter + 1]]) )
                {
                    [values addObjectsFromArray:[[thisLine stringByTrimmingCharactersInSet:
                        [NSCharacterSet whitespaceCharacterSet]] componentsSeparatedByString:@"\t"]];
                    thisLine = (NSString*) [lines objectAtIndex:++i];
                }
                i--;
                if (![values count])
                    break;
                // Add the sweep variable's value only if it wasn't added before.
                if (!analysisAlreadyExists)
                {
                    // If the sweep value is complex number, we take the real part only.
                    sweepValueString = (NSString*) [values objectAtIndex:1];
                    sweepIsComplex = [sweepValueString hasSuffix:@","];
                    if (sweepIsComplex)
                    {
                      sweepValueString = [sweepValueString substringToIndex:([sweepValueString length] - 1)];
                    }
                    sweepValue = [sweepValueString doubleValue];
                    // Check if a new set has started
                    if (!indexCounter && !setCounter)
                        sweepStartValue = sweepValue;
                    else if (sweepStartValue == sweepValue)
                        setCounter++;
                    // Add the value of the sweep variable - must not be complex
                    [[table variableAtIndex:0] addDoubleValue:sweepValue
                                                        toSet:setCounter];
                }
                //
                for (varIndex = sweepIsComplex ? 3 : 2, k = 1; varIndex < [values count]; varIndex++, k++)
                {
                    if ([[values objectAtIndex:varIndex] hasSuffix:@","])
                    {
                        realPart = (NSString*) [values objectAtIndex:varIndex];
                        realPart = [realPart substringToIndex:([realPart length] - 1)];
                        [[table variableAtIndex:(variableIndexOffset + k)] addValue:[[ComplexNumber alloc] initWithReal:[realPart doubleValue]
                                                                                                              imaginary:[[values objectAtIndex:(varIndex+1)] doubleValue]]
                                                                              toSet:setCounter];
                        varIndex++;
                    }
                    else {
                        let analysisVariable = [table variableAtIndex:(variableIndexOffset + k)];
                        [analysisVariable addDoubleValue:[[values objectAtIndex:varIndex] doubleValue]
                                                   toSet:setCounter];
                    }
                }
                //fprintf(stderr, "%d ", indexCounter); 
                indexCounter++;
            }
        }
        else if ([[lines objectAtIndex:(i+1)] hasPrefix:@"Index "])
        {
            // Parse the previous line to extract the analysis type
            long t = 0;
            BOOL analysisNameExists = NO;

            if (i + 3 >= numLines) break; // corrupt SPICE output
            // Extract analysis name, getting rid of the date string
            /* Note: We cannot use the analysis names from the CircuitDocumentModel since SPICE output
                may contain separator lines within the list of values pertaining to the same analysis
                and we have to check that. */
            NSString* analysisName = [[lines objectAtIndex:(i-1)] stringByTrimmingCharactersInSet:
                [NSCharacterSet whitespaceCharacterSet]];
         /* analysisName = [analysisName substringToIndex:[analysisName rangeOfString:@"Analysis"].location]; */
            // Reset index count
            indexCounter = 0;
            // Reset the set count
            setCounter = 0;
            // Is this a new analysis type or do we merely add new variables?
            // Important: Search the tables in reverse order
            for (t = [resultsTables count] - 1; t >= 0; t--)
                if ([[[resultsTables objectAtIndex:t] analysisTitle] hasPrefix:analysisName])
                {
                    analysisNameExists = YES;
                    table = (ResultsTable*) [resultsTables objectAtIndex:t];
                    break;
                }
            /* If this analysis type was used before is it the continuation of the
                previous output or is it a new analysis of the same kind?
                We have to check if the first index number of the values is zero. */
            analysisAlreadyExists = analysisNameExists /* && ![[lines objectAtIndex:(i+3)] hasPrefix:@"0"] */;
            
            if (!analysisAlreadyExists)
            {
                // Create a new ResultsTable if the analysis type is new
                table = [[ResultsTable alloc] init];
                if (analysisNameExists)
                    [table setAnalysisTitle:[[[resultsTables objectAtIndex:t] analysisTitle] stringByAppendingString:@"|"]];
                else
                    [table setAnalysisTitle:analysisName];
                [table setCircuitTitle:(circuitName ? circuitName : @"")];
                [resultsTables addObject:table];
            }
            i++; // continue to next line
        }
        else
            i++; // continue to next line
    }
    
    if (success)
    {
      return resultsTables;
    }
    else
    {
      return nil;
    }
}

@end

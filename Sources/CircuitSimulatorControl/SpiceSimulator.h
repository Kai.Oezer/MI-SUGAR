/**
*
*   Copyright Kai Özer, 2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, SpiceSimulatorType)
{
  SpiceSimulatorTypeSpice = 0,
  SpiceSimulatorTypeNgspice,
  SpiceSimulatorTypeGnucap,
  SpiceSimulatorTypePSpice,
}
NS_SWIFT_NAME(SpiceSimulatorType);

NS_ASSUME_NONNULL_BEGIN

/// The notification object is the ``SpiceSimulator`` instance.
/// The userInfo dictionary contains the result of the simulation.
extern NSString* const SpiceSimulatorSimulationDidFinishNotification;

@interface SpiceSimulator : NSObject

@property (readonly) BOOL isRunning;
@property (nullable) NSString* input;

/// - parameter filePath: The path to an executable file with execution permissions.
- (nullable instancetype) initWithSimulatorType:(SpiceSimulatorType)type
                                 executablePath:(NSString*)filePath;

/// start running
- (void) launch;

- (void) abort;

@end

NS_ASSUME_NONNULL_END

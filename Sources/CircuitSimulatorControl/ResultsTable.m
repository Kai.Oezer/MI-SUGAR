/**
*
*   Copyright Kai Özer, 2003-2018
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import "ResultsTable.h"

@implementation ResultsTable

- (instancetype) init
{
	if (self = [super init])
	{
		_analysisType = AnalysisTypeAC;
		_variables = [NSMutableArray<AnalysisVariable*> arrayWithCapacity:1];
		_analysisTitle = nil;
		_circuitTitle = nil;
	}
	return self;
}

- (AnalysisVariable*) variableAtIndex:(long)index
{
	if (index < 0 || index >= [_variables count]) {
		return nil;
	}
	return [_variables objectAtIndex:index];
}

- (void) addVariable:(AnalysisVariable*)variable
{
    [_variables addObject:variable];
}

/// Converts the data to a text with one column for each variable.
///
/// Columns are separated by tab characters.
- (NSString*) description
{
	let numSets = [[self variableAtIndex:0] numberOfSets];
	let numValuesPerSet = [[self variableAtIndex:0] numberOfValuesPerSet];
	let lines = [NSMutableArray<NSString*> arrayWithCapacity:numValuesPerSet];

	let header = [_variables componentsJoinedByString:@"\t"];
	[lines addObject:header];

	for (int currentSet = 0; currentSet < numSets; currentSet++)
	{
		[lines addObject:@""];

		for (int currentValue = 0; currentValue < numValuesPerSet; currentValue++)
		{
			let tokens = [NSMutableArray arrayWithCapacity:[_variables count]];
			for (AnalysisVariable* currentVar in _variables)
			{
				if ([currentVar isComplex])
				{
					let cn = (ComplexNumber*)[currentVar valueAtIndex:currentValue forSet:currentSet];
					[tokens addObject: [NSString stringWithFormat:@"%g %g", [cn real], [cn imaginary]]];
				}
				else
				{
					[tokens addObject: [NSString stringWithFormat:@"%g", [currentVar doubleValueAtIndex:currentValue forSet:currentSet]]];
				}
			}
			let line = [tokens componentsJoinedByString:@"\t"];
			[lines addObject:line];
		}
	}

	return [lines componentsJoinedByString:@"\n"];
}


@end

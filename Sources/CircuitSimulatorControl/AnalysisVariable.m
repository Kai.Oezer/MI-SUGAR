/**
*
*   Copyright Kai Özer, 2003-2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import "AnalysisVariable.h"

@implementation AnalysisVariable
{
  NSString* name;
  double scaleFactor;     // the scale factor for presentation purposes - does not alter the stored values
  double averageValue;  // the average of all values, also valid for magnitude of complex numbers
  double averageValueReal; // average value of real part
  double averageValueImaginary; // average value of imaginary part
  BOOL foundAverage;  // indicates that the average is already calculated
  double maximum; // maximum of magnitude, if complex
  double maximumReal; // maximum of real part
  double maximumImaginary; // maximum of imaginary part
  double minimum; // minimum of magnitude, if complex
  double minimumReal; // minimum of real part
  double minimumImaginary; // minimum of imaginary part
  BOOL foundMinMax;  // indicates that the maximum and minimum values were already found
  enum VariableType {Voltage = 0, Current, Time} type;
  NSMutableArray<NSMutableArray<NSNumber*>*>* values; ///< an array of arrays of either `NSNumber` or ``ComplexNumber``, depending on ``complex``
  BOOL complex;
}

@synthesize isComplex = complex;

- (instancetype) initWithName:(NSString*)varName
{
    if (self = [super init])
    {
        values = [NSMutableArray<NSMutableArray<NSNumber*>*> arrayWithCapacity:1];
        name = varName;
        scaleFactor = 1.0;
        averageValue = 0.0;
        foundMinMax = NO;
        foundAverage = NO;
        complex = NO;
        _scalingAroundAverage = NO;
        _floatRepresentation = ComplexNumberFloatRepresentationMagnitude;
    }
    return self;
}


- (long) numberOfSets
{
    return [values count];
}


- (long) numberOfValuesPerSet
{
    if ([values count] > 0)
        return [[values objectAtIndex:0] count];
    else
        return 0;
}


- (NSString*) name { return name; }
- (int) type { return type; }
- (double) scaleFactor { return scaleFactor; }
- (void) setScaleFactor:(double)factor { scaleFactor = factor; }


- (double) averageValue
{
    if (!complex)
        return averageValue;
    else
    {
        switch (self.floatRepresentation)
        {
            case ComplexNumberFloatRepresentationMagnitude:
                return averageValue;
            case ComplexNumberFloatRepresentationReal:
                return averageValueReal;
            case ComplexNumberFloatRepresentationImaginary:
                return averageValueImaginary;
            default:
                return averageValue;
        }
    }
}


- (void) calculateAverageValue
{
    double average, averageReal, averageImaginary;
    long i, j;
    NSMutableArray* tmpSet;
    double totalNumValues = (double)([self numberOfSets] * [self numberOfValuesPerSet]);

    average = averageReal = averageImaginary = 0.0;

    if (complex)
    {
        ComplexNumber* cn;
        for (i = [self numberOfSets] - 1; i >= 0; i--)
        {
            tmpSet = [values objectAtIndex:i];
            for (j = [self numberOfValuesPerSet] - 1; j >= 0; j--)
            {
                cn = [tmpSet objectAtIndex:j];
                average += [cn magnitude];
                averageReal += [cn real];
                averageImaginary += [cn imaginary];
            }
        }
        averageReal /= totalNumValues;
        averageImaginary /= totalNumValues;
        averageValueReal = averageReal;
        averageValueImaginary = averageImaginary;
    }
    else
    {
        for (i = [self numberOfSets] - 1; i >= 0; i--)
        {
            tmpSet = [values objectAtIndex:i];
            for (j = [self numberOfValuesPerSet] - 1; j >= 0; j--)
                average += [[tmpSet objectAtIndex:j] doubleValue];
        }
    }
    average /= totalNumValues;
    
    averageValue = average;
    foundAverage = YES;
}


- (void) findMinMax
{
    if ([values count] == 0)
        maximum = maximumReal = maximumImaginary =
        minimum = minimumReal = minimumImaginary = 0.0;
    else
    {
        long i, j;
        double tmpMax, tmpMaxReal, tmpMaxImag, tmpMin,
            tmpMinReal, tmpMinImag, current;

        tmpMax = tmpMin =
            [[[values objectAtIndex:0] objectAtIndex:0] doubleValue];
        if (complex)
        {
            let complexNumber = (ComplexNumber*)[[values objectAtIndex:0] objectAtIndex:0];
            tmpMaxReal = tmpMinReal = [complexNumber real];
            tmpMaxImag = tmpMinImag = [complexNumber imaginary];
        }
        else
            tmpMaxReal = tmpMinReal = tmpMaxImag = tmpMinImag = 0.0;
        
        for (i = [self numberOfSets] - 1; i >= 0; i--)
        {
            NSMutableArray* tmpSet = [values objectAtIndex:i];

            // This block also processes the magnitude values if the numbers are complex
            for (j = [self numberOfValuesPerSet] - 1; j >= 0; j--)
            {
                current = [[tmpSet objectAtIndex:j] doubleValue];
                if (tmpMax < current)
                    tmpMax = current;
                else if (tmpMin > current)
                    tmpMin = current;
                if (complex)
                {
                    // real and imaginary part max/min values of complex numbers
                    current = [[tmpSet objectAtIndex:j] real];
                    if (tmpMaxReal < current)
                        tmpMaxReal = current;
                    else if (tmpMinReal > current)
                        tmpMinReal = current;
                    current = [[tmpSet objectAtIndex:j] imaginary];
                    if (tmpMaxImag < current)
                        tmpMaxImag = current;
                    else if (tmpMinImag > current)
                        tmpMinImag = current;
                }
                
            }
        }

        maximum = tmpMax;
        minimum = tmpMin;
        if (complex)
        {
            maximumReal = tmpMaxReal;
            maximumImaginary = tmpMaxImag;
            minimumReal = tmpMinReal;
            minimumImaginary = tmpMinImag;
        }
        foundMinMax = YES;
    }
}


- (double) maximum
{
    if (!complex)
        return maximum;
    else
    {
        switch (self.floatRepresentation)
        {
            case ComplexNumberFloatRepresentationMagnitude:
                return maximum;
            case ComplexNumberFloatRepresentationReal:
                return maximumReal;
            case ComplexNumberFloatRepresentationImaginary:
                return maximumImaginary;
            default:
                return maximum;
        }
    }
}


- (double) minimum
{
    if (!complex)
        return minimum;
    else
    {
        switch (self.floatRepresentation)
        {
            case ComplexNumberFloatRepresentationMagnitude:
                return minimum;
            case ComplexNumberFloatRepresentationReal:
                return minimumReal;
            case ComplexNumberFloatRepresentationImaginary:
                return minimumImaginary;
            default:
                return minimum;
        }
    }
}


- (double) realMinimum { return complex ? minimumReal : minimum; }
- (double) realMaximum { return complex ? maximumReal : maximum; }
- (double) imaginaryMinimum { return complex ? minimumImaginary : minimum; }
- (double) imaginaryMaximum { return complex ? maximumImaginary: maximum; }


- (void) addDoubleValue:(double)value
                  toSet:(NSUInteger)setIndex
{
    [self addValue:[NSNumber numberWithDouble:value] toSet:setIndex];
}


- (void) addValue:(NSNumber*)value
            toSet:(NSUInteger)setIndex
{
    complex = [value isKindOfClass:[ComplexNumber class]];
    if (setIndex < [values count]) {
        [[values objectAtIndex:setIndex] addObject:value];
    } else {
        NSAssert(setIndex == [values count], @"The set index must exceed the number of existing sets only be 1.");
        let newSet = [NSMutableArray<NSNumber*> arrayWithObject: value];
        [values addObject:newSet];
    }
}


- (NSNumber*) valueAtIndex:(NSUInteger)valueIndex
                    forSet:(NSUInteger)setIndex
{
    NSAssert((setIndex >= 0) && (setIndex < [values count]), @"invalid set index");
    return [[values objectAtIndex:setIndex] objectAtIndex:valueIndex];
}


- (double) doubleValueAtIndex:(NSUInteger)valueIndex
											 forSet:(NSUInteger)setIndex
{
    NSAssert((setIndex >= 0) && (setIndex < [values count]), @"invalid set index");
    if (!complex) {
        return [[[values objectAtIndex:setIndex] objectAtIndex:valueIndex] doubleValue];
    } else {
        let complexNumber = (ComplexNumber*)[[values objectAtIndex:setIndex] objectAtIndex:valueIndex];
        switch (self.floatRepresentation) {
            case ComplexNumberFloatRepresentationReal:
                return [complexNumber real];
            case ComplexNumberFloatRepresentationImaginary:
                return [complexNumber imaginary];
            default:
                return [complexNumber magnitude];
        }
    }
}


- (ComplexNumber*) complexValueAtIndex:(int)numberIndex
                                   forSet:(int)setIndex
{
    if (!complex) {
        return nil;
    }
    NSAssert((setIndex >= 0) && (setIndex < [values count]), @"invalid set index");
    let number = [[values objectAtIndex:setIndex] objectAtIndex:numberIndex];
    NSAssert([number isKindOfClass:[ComplexNumber class]], @"Expecting a complex number type.");
    return (ComplexNumber*)number;
}



- (NSArray<NSNumber*>*) valuesOfSet:(int)setIndex;
{
    NSAssert((setIndex >= 0) && (setIndex < [values count]), @"invalid set index");
    return [values objectAtIndex:setIndex];
}

@end

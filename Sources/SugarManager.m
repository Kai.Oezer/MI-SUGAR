/**
*
*   Copyright Kai Özer, 2003-2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import "SugarManager.h"
#import "CircuitDocument.h"
#import "MI_NonlinearElements.h"
#import "MI_LinearElements.h"
#import "MI_MiscellaneousElements.h"
#import "MI_PowerSourceElements.h"
#import "MI_DeviceModelManager.h"
#import "MI_SVGConverter.h"
#import "MI_TextElement.h"
#include <sys/sysctl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <math.h>
#include <CoreFoundation/CFURL.h>

NSString* SUGARML_HEADER = @"<?xml version=\"1.0\"?>\n<SugarData xmlns=\"http://www.macinit.com/misugar\"\nxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\nxsi:schemaLocation=\"http://www.macinit.com/schemas/sugarml.xsd\" ?>";
NSString* MISUGAR_RELEASE_DATE = @"December 17, 2024";
NSString* MISUGAR_WEBSITE = @"https://robo.fish/mi-sugar";
NSString* MISUGAR_CUSTOM_SIMULATOR_PATH = @"SpiceExecutablePath";
NSString* MISUGAR_BuiltinSPICEPath = @"";
NSString* MISUGAR_USE_CUSTOM_SIMULATOR = @"UseCustomSimulator";
NSString* MISUGAR_SOURCE_VIEW_FONT_NAME = @"SourceViewFontName";
NSString* MISUGAR_SOURCE_VIEW_FONT_SIZE = @"SourceViewFontSize";
NSString* MISUGAR_RAW_OUTPUT_VIEW_FONT_NAME = @"RawOutputViewFontName";
NSString* MISUGAR_RAW_OUTPUT_VIEW_FONT_SIZE = @"RawOutputViewFontSize";
NSString* MISUGAR_SHOW_UNTITLED_DOCUMENT_AT_STARTUP = @"ShowUntitledDocumentAtStartup";
NSString* MISUGAR_FONT_CHANGE_NOTIFICATION = @"FontChangeNotification";
NSString* MISUGAR_PLOT_GRAPHS_LINE_WIDTH = @"PlotGraphsLineWidth";
NSString* MISUGAR_PLOT_GRID_LINE_WIDTH = @"PlotGridLineWidth";
NSString* MISUGAR_PLOT_LABELS_FONT_SIZE = @"PlotLabelsFontSize";
NSString* MISUGAR_PLOTTER_BACKGROUND_COLOR = @"PlotterBackgroundColor";
NSString* MISUGAR_PLOTTER_GRID_COLOR = @"PlotterGridColor";
NSString* MISUGAR_PLOT_DEFAULT_COMPLEX_NUMBER_REPRESENTATION = @"DefaultComplexNumberRepresentation";
NSString* MISUGAR_PLOTTER_LABEL_FONT_CHANGE_NOTIFICATION = @"LabelFontChangeNotification";
NSString* MISUGAR_PLOTTER_GRAPHS_LINE_WIDTH_CHANGE_NOTIFICATION = @"GraphLineWidthChangeNotification";
NSString* MISUGAR_PLOTTER_GRID_LINE_WIDTH_CHANGE_NOTIFICATION = @"GridLineWidthChangeNotification";
NSString* MISUGAR_PLOTTER_GRID_COLOR_CHANGE_NOTIFICATION = @"GridColorChangeNotification";
NSString* MISUGAR_PLOTTER_BACKGROUND_CHANGE_NOTIFICATION = @"BackgroundColorChangeNotification";
NSString* MISUGAR_PLOTTER_REMEMBERS_SETTINGS = @"PlotterRemembersSettings";
NSString* MISUGAR_PLOTTER_CLOSES_OLD_WINDOW = @"PlotterClosesOldWindows";
NSString* MISUGAR_PLOTTER_AUTO_SHOW_GUIDES_TAB = @"PlotterAutoShowGuidesTab";
NSString* MISUGAR_PLOTTER_SHOWS_GRID = @"PlotterShowsGrid";
NSString* MISUGAR_PLOTTER_SHOWS_LABELS = @"PlotterShowsLabels";
NSString* MISUGAR_PLOTTER_HAS_LOGARITHMIC_ABSCISSA = @"PlotterHasLogarithmicAbscissa";
NSString* MISUGAR_PLOTTER_HAS_LOGARITHMIC_ORDINATE = @"PlotterHasLogarithmicOrdinate";
NSString* MISUGAR_PLOTTER_HAS_LOG_LABELS_FOR_LOG_SCALE = @"PlotterHasLogLabelsForLogScale";
NSString* MISUGAR_MATHML_ITEM = @"Export Analysis to MathML...";
NSString* MISUGAR_MATLAB_ITEM = @"Export Analysis to Matlab...";
NSString* MISUGAR_TABULAR_TEXT_ITEM = @"Export Analysis to Tabular Text...";
NSString* MISUGAR_SVG_EXPORT_ITEM = @"Export Schematic to SVG...";
NSString* MISUGAR_CAPTURE_ITEM = @"Capture";
NSString* MISUGAR_ANALYZE_ITEM = @"Analyze";
NSString* MISUGAR_PLOT_ITEM = @"Plot";
NSString* MISUGAR_MAKE_SUBCIRCUIT_ITEM = @"Make Subcircuit...";
NSString* MISUGAR_GENERAL_PREFERENCES_ITEM = @"GeneralPrefsToolbarItem";
NSString* MISUGAR_SCHEMATIC_PREFERENCES_ITEM = @"SchematicPrefsToolbarItem";
NSString* MISUGAR_STARTUP_PREFERENCES_ITEM = @"StartupPrefsToolbarItem";
NSString* MISUGAR_FONT_PREFERENCES_ITEM = @"FontPrefsToolbarItem";
NSString* MISUGAR_SIMULATOR_PREFERENCES_ITEM = @"SimulatorPrefsToolbarItem";
NSString* MISUGAR_PLOTTER_PREFERENCES_ITEM = @"PlotterPrefsToolbarItem";
NSString* MISUGAR_LICENSE_KEY = @"Donation Key";
NSString* MISUGAR_DOCUMENT_WINDOW_FRAME = @"DocumentWindowFrame";
NSString* MISUGAR_PLOTTER_WINDOW_FRAME = @"PlotterWindowFrame";
NSString* MISUGAR_DOCUMENT_LAYOUT = @"DocumentLayout";
NSString* MISUGAR_DOCUMENT_FRACTIONAL_SIZE_OF_CANVAS = @"PercentageWidthCanvas";
NSString* MISUGAR_DOCUMENT_FRACTIONAL_HEIGHT_OF_NETLIST_FIELD = @"PercentageHeightNetlist";
NSString* MISUGAR_DOCUMENT_FRACTIONAL_HEIGHT_OF_OUTPUT_FIELD = @"PercentageHeightOutput";
// SCHEMATIC-RELATED
NSString* MISUGAR_ELEMENTS_PANEL_ALPHA = @"ElementsPanelAlpha";
NSString* MISUGAR_INFO_PANEL_ALPHA = @"InfoPanelAlpha";
NSString* MISUGAR_SHOW_PLACEMENT_GUIDES = @"ShowPlacementGuides";
NSString* MISUGAR_AUTOINSERT_NODE_ELEMENT = @"AutoInsertNodeElement";
NSString* MI_SchematicElementPboardType = @"robo.fish.misugar.schematic-element-pasteboard-type";
NSString* MI_SchematicElementsPboardType = @"robo.fish.misugar.schematic-elements-pasteboard-type";
NSString* MISUGAR_CIRCUIT_DEVICE_MODELS = @"Circuit Device Models";
NSString* MISUGAR_PLACEMENT_GUIDE_VISIBILITY_CHANGE_NOTIFICATION = @"PlacementGuideVisibility";
NSString* MISUGAR_ELEMENTS_PANEL_FRAME = @"ElementsPanelFrame";
NSString* MISUGAR_SCHEMATIC_CANVAS_BACKGROUND_COLOR = @"SchematicCanvasBackgroundColor";
NSString* MISUGAR_CANVAS_BACKGROUND_CHANGE_NOTIFICATION = @"CanvasBackgroundNotification";
NSString* MISUGAR_SUBCIRCUIT_LIBRARY_FOLDER = @"SubcircuitLibraryFolderPath";

static SugarManager* managerInstance = nil;

@interface SugarManager () <
    NSToolbarDelegate,
    NSMenuItemValidation,
    NSToolbarItemValidation,
    NSControlTextEditingDelegate,
    NSFontChanging
    >
@end

@implementation SugarManager
{
  IBOutlet NSTextField* customSimulatorField;             // "SugarPreferences.nib"
  IBOutlet NSButton* customSimulatorBrowseButton;         // "SugarPreferences.nib"
  IBOutlet NSButtonCell* useSPICEButton;                  // "SugarPreferences.nib"
  IBOutlet NSButtonCell* useCustomSimulatorButton;        // "SugarPreferences.nib"
  IBOutlet NSPanel* preferencesPanel;                     // "SugarPreferences.nib"
  IBOutlet NSWindow* aboutPanel;                          // "About.nib"
  IBOutlet NSTextField* sourceFontNameField;              // "SugarPreferences.nib"
  IBOutlet NSTextField* rawOutputFontNameField;           // "SugarPreferences.nib"
  IBOutlet NSButton* lookForUpdateAtStartupButton;        // "SugarPreferences.nib"
  IBOutlet NSButton* lookForUpdateButton;                 // "SugarPreferences.nib"
  IBOutlet NSButton* showUntitledDocumentAtStartupButton; // "SugarPreferences.nib"
  IBOutlet NSTextField* updateInfoField;                  // "SugarPreferences.nib"
  IBOutlet NSPopUpButton* plotterGraphsLineWidthChooser;  // "SugarPreferences.nib"
  IBOutlet NSPopUpButton* plotterGridLineWidthChooser;    // "SugarPreferences.nib"
  IBOutlet NSPopUpButton* plotterLabelsFontSizeChooser;   // "SugarPreferences.nib"
  IBOutlet NSColorWell* plotterBackgroundColorChooser;    // "SugarPreferences.nib"
  IBOutlet NSColorWell* plotterGridColorChooser;          // "SugarPreferences.nib"
  IBOutlet NSButton* plotterRemembersSettingsButton;      // "SugarPreferences.nib"
  IBOutlet NSButton* plotterClosesOldWindows;             // "SugarPreferences.nib"
  IBOutlet NSButton* plotterAutoShowsGuidesTab;           // "SugarPreferences.nib"
  IBOutlet NSPopUpButton* conversionPolicyChooser;        // "SugarPreferences.nib"
  IBOutlet NSPopUpButton* fileSavingPolicyChooser;        // "SugarPreferences.nib"
  IBOutlet NSPopUpButton* layoutChooser;                  // "SugarPreferences.nib"
  IBOutlet NSButton* openElementsPanelOnStartup;          // "SugarPreferences.nib"
  IBOutlet NSButton* openInfoPanelOnStartup;              // "SugarPreferences.nib"
  IBOutlet NSButton* autoInsertNodeElement;               // "SugarPreferences.nib"
  IBOutlet NSButton* showPlacementGuides;                 // "SugarPreferences.nib"
  BOOL lookForUpdateAtStartup;
  BOOL settingSourceFont;
  BOOL convertToMathML;
  BOOL settingPlotterBackgroundColor;
  BOOL settingPlotterGridColor;
  NSToolbarItem* startupPreferences;                      // for the preferences panel
  NSToolbarItem* simulatorPreferences;                    // for the preferences panel
  NSToolbarItem* fontPreferences;                         // for the preferences panel
  NSToolbarItem* plotterPreferences;                      // for the preferences panel
  NSToolbarItem* generalPreferences;                      // for the preferences panel
  NSToolbarItem* schematicPreferences;                    // for the preferences panel
  IBOutlet NSView* plotterPrefsView;                      // "SugarPreferences.nib"
  IBOutlet NSView* simulatorPrefsView;                    // "SugarPreferences.nib"
  IBOutlet NSView* fontPrefsView;                         // "SugarPreferences.nib"
  IBOutlet NSView* startupPrefsView;                      // "SugarPreferences.nib"
  IBOutlet NSView* generalPrefsView;                      // "SugarPreferences.nib"
  IBOutlet NSView* schematicPrefsView;                    // "SugarPreferences.nib"

  // ABOUT PANEL
  IBOutlet NSTextField* versionField;
  IBOutlet NSTextField* releaseDateField;
  IBOutlet NSTextField* copyrightField;
  IBOutlet NSTextField* websiteField;

  // SCHEMATIC-RELATED VARIABLES
  IBOutlet NSPanel* elementsPanel;
  IBOutlet MI_SchematicElementChooser* resistorChooser;
  IBOutlet MI_SchematicElementChooser* capacitorChooser;
  IBOutlet MI_SchematicElementChooser* inductorChooser;
  IBOutlet MI_SchematicElementChooser* sourceChooser;
  IBOutlet MI_SchematicElementChooser* transistorChooser;
  IBOutlet MI_SchematicElementChooser* diodeChooser;
  IBOutlet MI_SchematicElementChooser* nodeChooser;
  IBOutlet MI_SchematicElementChooser* groundChooser;
  IBOutlet MI_SchematicElementChooser* switchChooser;
  IBOutlet MI_SchematicElementChooser* subcircuitChooser;
  IBOutlet MI_SchematicElementChooser* specialElementChooser;
  IBOutlet NSSlider* panelTransparencyAdjustment;
  IBOutlet NSOutlineView* subcircuitsTable;               // elements panel
  IBOutlet NSColorWell* schematicCanvasBackground;        // "SugarPreferences.nib"
  IBOutlet NSTextField* subcircuitLibraryPathField;       // "SugarPreferences.nib"
  IBOutlet NSTabView* elementCategoryChooser;             // the main tab view in the elements panel
  IBOutlet NSTextField* subcircuitNamespaceField;         // displays the namespace of the selected subcircuit
  MI_Tool* currentTool;                                   // current tool selected by user
  MI_ScaleTool* scaleTool;
  MI_SelectConnectTool* selectTool;                       // the default tool
  MI_SubcircuitLibraryManager* libManager;

  MI_Inspector* inspector;
}

- (instancetype) init
{
    if (self = [super init])
    {
        NSFont* defaultFont = [NSFont userFontOfSize:0.0f];
        /* Register defaults */
        let defaultsDictionary = [NSMutableDictionary<NSString*,NSObject*> dictionary];
        [defaultsDictionary setObject:@"Horizontal"
                               forKey:MISUGAR_DOCUMENT_LAYOUT];
        [defaultsDictionary setObject:@""
                               forKey:MISUGAR_CUSTOM_SIMULATOR_PATH];
        [defaultsDictionary setObject:[defaultFont fontName]
                               forKey:MISUGAR_SOURCE_VIEW_FONT_NAME];
        [defaultsDictionary setObject:[NSNumber numberWithFloat:[defaultFont pointSize]]
                               forKey:MISUGAR_SOURCE_VIEW_FONT_SIZE];
        [defaultsDictionary setObject:[defaultFont fontName]
                               forKey:MISUGAR_RAW_OUTPUT_VIEW_FONT_NAME];
        [defaultsDictionary setObject:[NSNumber numberWithFloat:[defaultFont pointSize]]
                               forKey:MISUGAR_RAW_OUTPUT_VIEW_FONT_SIZE];
        [defaultsDictionary setObject:[NSNumber numberWithBool:YES]
                               forKey:MISUGAR_SHOW_UNTITLED_DOCUMENT_AT_STARTUP];
        [defaultsDictionary setObject:[NSNumber numberWithChar:0] // chooser list index
                               forKey:MISUGAR_PLOT_GRAPHS_LINE_WIDTH];
        [defaultsDictionary setObject:[NSNumber numberWithChar:0] // chooser list index
                               forKey:MISUGAR_PLOT_GRID_LINE_WIDTH];
        [defaultsDictionary setObject:[NSNumber numberWithChar:1] // chooser list index
                               forKey:MISUGAR_PLOT_LABELS_FONT_SIZE];
        [defaultsDictionary setObject:[NSKeyedArchiver archivedDataWithRootObject: [NSColor colorWithDeviceRed:1.0f green:1.0f blue:1.0f alpha:1.0f]
                                                            requiringSecureCoding:NO
                                                                            error:nil] // color well's color
                               forKey:MISUGAR_PLOTTER_BACKGROUND_COLOR];
        [defaultsDictionary setObject:[NSKeyedArchiver archivedDataWithRootObject:[NSColor colorWithDeviceWhite:0.8f alpha:1.0f]
                                                            requiringSecureCoding:NO
                                                                            error:nil] // color well's color
                               forKey:MISUGAR_PLOTTER_GRID_COLOR];
        [defaultsDictionary setObject:[NSNumber numberWithChar:0] // chooser list index
                               forKey:MISUGAR_PLOT_DEFAULT_COMPLEX_NUMBER_REPRESENTATION];
        [defaultsDictionary setObject:[NSNumber numberWithBool:NO]
                               forKey:MISUGAR_USE_CUSTOM_SIMULATOR];
        [defaultsDictionary setObject:[NSNumber numberWithBool:NO]
                               forKey:MISUGAR_PLOTTER_REMEMBERS_SETTINGS];
        [defaultsDictionary setObject:[NSNumber numberWithBool:NO]
                               forKey:MISUGAR_PLOTTER_CLOSES_OLD_WINDOW];
        [defaultsDictionary setObject:[NSNumber numberWithBool:YES]
                               forKey:MISUGAR_PLOTTER_AUTO_SHOW_GUIDES_TAB];
        [defaultsDictionary setObject:[NSNumber numberWithBool:YES]
                               forKey:MISUGAR_PLOTTER_SHOWS_GRID];
        [defaultsDictionary setObject:[NSNumber numberWithBool:YES]
                               forKey:MISUGAR_PLOTTER_SHOWS_LABELS];
        [defaultsDictionary setObject:[NSNumber numberWithBool:NO]
                               forKey:MISUGAR_PLOTTER_HAS_LOGARITHMIC_ABSCISSA];
        [defaultsDictionary setObject:[NSNumber numberWithBool:NO]
                               forKey:MISUGAR_PLOTTER_HAS_LOGARITHMIC_ORDINATE];
        [defaultsDictionary setObject:[NSNumber numberWithBool:NO]
                               forKey:MISUGAR_PLOTTER_HAS_LOG_LABELS_FOR_LOG_SCALE];
        [defaultsDictionary setObject:[NSNumber numberWithFloat:0.90f]
                               forKey:MISUGAR_ELEMENTS_PANEL_ALPHA];
        [defaultsDictionary setObject:[NSNumber numberWithFloat:0.85f]
                               forKey:MISUGAR_INFO_PANEL_ALPHA];
        [defaultsDictionary setObject:[NSNumber numberWithBool:YES]
                               forKey:MISUGAR_SHOW_PLACEMENT_GUIDES];
        [defaultsDictionary setObject:[NSNumber numberWithBool:YES]
                               forKey:MISUGAR_AUTOINSERT_NODE_ELEMENT];
        [defaultsDictionary setObject:[NSNumber numberWithFloat:0.6f]
                               forKey:MISUGAR_DOCUMENT_FRACTIONAL_SIZE_OF_CANVAS];
        [defaultsDictionary setObject:[NSNumber numberWithFloat:0.5f]
                               forKey:MISUGAR_DOCUMENT_FRACTIONAL_HEIGHT_OF_NETLIST_FIELD];
        [defaultsDictionary setObject:[NSNumber numberWithFloat:0.4f]
                               forKey:MISUGAR_DOCUMENT_FRACTIONAL_HEIGHT_OF_OUTPUT_FIELD];
        [defaultsDictionary setObject:[NSKeyedArchiver archivedDataWithRootObject: [NSColor colorWithDeviceRed:1.0f green:1.0f blue:1.0f alpha:1.0f]
                                                            requiringSecureCoding:NO
                                                                            error:nil] // color well's color
                               forKey:MISUGAR_SCHEMATIC_CANVAS_BACKGROUND_COLOR];
        [defaultsDictionary setObject:[MI_SubcircuitLibraryManager defaultSubcircuitLibraryPath]
                               forKey:MISUGAR_SUBCIRCUIT_LIBRARY_FOLDER];
        
        [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsDictionary];
        settingSourceFont = YES;
        settingPlotterBackgroundColor = NO;
        settingPlotterGridColor = NO;
        elementsPanel = nil;
        selectTool = [[MI_SelectConnectTool alloc] init];
        scaleTool = [[MI_ScaleTool alloc] init];
        currentTool = selectTool;
        inspector = [MI_Inspector sharedInspector];
    }
    return self;
}

- (void) awakeFromNib
{
  // Setting the circuit simulator tool
  MISUGAR_BuiltinSPICEPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingString:@"/spice"];

  // Building the toolbar of the preferences window
  NSToolbar* toolbar = [[NSToolbar alloc] initWithIdentifier:@"PreferencesToolbar"];
  toolbar.allowsUserCustomization = NO;
  toolbar.autosavesConfiguration = NO;
  toolbar.displayMode = NSToolbarDisplayModeIconAndLabel;
  toolbar.delegate = self;
  preferencesPanel.toolbar = toolbar;
  preferencesPanel.toolbarStyle = NSWindowToolbarStylePreference;

  // The device model manager must already exist when the subcircuit
  // library manager is created in order to add the device models,
  // found in the subcircuits, to the device model library

  [MI_DeviceModelManager sharedManager];

  libManager = [[MI_SubcircuitLibraryManager alloc]
      initWithChooserView:subcircuitChooser
                tableView:subcircuitsTable
            namespaceView:subcircuitNamespaceField];
}

// MARK: NSApplicationDelegate

- (BOOL) applicationShouldOpenUntitledFile:(NSApplication*)sender
{
  return [[[NSUserDefaults standardUserDefaults] objectForKey:MISUGAR_SHOW_UNTITLED_DOCUMENT_AT_STARTUP] boolValue];
}

/// Explicit opt-in to secure coding to silence runtime warning.
- (BOOL) applicationSupportsSecureRestorableState:(NSApplication *)app
{
	return YES;
}

// MARK: IBAction

- (IBAction) showPreferencesPanel:(id)sender
{
  if (!preferencesPanel)
  {
    NSUserDefaults* userdefs = [NSUserDefaults standardUserDefaults];
    NSFont* sourceFont =
        [NSFont fontWithName:[userdefs objectForKey:MISUGAR_SOURCE_VIEW_FONT_NAME]
                        size:[[userdefs objectForKey:MISUGAR_SOURCE_VIEW_FONT_SIZE] floatValue]];
    NSFont* rawOutputFont =
        [NSFont fontWithName:[userdefs objectForKey:MISUGAR_RAW_OUTPUT_VIEW_FONT_NAME]
                        size:[[userdefs objectForKey:MISUGAR_RAW_OUTPUT_VIEW_FONT_SIZE] floatValue]];
    [[NSBundle mainBundle] loadNibNamed:@"SugarPreferences" owner:self topLevelObjects:nil];

    BOOL const useCustomSim = [[userdefs objectForKey:MISUGAR_USE_CUSTOM_SIMULATOR] boolValue];
    useCustomSimulatorButton.state = useCustomSim ? NSControlStateValueOn : NSControlStateValueOff;
    useSPICEButton.state = useCustomSim ? NSControlStateValueOff : NSControlStateValueOn;
    customSimulatorField.enabled = useCustomSim;
    customSimulatorBrowseButton.enabled = useCustomSim;

    [layoutChooser selectItemWithTitle:[userdefs objectForKey:MISUGAR_DOCUMENT_LAYOUT]];
    [customSimulatorField setStringValue:(NSString*)[userdefs objectForKey:MISUGAR_CUSTOM_SIMULATOR_PATH]];
    [sourceFontNameField setStringValue:[[sourceFont displayName] stringByAppendingFormat:@" %2.1f", [sourceFont pointSize]]];
    [rawOutputFontNameField setStringValue:[[rawOutputFont displayName] stringByAppendingFormat:@" %2.1f", [rawOutputFont pointSize]]];
    [lookForUpdateAtStartupButton setState:lookForUpdateAtStartup];
    [showUntitledDocumentAtStartupButton setState:([[userdefs objectForKey:MISUGAR_SHOW_UNTITLED_DOCUMENT_AT_STARTUP] boolValue] ? NSControlStateValueOn : NSControlStateValueOff)];
    [plotterGraphsLineWidthChooser selectItemAtIndex:(int)[[userdefs objectForKey:MISUGAR_PLOT_GRAPHS_LINE_WIDTH] charValue]];
    [plotterGridLineWidthChooser selectItemAtIndex:(int)[[userdefs objectForKey:MISUGAR_PLOT_GRID_LINE_WIDTH] charValue]];
    [plotterLabelsFontSizeChooser selectItemAtIndex:(int)[[userdefs objectForKey:MISUGAR_PLOT_LABELS_FONT_SIZE] charValue]];
    [NSColorPanel setPickerMode:NSColorPanelModeHSB];
    NSColor* plotterBackgroundColor = [NSKeyedUnarchiver unarchivedObjectOfClass:[NSColor class] fromData:[userdefs objectForKey:MISUGAR_PLOTTER_BACKGROUND_COLOR] error:nil];
    if (plotterBackgroundColor == nil) { plotterBackgroundColor = [NSColor whiteColor]; }
    [plotterBackgroundColorChooser setColor:plotterBackgroundColor];
    NSColor* plotterGridColor = [NSKeyedUnarchiver unarchivedObjectOfClass:[NSColor class] fromData:[userdefs objectForKey:MISUGAR_PLOTTER_GRID_COLOR] error:nil];
    if (plotterGridColor == nil) { plotterGridColor = [NSColor grayColor]; }
    [plotterGridColorChooser setColor:plotterGridColor];
    [plotterRemembersSettingsButton setState:([[userdefs objectForKey:MISUGAR_PLOTTER_REMEMBERS_SETTINGS] boolValue] ? NSControlStateValueOn : NSControlStateValueOff)];
    [plotterClosesOldWindows setState:([[userdefs objectForKey:MISUGAR_PLOTTER_CLOSES_OLD_WINDOW] boolValue] ? NSControlStateValueOn : NSControlStateValueOff)];
    [plotterAutoShowsGuidesTab setState:([[userdefs objectForKey:MISUGAR_PLOTTER_AUTO_SHOW_GUIDES_TAB] boolValue] ? NSControlStateValueOn : NSControlStateValueOff)];
    [autoInsertNodeElement setState:([[userdefs objectForKey:MISUGAR_AUTOINSERT_NODE_ELEMENT] boolValue] ? NSControlStateValueOn : NSControlStateValueOff)];
    [showPlacementGuides setState:([[userdefs objectForKey:MISUGAR_SHOW_PLACEMENT_GUIDES] boolValue] ? NSControlStateValueOn : NSControlStateValueOff)];
    NSColor* schematicBackgroundColor = [NSKeyedUnarchiver unarchivedObjectOfClass:[NSColor class] fromData:[userdefs objectForKey:MISUGAR_SCHEMATIC_CANVAS_BACKGROUND_COLOR] error:nil];
    if (schematicBackgroundColor == nil) { schematicBackgroundColor = [NSColor whiteColor]; }
    [schematicCanvasBackground setColor:schematicBackgroundColor];
    [subcircuitLibraryPathField setStringValue:[userdefs objectForKey:MISUGAR_SUBCIRCUIT_LIBRARY_FOLDER]];
    [preferencesPanel setContentSize:[generalPrefsView frame].size];
    [preferencesPanel setContentView:generalPrefsView];
    [preferencesPanel setTitle:@"MI-SUGAR General Preferences"];
  }
  [preferencesPanel orderFront:self];
  [preferencesPanel makeKeyWindow];
}


- (IBAction) showAboutPanel:(id)sender
{
  [[NSBundle mainBundle] loadNibNamed:@"About" owner:self topLevelObjects:nil];

  [releaseDateField setStringValue:MISUGAR_RELEASE_DATE];

  let infoDictionary = [[NSBundle mainBundle] infoDictionary];
  NSString* version = [infoDictionary valueForKey:@"CFBundleShortVersionString"];
  NSAssert(version != nil, @"Could not find the marketing version in the info dictionary.");
  [versionField setStringValue:version];
  NSString* copyright = [infoDictionary valueForKey:@"NSHumanReadableCopyright"];
  NSAssert(copyright != nil, @"Could not find the copyright text in the info dictionary");
  [copyrightField setStringValue:copyright];

  NSAttributedString* websiteLink = [[NSAttributedString alloc]
    initWithString: MISUGAR_WEBSITE
    attributes: @{ NSLinkAttributeName : [NSURL URLWithString:MISUGAR_WEBSITE] }
  ];
  [websiteField setAllowsEditingTextAttributes: YES];
  [websiteField setSelectable:YES];
  [websiteField setAttributedStringValue:websiteLink];

  [aboutPanel center];
  [aboutPanel makeKeyAndOrderFront:self];
}


- (IBAction) setCustomSimulator:(id)sender
{
  NSOpenPanel* op = [NSOpenPanel openPanel];
  [op setCanChooseFiles:YES];
  [op setCanChooseDirectories:NO];
  [op setRepresentedFilename:@"spice"];
  [op beginSheetModalForWindow:preferencesPanel completionHandler:^(NSInteger result) {
    if ( result == NSModalResponseOK )
    {
      [self->customSimulatorField setStringValue:[[op URL] path]];
      [[NSUserDefaults standardUserDefaults] setObject:[[op URL] path] forKey:MISUGAR_CUSTOM_SIMULATOR_PATH];
    }
  }];
}


- (IBAction) setSubcircuitLibraryFolder:(id)sender
{
  NSOpenPanel* op = [NSOpenPanel openPanel];
  [op setCanChooseFiles:NO];
  [op setCanChooseDirectories:YES];
  [op setPrompt:@"Select"];
  [op setDirectoryURL:[NSURL fileURLWithPath:[subcircuitLibraryPathField stringValue]]];
  [op beginSheetModalForWindow:preferencesPanel completionHandler:^(NSInteger result) {
    if ( result == NSModalResponseOK )
    {
      [self->subcircuitLibraryPathField setStringValue:[[op URL] path]];
      [[NSUserDefaults standardUserDefaults]
       setObject:[[op URL] path]
       forKey:MISUGAR_SUBCIRCUIT_LIBRARY_FOLDER];
      [self->libManager refreshAll];
    }
  }];
}

// MARK: NSControlTextEditingDelegate

/// Called when the user presses enter in a text field.
- (void) controlTextDidEndEditing:(NSNotification*)aNotification
{
  if ([aNotification object] == customSimulatorField)
  {
      // user has set the new path to the simulator tool
      [[NSUserDefaults standardUserDefaults] setObject:[customSimulatorField stringValue]
                                              forKey:MISUGAR_CUSTOM_SIMULATOR_PATH];
  }
}

// MARK:  IBAction - Font settings

- (IBAction) setSourceFont:(id)sender
{
    NSUserDefaults* userdefs = [NSUserDefaults standardUserDefaults];
    NSFontManager* fmanager = [NSFontManager sharedFontManager];
    NSFont* oldFont =
        [NSFont fontWithName:[userdefs objectForKey:MISUGAR_SOURCE_VIEW_FONT_NAME]
                        size:[[userdefs objectForKey:MISUGAR_SOURCE_VIEW_FONT_SIZE] floatValue]];
    NSFontPanel* panel = [fmanager fontPanel:YES];
    panel.delegate = self;
    [fmanager setSelectedFont:oldFont isMultiple:NO];
    [panel makeFirstResponder:self];
    settingSourceFont = YES;
    [fmanager orderFrontFontPanel:self];
}


- (IBAction) setRawOutputFont:(id)sender
{
  NSUserDefaults* userdefs = [NSUserDefaults standardUserDefaults];
  NSFontManager* fmanager = [NSFontManager sharedFontManager];
  NSFont* oldFont =
    [NSFont fontWithName:[userdefs objectForKey:MISUGAR_RAW_OUTPUT_VIEW_FONT_NAME]
                    size:[[userdefs objectForKey:MISUGAR_RAW_OUTPUT_VIEW_FONT_SIZE] floatValue]];
  settingSourceFont = NO;
  NSFontPanel* panel = [fmanager fontPanel:YES];
  [panel setDelegate:self];
  [fmanager setSelectedFont:oldFont isMultiple:NO];
  [panel makeFirstResponder:self];
  [fmanager orderFrontFontPanel:self];
}


- (void) changeFont:(id)sender
{
    NSFontManager* fmanager = [NSFontManager sharedFontManager];
    NSFont* newFont = [fmanager convertFont:[fmanager selectedFont]];
    NSUserDefaults* userdefs = [NSUserDefaults standardUserDefaults];
    if (settingSourceFont)
    {
        [sourceFontNameField setStringValue:[[newFont displayName] stringByAppendingFormat:@" %2.1f", [newFont pointSize]]];
        [userdefs setObject:[newFont fontName]
                     forKey:MISUGAR_SOURCE_VIEW_FONT_NAME];
        [userdefs setObject:[NSNumber numberWithFloat:[newFont pointSize]]
                     forKey:MISUGAR_SOURCE_VIEW_FONT_SIZE];
    }
    else
    {
        [rawOutputFontNameField setStringValue:[[newFont displayName] stringByAppendingFormat:@" %2.1f", [newFont pointSize]]];
        [userdefs setObject:[newFont fontName]
                     forKey:MISUGAR_RAW_OUTPUT_VIEW_FONT_NAME];
        [userdefs setObject:[NSNumber numberWithFloat:[newFont pointSize]]
                     forKey:MISUGAR_RAW_OUTPUT_VIEW_FONT_SIZE];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:MISUGAR_FONT_CHANGE_NOTIFICATION
                                                        object:[NSNumber numberWithBool:settingSourceFont]];
}


// MARK: NSWindowDelegate

/* This method is used for the proper function of the font panel */
- (void)windowDidBecomeKey:(NSNotification *)aNotification
{
    if ([[aNotification name] isEqualToString:NSWindowDidBecomeKeyNotification])
        if ([[[NSFontManager sharedFontManager] fontPanel:YES] isVisible])
            [preferencesPanel makeFirstResponder:self];
}

// MARK: IBAction - Plotter settings

- (IBAction) setLabelsFontSize:(id)sender
{
    [[NSUserDefaults standardUserDefaults]
        setObject:[NSNumber numberWithChar:[sender indexOfSelectedItem]]
           forKey:MISUGAR_PLOT_LABELS_FONT_SIZE];
    [[NSNotificationCenter defaultCenter]
        postNotificationName:MISUGAR_PLOTTER_LABEL_FONT_CHANGE_NOTIFICATION
                      object:self];
}


- (IBAction) setGraphsLineWidth:(id)sender
{
    [[NSUserDefaults standardUserDefaults]
        setObject:[NSNumber numberWithChar:[sender indexOfSelectedItem]]
           forKey:MISUGAR_PLOT_GRAPHS_LINE_WIDTH];
    [[NSNotificationCenter defaultCenter]
        postNotificationName:MISUGAR_PLOTTER_GRAPHS_LINE_WIDTH_CHANGE_NOTIFICATION
                      object:self];
}


- (IBAction) setGridLineWidth:(id)sender
{
    [[NSUserDefaults standardUserDefaults]
        setObject:[NSNumber numberWithChar:[sender indexOfSelectedItem]]
           forKey:MISUGAR_PLOT_GRID_LINE_WIDTH];
    [[NSNotificationCenter defaultCenter]
        postNotificationName:MISUGAR_PLOTTER_GRID_LINE_WIDTH_CHANGE_NOTIFICATION
                      object:self];
}


- (void) changePlotterColors:(id)sender;
{
    if (settingPlotterBackgroundColor)
    {
        [plotterBackgroundColorChooser setColor:[sender color]];
        [[NSUserDefaults standardUserDefaults]
            setObject:[NSKeyedArchiver archivedDataWithRootObject:[sender color] requiringSecureCoding:NO error:nil]
            forKey:MISUGAR_PLOTTER_BACKGROUND_COLOR];
        [[NSNotificationCenter defaultCenter]
            postNotificationName:MISUGAR_PLOTTER_BACKGROUND_CHANGE_NOTIFICATION
                        object:self];
    }
    else if (settingPlotterGridColor)
    {
        [plotterGridColorChooser setColor:[sender color]];
        [[NSUserDefaults standardUserDefaults]
            setObject:[NSKeyedArchiver archivedDataWithRootObject:[sender color] requiringSecureCoding:NO error:nil]
            forKey:MISUGAR_PLOTTER_GRID_COLOR];
        [[NSNotificationCenter defaultCenter]
            postNotificationName:MISUGAR_PLOTTER_GRID_COLOR_CHANGE_NOTIFICATION
                        object:self];
    }
}


- (IBAction) setPlotterBackgroundColor:(id)sender
{
    NSColorPanel* cp = [NSColorPanel sharedColorPanel];
    settingPlotterBackgroundColor = YES;
    settingPlotterGridColor = NO;
    [cp setTarget:self];
    [cp setAction:@selector(changePlotterColors:)];
    [cp setDelegate:self];
    [cp makeFirstResponder:nil];
    [cp setColor:[plotterBackgroundColorChooser color]];
    [NSColorPanel setPickerMode:NSColorPanelModeHSB];
    [cp makeKeyAndOrderFront:self];
}

- (IBAction) setPlotterGridColor:(id)sender
{
    NSColorPanel* cp = [NSColorPanel sharedColorPanel];
    settingPlotterBackgroundColor = NO;
    settingPlotterGridColor = YES;
    [cp setTarget:self];
    [cp setAction:@selector(changePlotterColors:)];
    [cp setDelegate:self];
    [cp makeFirstResponder:nil];
    [cp setColor:[plotterGridColorChooser color]];
    [NSColorPanel setPickerMode:NSColorPanelModeHSB];
    [cp makeKeyAndOrderFront:self];
}

- (IBAction) setPlotterFunctionSetting:(id)sender
{
    if (sender == plotterRemembersSettingsButton)
        [[NSUserDefaults standardUserDefaults]
            setObject:[NSNumber numberWithBool:([plotterRemembersSettingsButton state] == NSControlStateValueOn)]
               forKey:MISUGAR_PLOTTER_REMEMBERS_SETTINGS];
    else if (sender == plotterClosesOldWindows)
        [[NSUserDefaults standardUserDefaults]
            setObject:[NSNumber numberWithBool:([plotterClosesOldWindows state] == NSControlStateValueOn)]
               forKey:MISUGAR_PLOTTER_CLOSES_OLD_WINDOW];
    else /* if (sender == plotterAutoShowsGuidesTab) */
        [[NSUserDefaults standardUserDefaults]
            setObject:[NSNumber numberWithBool:([plotterAutoShowsGuidesTab state] == NSControlStateValueOn)]
               forKey:MISUGAR_PLOTTER_AUTO_SHOW_GUIDES_TAB];
}

// MARK: IBAction

- (IBAction) setShowUntitledDocumentAtStartup:(id)sender
{
    [[NSUserDefaults standardUserDefaults]
        setObject:[NSNumber numberWithBool:([showUntitledDocumentAtStartupButton state] == NSControlStateValueOn)]
           forKey:MISUGAR_SHOW_UNTITLED_DOCUMENT_AT_STARTUP];    
}

- (IBAction) selectSimulator:(id)sender
{
    BOOL useCustomSim = ([sender selectedCell] == useCustomSimulatorButton);
    [[NSUserDefaults standardUserDefaults]
        setObject:[NSNumber numberWithBool:useCustomSim]
           forKey:MISUGAR_USE_CUSTOM_SIMULATOR];
    if (useCustomSim)
    {
        [customSimulatorField setEnabled:YES];
        [customSimulatorBrowseButton setEnabled:YES];
    }
    else
    {
        [customSimulatorField setEnabled:NO];
        [customSimulatorBrowseButton setEnabled:NO];
    }
}

// MARK: IBAction - Exporting

- (IBAction) exportToMathML:(id)sender
{
    NSWindow* documentWindow = [NSApp mainWindow];
    convertToMathML = YES;
    if (documentWindow && [[documentWindow delegate] isKindOfClass:[CircuitDocument class]])
    {
      NSSavePanel* savePanel = [NSSavePanel savePanel];
      [savePanel setRepresentedFilename:[[documentWindow title] stringByAppendingString:@".mml"]];
      [savePanel beginSheetModalForWindow:documentWindow completionHandler:^(NSInteger result) {
        if ( (result == NSModalResponseOK) && [[[NSApp mainWindow] delegate] isKindOfClass:[CircuitDocument class]] )
        {
          [(CircuitDocument*)[[NSApp mainWindow] delegate] export:@"MathML" toFile:[[[savePanel URL] path] stringByDeletingPathExtension]];
        }
      }];
    }
}


- (IBAction) exportToMatlab:(id)sender
{
    NSWindow* documentWindow = [NSApp mainWindow];
    convertToMathML = NO;
    if (documentWindow && [[documentWindow delegate] isKindOfClass:[CircuitDocument class]])
    {
      NSSavePanel* savePanel = [NSSavePanel savePanel];
      [savePanel setRepresentedFilename:[[documentWindow title] stringByAppendingString:@".m"]];
      [savePanel beginSheetModalForWindow:documentWindow completionHandler:^(NSInteger result) {
        if ( (result == NSModalResponseOK) && [[[NSApp mainWindow] delegate] isKindOfClass:[CircuitDocument class]] )
        {
          [(CircuitDocument*)[[NSApp mainWindow] delegate] export:@"Matlab" toFile:[[[savePanel URL] path] stringByDeletingPathExtension]];
        }
      }];
    }
}


- (IBAction) exportToTabularText:(id)sender
{
    NSWindow* documentWindow = [NSApp mainWindow];
    if (documentWindow && [[documentWindow delegate] isKindOfClass:[CircuitDocument class]])
    {
      NSSavePanel* savePanel = [NSSavePanel savePanel];
      [savePanel setRepresentedFilename:[[documentWindow title] stringByAppendingString:@".txt"]];
      [savePanel beginSheetModalForWindow:documentWindow completionHandler:^(NSInteger result) {
        if ( (result == NSModalResponseOK) && [[[NSApp mainWindow] delegate] isKindOfClass:[CircuitDocument class]] )
        {
          [(CircuitDocument*)[[NSApp mainWindow] delegate] export:@"Tabular" toFile:[[[savePanel URL] path] stringByDeletingPathExtension]];
        }        
      }];
    }
}

// MARK: NSMenuItemValidation

- (BOOL) validateMenuItem:(NSMenuItem*)item
{
    if ([[item title] isEqualToString:MISUGAR_MATHML_ITEM] ||
        [[item title] isEqualToString:MISUGAR_MATLAB_ITEM] ||
        [[item title] isEqualToString:MISUGAR_TABULAR_TEXT_ITEM] ||
        [[item title] isEqualToString:MISUGAR_CAPTURE_ITEM] ||
        [[item title] isEqualToString:MISUGAR_ANALYZE_ITEM] ||
        [[item title] isEqualToString:MISUGAR_MAKE_SUBCIRCUIT_ITEM] ||
        [[item title] isEqualToString:MISUGAR_PLOT_ITEM] ||
        [[item title] isEqualToString:MISUGAR_SVG_EXPORT_ITEM])
    {
        NSWindow* win;
        if ((win = [NSApp mainWindow]) &&
            ([[win delegate] isKindOfClass:[CircuitDocument class]])) // This check is necessary to avoid infinite loops due to the use of the font panel
            return [(CircuitDocument*)[win delegate] validateMenuItem:item];
        else
            return NO;
    }
    else
        return YES;
}

// MARK: Circuit processing

- (IBAction) analyzeCircuit:(id)sender
{
    NSWindow* documentWindow = [NSApp mainWindow];
    if (documentWindow &&
        [[documentWindow delegate] isKindOfClass:[CircuitDocument class]] &&
        [(CircuitDocument*)[documentWindow delegate] model])
        [(CircuitDocument*)[documentWindow delegate] runSimulator:nil];
}

- (IBAction) plotCircuitAnalysis:(id)sender
{
    NSWindow* documentWindow = [NSApp mainWindow];
    if (documentWindow &&
        [[documentWindow delegate] isKindOfClass:[CircuitDocument class]] &&
        [(CircuitDocument*)[documentWindow delegate] model])
        [(CircuitDocument*)[documentWindow delegate] plotResults:nil];
}


- (IBAction) makeSubcircuit:(id)sender
{
    NSWindow* documentWindow = [NSApp mainWindow];
    if (documentWindow &&
        [[documentWindow delegate] isKindOfClass:[CircuitDocument class]] &&
        [(CircuitDocument*)[documentWindow delegate] model])
        [(CircuitDocument*)[documentWindow delegate] makeSubcircuit];
}


- (IBAction) schematicToSVG:(id)sender
{
    NSWindow* documentWindow = [NSApp mainWindow];
    if (documentWindow &&
        [[documentWindow delegate] isKindOfClass:[CircuitDocument class]])
    {
        NSSavePanel* sp = [NSSavePanel savePanel];
        [sp setAllowedFileTypes:@[@"svg"]];
        [sp setCanCreateDirectories:YES];
        [sp setCanSelectHiddenExtension:NO];
        [sp setRepresentedFilename:[documentWindow title]];
        [sp beginSheetModalForWindow:documentWindow completionHandler:^(NSInteger returnCode) {
          if (returnCode == NSModalResponseOK)
          {
            if ( [[[NSApp mainWindow] delegate] isKindOfClass:[CircuitDocument class]] )
            {
              MI_Schematic* s = [[(CircuitDocument*)[[NSApp mainWindow] delegate] model] schematic];
              [(NSString*)[MI_SVGConverter schematicToSVG:s] writeToURL:[sp URL] atomically:YES encoding:NSASCIIStringEncoding error:nil];
            }
          }
        }];
    }
}

// MARK: Toolbar methods

/* toolbar delegate method */
- (NSArray<NSString*>*) toolbarDefaultItemIdentifiers:(NSToolbar*)toolbar
{
  return @[
    MISUGAR_GENERAL_PREFERENCES_ITEM, MISUGAR_SCHEMATIC_PREFERENCES_ITEM,
    MISUGAR_STARTUP_PREFERENCES_ITEM, MISUGAR_SIMULATOR_PREFERENCES_ITEM,
    MISUGAR_PLOTTER_PREFERENCES_ITEM, MISUGAR_FONT_PREFERENCES_ITEM
  ];
}

/* toolbar delegate method */
- (NSArray<NSString*>*) toolbarAllowedItemIdentifiers:(NSToolbar*)toolbar
{
  return @[
    MISUGAR_GENERAL_PREFERENCES_ITEM, MISUGAR_SCHEMATIC_PREFERENCES_ITEM,
    MISUGAR_STARTUP_PREFERENCES_ITEM, MISUGAR_SIMULATOR_PREFERENCES_ITEM,
    MISUGAR_PLOTTER_PREFERENCES_ITEM, MISUGAR_FONT_PREFERENCES_ITEM
  ];
}

/* toolbar delegate method */
- (NSToolbarItem *)toolbar:(NSToolbar*)toolbar
     itemForItemIdentifier:(NSString*)itemIdentifier
 willBeInsertedIntoToolbar:(BOOL)flag
{
  if ([itemIdentifier isEqualToString:MISUGAR_GENERAL_PREFERENCES_ITEM])
  {
    generalPreferences = [[NSToolbarItem alloc] initWithItemIdentifier:MISUGAR_GENERAL_PREFERENCES_ITEM];
    [generalPreferences setLabel:@"General"];
    [generalPreferences setAction:@selector(switchPreferenceView:)];
    [generalPreferences setTarget:self];
    [generalPreferences setToolTip:@"General preferences"];
    [generalPreferences setImage:[NSImage imageWithSystemSymbolName:@"slider.horizontal.3" accessibilityDescription:nil]];
    return generalPreferences;
  }
  if ([itemIdentifier isEqualToString:MISUGAR_SCHEMATIC_PREFERENCES_ITEM])
  {
    schematicPreferences = [[NSToolbarItem alloc] initWithItemIdentifier:MISUGAR_SCHEMATIC_PREFERENCES_ITEM];
    [schematicPreferences setLabel:@"Schematic"];
    [schematicPreferences setAction:@selector(switchPreferenceView:)];
    [schematicPreferences setTarget:self];
    [schematicPreferences setToolTip:@"Schematic preferences"];
    [schematicPreferences setImage:[NSImage imageNamed:@"schematic_pref_button"]];
    return schematicPreferences;
  }
  else if ([itemIdentifier isEqualToString:MISUGAR_STARTUP_PREFERENCES_ITEM])
  {
    startupPreferences = [[NSToolbarItem alloc] initWithItemIdentifier:MISUGAR_STARTUP_PREFERENCES_ITEM];
    [startupPreferences setLabel:@"Startup"];
    [startupPreferences setAction:@selector(switchPreferenceView:)];
    [startupPreferences setTarget:self];
    [startupPreferences setToolTip:@"Startup preferences"];
    [startupPreferences setImage:[NSImage imageWithSystemSymbolName:@"arrow.turn.right.up" accessibilityDescription:nil]];
    return startupPreferences;
  }
  else if ([itemIdentifier isEqualToString:MISUGAR_FONT_PREFERENCES_ITEM])
  {
    fontPreferences = [[NSToolbarItem alloc] initWithItemIdentifier:MISUGAR_FONT_PREFERENCES_ITEM];
    [fontPreferences setLabel:@"Fonts"];
    [fontPreferences setAction:@selector(switchPreferenceView:)];
    [fontPreferences setTarget:self];
    [fontPreferences setToolTip:@"Font preferences"];
    [fontPreferences setImage:[NSImage imageWithSystemSymbolName:@"textformat.size" accessibilityDescription:nil]];
    return fontPreferences;
  }
  if ([itemIdentifier isEqualToString:MISUGAR_SIMULATOR_PREFERENCES_ITEM])
  {
    simulatorPreferences = [[NSToolbarItem alloc] initWithItemIdentifier:MISUGAR_SIMULATOR_PREFERENCES_ITEM];
    [simulatorPreferences setLabel:@"Simulator"];
    [simulatorPreferences setAction:@selector(switchPreferenceView:)];
    [simulatorPreferences setTarget:self];
    [simulatorPreferences setToolTip:@"Simulator preferences"];
    [simulatorPreferences setImage:[NSImage imageWithSystemSymbolName:@"gearshape.2" accessibilityDescription:nil]] ;
    return simulatorPreferences;
  }
  else if ([itemIdentifier isEqualToString:MISUGAR_PLOTTER_PREFERENCES_ITEM])
  {
    plotterPreferences = [[NSToolbarItem alloc] initWithItemIdentifier:MISUGAR_PLOTTER_PREFERENCES_ITEM];
    [plotterPreferences setLabel:@"Plotter"];
    [plotterPreferences setAction:@selector(switchPreferenceView:)];
    [plotterPreferences setTarget:self];
    [plotterPreferences setToolTip:@"Plotter preferences"];
    [plotterPreferences setImage:[NSImage imageWithSystemSymbolName:@"waveform.path.ecg.rectangle" accessibilityDescription:nil]];
    return plotterPreferences;
  }
  else
    return nil;
}

/* NSToolbarItemValidation */
- (BOOL) validateToolbarItem:(NSToolbarItem *)theItem
{
    return YES;
}


- (void) windowWillClose:(NSNotification *)aNotification
{
    id p = [aNotification object];
    if ([p isKindOfClass:[NSColorPanel class]] ||
        [p isKindOfClass:[NSFontPanel class]])
        [p setTarget:nil];
}


- (IBAction) switchPreferenceView:(id)sender
{
    float newHeight, newWidth;
    NSRect newFrame;
    float toolbarHeight;

    [preferencesPanel setTitle:@""];
    NSView* emptyView = [[NSView alloc] init];
    newFrame = [NSWindow contentRectForFrameRect:[preferencesPanel frame]
                                       styleMask:[preferencesPanel styleMask]];
    toolbarHeight =
        NSHeight(newFrame) - NSHeight([[preferencesPanel contentView] frame]);

    if ( (sender == generalPreferences) &&
         ([preferencesPanel contentView] != generalPrefsView) )
    {
        newHeight = [generalPrefsView bounds].size.height;
        newWidth = [generalPrefsView bounds].size.width;
    }
    else if ( (sender == schematicPreferences) &&
              ([preferencesPanel contentView] != schematicPrefsView) )
    {
        newHeight = [schematicPrefsView bounds].size.height;
        newWidth = [schematicPrefsView bounds].size.width;
    }
    else if ( (sender == startupPreferences) &&
         ([preferencesPanel contentView] != startupPrefsView) )
    {
        newHeight = [startupPrefsView bounds].size.height;
        newWidth = [startupPrefsView bounds].size.width;
    }
    else if  ( (sender == simulatorPreferences) &&
               ([preferencesPanel contentView] != simulatorPrefsView) )
    {
        newHeight = [simulatorPrefsView bounds].size.height;
        newWidth = [simulatorPrefsView bounds].size.width;
    }
    else if ( (sender == fontPreferences) &&
              ([preferencesPanel contentView] != fontPrefsView) )
    {
        newHeight = [fontPrefsView bounds].size.height;
        newWidth = [fontPrefsView bounds].size.width;
    }
    else if ( (sender == plotterPreferences) &&
              ([preferencesPanel contentView] != plotterPrefsView) )
    {
        newHeight = [plotterPrefsView bounds].size.height;
        newWidth = [plotterPrefsView bounds].size.width;
    }
    else
        return;
    
    [preferencesPanel setContentView:emptyView];
    newFrame.origin.y += newFrame.size.height - toolbarHeight - newHeight;
    newFrame.size.height = newHeight + toolbarHeight;
    //newFrame.size.width = newWidth;
    newFrame =
        [NSWindow frameRectForContentRect:newFrame
                                styleMask:[preferencesPanel styleMask]];
    [preferencesPanel setFrame:newFrame
                       display:YES
                       animate:YES];

    if (sender == generalPreferences) {
        [preferencesPanel setContentView:generalPrefsView];
        [preferencesPanel setTitle:@"MI-SUGAR General Preferences"];
    }
    else if (sender == schematicPreferences) {
        [preferencesPanel setContentView:schematicPrefsView];
        [preferencesPanel setTitle:@"MI-SUGAR Schematic Preferences"];
    }
    else if (sender == startupPreferences) {
        [preferencesPanel setContentView:startupPrefsView];
        [preferencesPanel setTitle:@"MI-SUGAR Startup Preferences"];
    }
    else if (sender == simulatorPreferences) {
        [preferencesPanel setContentView:simulatorPrefsView];
        [preferencesPanel setTitle:@"MI-SUGAR Simulator Preferences"];
    }
    else if (sender == fontPreferences) {
        [preferencesPanel setContentView:fontPrefsView];
        [preferencesPanel setTitle:@"MI-SUGAR Font Preferences"];
    }
    else if (sender == plotterPreferences) {
        [preferencesPanel setContentView:plotterPrefsView];
        [preferencesPanel setTitle:@"MI-SUGAR Plotter Preferences"];
    }
}

// MARK: License-related methods

- (IBAction) setLayout:(id)sender
{
    [[NSUserDefaults standardUserDefaults]
        setObject:[[layoutChooser selectedItem] title]
           forKey:MISUGAR_DOCUMENT_LAYOUT];
}

// MARK: NSApplicationDelegate

- (NSApplicationTerminateReply) applicationShouldTerminate:(NSApplication*)sender
{
    // save the size of the key document window (if any)
    [[NSDocumentController sharedDocumentController]
        closeAllDocumentsWithDelegate:nil
                  didCloseAllSelector:nil
                          contextInfo:nil];
    if (elementsPanel != nil)
    {
        [elementsPanel saveFrameUsingName:MISUGAR_ELEMENTS_PANEL_FRAME];
        [[NSUserDefaults standardUserDefaults]
            setObject:[NSNumber numberWithFloat:[elementsPanel alphaValue]]
               forKey:MISUGAR_ELEMENTS_PANEL_ALPHA];
    }

    [[MI_DeviceModelManager sharedManager] saveModels];
    
    inspector = nil; // releasing 'inspector' will save the size and position of the info panel

    return NSTerminateNow;
}

// MARK: Schematic-related methods

- (IBAction) captureCurrentSchematic:(id)sender
{
  NSWindow* documentWindow = [NSApp mainWindow];
  if (documentWindow && [[documentWindow delegate] isKindOfClass:[CircuitDocument class]])
  {
    [[documentWindow delegate] performSelector:@selector(convertSchematicToNetlist:) withObject:sender];
  }
}

- (IBAction) goToSubcircuitsFolder:(id)sender
{
  NSString* libPath = [[NSUserDefaults standardUserDefaults] objectForKey:MISUGAR_SUBCIRCUIT_LIBRARY_FOLDER];
  NSURL* libLocation = [NSURL fileURLWithPath:libPath];
  if (libLocation == NULL) { return; }
  [[NSWorkspace sharedWorkspace] openURL:libLocation configuration:[NSWorkspaceOpenConfiguration configuration] completionHandler:NULL];
  /* This dead code uses AppleScript and Core Foundation to open the subcircuit folder
  // Using Core Foundation URL functions to convert the subcircuit folder path from Unix to HFS representation
  CFURLRef ref = CFURLCreateWithFileSystemPath(NULL,
      CFStringCreateWithCString(NULL, [libPath cString], kCFStringEncodingUTF8),
      kCFURLPOSIXPathStyle, 1);
  CFStringRef hfs = CFURLCopyFileSystemPath(ref, kCFURLHFSPathStyle);
  CFIndex bufferSize = 4 * CFStringGetLength(hfs) + 1;
  char* hfsString = (char*) malloc((int)bufferSize);
  if (!CFStringGetCString(hfs, hfsString, bufferSize, kCFStringEncodingUTF8))
      NSLog(@"Error occured during conversion of subcircuit folder path from POSIX format to HFS format.");
  else
  {
    libPath = [NSString stringWithUTF8String:(const char*)hfsString];
    // Use AppleScript to open the Finder and direct it to the subcircuits folder
    NSDictionary** errorDict = NULL;
    NSString* command = [NSString stringWithFormat:
        @"tell application \"Finder\"\nreveal folder \"%@\"\nactivate\nend tell", libPath];
    NSAppleScript* subcktFolderOpenScript = [[NSAppleScript alloc] initWithSource:command];
    [subcktFolderOpenScript compileAndReturnError:errorDict];
    [subcktFolderOpenScript executeAndReturnError:errorDict];
  }
  free(hfsString);
  */
}

- (IBAction) refreshSubcircuitsTable:(id)sender
{
    [libManager refreshAll];
}

- (IBAction) showPlacementGuides:(id)sender
{
    [[NSNotificationCenter defaultCenter]
        postNotificationName:MISUGAR_PLACEMENT_GUIDE_VISIBILITY_CHANGE_NOTIFICATION
                      object:[NSNumber numberWithBool:([showPlacementGuides state] == NSControlStateValueOn)]];
    [[NSUserDefaults standardUserDefaults]
            setObject:[NSNumber numberWithBool:([sender state] == NSControlStateValueOn)]
               forKey:MISUGAR_SHOW_PLACEMENT_GUIDES];
}

- (IBAction) showInfoPanel:(id)sender
{
    [inspector toggleInfoPanel];
}

- (IBAction) showElementsPanel:(id)sender
{
    if (elementsPanel == nil)
        [self prepareElementsPanel];
    if ([elementsPanel isVisible])
        [elementsPanel orderOut:self];
    else
        [elementsPanel orderFront:self];
}

- (void) prepareElementsPanel
{
    float alpha = [[[NSUserDefaults standardUserDefaults] objectForKey:MISUGAR_ELEMENTS_PANEL_ALPHA] floatValue];
    let transistorElements = @[
        [MI_NPNTransistorElement new],
        [MI_PNPTransistorElement new],
        [MI_NJFETTransistorElement new],
        [MI_PJFETTransistorElement new],
        [MI_EnhancementNMOSTransistorElement new],
        [MI_EnhancementPMOSTransistorElement new],
        [MI_DepletionNMOSTransistorElement new],
        [MI_DepletionPMOSTransistorElement new],
        [MI_EnhancementNMOSwBulkTransistorElement new],
        [MI_EnhancementPMOSwBulkTransistorElement new],
        [MI_DepletionNMOSwBulkTransistorElement new],
        [MI_DepletionPMOSwBulkTransistorElement new]
    ];
    let diodeElements = @[
        [MI_DiodeElement new],
        [MI_ZenerDiodeElement new],
        [MI_LightEmittingDiodeElement new],
        [MI_PhotoDiodeElement new]
    ];
    let resistorElements = @[
        [MI_Resistor_US_Element new],
        [MI_Resistor_IEC_Element new],
        [MI_Rheostat_US_Element new],
        [MI_Rheostat_IEC_Element new]
    ];
    let inductorElements = @[
        [MI_Inductor_US_Element new],
        [MI_Inductor_IEC_Element new]
    ];
    let capacitorElements = @[
        [MI_CapacitorElement new],
        [MI_PolarizedCapacitorElement new],
    ];
    let nodeElements = @[
        [MI_NodeElement new],
        [MI_SpikyNodeElement new],
    ];
    let switchElements = @[
        [MI_VoltageControlledSwitchElement new],
        [MI_Transformer_US_Element new],
        [MI_Transformer_IEC_Element new],
        [MI_TransmissionLineElement new],
    ];
    let groundElements = @[
        [MI_GroundElement new],
        [MI_PlainGroundElement new],
    ];
    let powerSourceElements = @[
      [MI_DCVoltageSourceElement new],
      [MI_ACVoltageSourceElement new],
      [MI_PulseVoltageSourceElement new],
      [MI_SinusoidalVoltageSourceElement new],
      [MI_CurrentSourceElement new],
      [MI_PulseCurrentSourceElement new],
      [MI_VoltageControlledCurrentSource new],
      [MI_VoltageControlledVoltageSource new],
      [MI_CurrentControlledCurrentSource new],
      [MI_CurrentControlledVoltageSource new],
      [MI_NonlinearDependentSource new],
    ];
    MI_TextElement* te = [[MI_TextElement alloc] init];
    [te setFont:[NSFont fontWithName:@"Lucida Grande" size:14.0f]];
    [te lock];
    let specialElements = @[te];
    [[NSBundle mainBundle] loadNibNamed:@"SchematicElementsPanel" owner:self topLevelObjects:nil];
    [transistorChooser setSchematicElementList:transistorElements];
    [diodeChooser setSchematicElementList:diodeElements];
    [resistorChooser setSchematicElementList:resistorElements];
    [inductorChooser setSchematicElementList:inductorElements];
    [capacitorChooser setSchematicElementList:capacitorElements];
    [nodeChooser setSchematicElementList:nodeElements];
    [groundChooser setSchematicElementList:groundElements];
    [sourceChooser setSchematicElementList:powerSourceElements];
    [switchChooser setSchematicElementList:switchElements];
    [specialElementChooser setSchematicElementList:specialElements];

    [subcircuitsTable setDataSource:libManager];
    [subcircuitsTable setDelegate:libManager];
    [subcircuitsTable setTarget:libManager];
    [subcircuitsTable setDoubleAction:@selector(showDefinitionOfSelectedSubcircuit:)];
    [subcircuitsTable setBackgroundColor:[NSColor windowBackgroundColor]];

    [elementsPanel setBecomesKeyOnlyIfNeeded:YES];
    [elementsPanel setFloatingPanel:YES];
    [elementsPanel setAlphaValue:alpha];
    [panelTransparencyAdjustment setFloatValue:alpha];
    //[elementsPanel setMovableByWindowBackground:YES]; // causes havoc on drag & drop
    [elementsPanel setFrameUsingName:MISUGAR_ELEMENTS_PANEL_FRAME];
}

- (IBAction) showDeviceModelPanel:(id)sender
{
    [[MI_DeviceModelManager sharedManager] togglePanel];
}

- (IBAction) setElementsPanelTransparency:(id)sender
{
    float value = [sender floatValue];
    if (value > 1.0f)
        value = 1.0f;
    else if (value < 0.5f)
        value = 0.5f;
    [elementsPanel setAlphaValue:value];
}

- (IBAction) setNodeAutoInsertion:(id)sender
{
    [[NSUserDefaults standardUserDefaults]
        setObject:[NSNumber numberWithBool:([sender state] == NSControlStateValueOn)]
           forKey:MISUGAR_AUTOINSERT_NODE_ELEMENT];
}

- (IBAction) setCanvasBackgroundColor:(id)sender
{
    let cp = [NSColorPanel sharedColorPanel];
    [cp setTarget:self];
    [cp setAction:@selector(changeCanvasBackgroundColor:)];
    [cp makeFirstResponder:nil];
    [cp setDelegate:self];
    [cp setColor:[schematicCanvasBackground color]];
    [NSColorPanel setPickerMode:NSColorPanelModeHSB];
    [cp makeKeyAndOrderFront:self];
}

- (void) changeCanvasBackgroundColor:(id)sender
{
    [schematicCanvasBackground setColor:[sender color]];
    [[NSUserDefaults standardUserDefaults]
        setObject:[NSKeyedArchiver archivedDataWithRootObject:[sender color] requiringSecureCoding:NO error:nil]
           forKey:MISUGAR_SCHEMATIC_CANVAS_BACKGROUND_COLOR];
    // Notify all windows
    [[NSNotificationCenter defaultCenter]
        postNotificationName:MISUGAR_CANVAS_BACKGROUND_CHANGE_NOTIFICATION
                      object:[sender color]];
}

- (MI_Tool*) currentTool
{
    return currentTool;
}

- (MI_SubcircuitLibraryManager*) subcircuitLibraryManager
{
    return libManager;
}


// MARK: NSTabViewController

// The delegate method of the tab view in the elements panel.
// Causes the elements panel to be stretched when showing the subcircuit list.
- (void)tabView:(NSTabView *)tabView
willSelectTabViewItem:(NSTabViewItem *)tabViewItem
{
    if (tabView == elementCategoryChooser)
    {
        float oldHeight = [elementsPanel frame].size.height;
        float newHeight = [[tabViewItem identifier] isEqualToString:@"basic"] ? 300.0f : 450.0f;
        
        [elementCategoryChooser setHidden:YES];
        
        [elementsPanel setFrame:NSMakeRect([elementsPanel frame].origin.x,
                                           [elementsPanel frame].origin.y + oldHeight - newHeight,
                                           [elementsPanel frame].size.width,
                                           newHeight)
                        display:YES
                        animate:YES];
        
        [elementCategoryChooser setHidden:NO];
    }
}

// MARK: Other

+ (NSString*) supportFolder
{
    NSString* theSupportFolder = nil;
    
    let appSupportDirectories = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    if ( [appSupportDirectories count] > 0 )
    {
        theSupportFolder = [[appSupportDirectories objectAtIndex:0] stringByAppendingPathComponent:@"robo.fish.mi-sugar"];
    }
    else
    {
        theSupportFolder = NSHomeDirectory();
    }
    return theSupportFolder;
}

@end

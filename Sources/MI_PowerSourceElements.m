/**
*
*   Copyright Kai Özer, 2003-2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import "MI_PowerSourceElements.h"
#import "MI_ConnectionPoint.h"

@implementation MI_DCVoltageSourceElement

- (instancetype) init
{
  if (self = [super initWithSize:NSMakeSize(32.0f, 48.0f)])
  {
    self.name = @"DC Voltage Source";
    self.label = @"Vdc";
    self.labelPosition = MI_DirectionRight;
    let anode = [[MI_ConnectionPoint alloc]
        initWithPosition:NSMakePoint(0.0f, 24.0f)
                    size:NSMakeSize(6.0f, 6.0f)
                    name:@"Anode"
     nodeNumberPlacement:MI_DirectionNortheast];
    let cathode = [[MI_ConnectionPoint alloc]
        initWithPosition:NSMakePoint(0.0f, -24.0f)
                    size:NSMakeSize(6.0f, 6.0f)
                    name:@"Cathode"
     nodeNumberPlacement:MI_DirectionSoutheast];
    self.connectionPoints = @{@"Anode": anode, @"Cathode": cathode};
    self.parameters[@"Voltage"] = @"12.0";
  }
  return self;
}

- (void) draw
{
  [super draw];
  let bp = [NSBezierPath bezierPath];
  [bp moveToPoint:NSMakePoint(self.position.x, self.position.y + 24.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  0.0f,  -6.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  0.0f, -18.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp relativeMoveToPoint:NSMakePoint( -4.0f, +30.0f)];
  [bp relativeLineToPoint:NSMakePoint(  8.0f,   0.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  0.0f, -14.0f)];
  [bp relativeLineToPoint:NSMakePoint( -8.0f,   0.0f)];
  [bp stroke];
  [[NSBezierPath bezierPathWithOvalInRect:NSMakeRect(self.position.x - 16.0f, self.position.y - 16.0f, 32.0f, 32.0f)] stroke];
  [super endDraw];
}

- (NSString*) shapeToSVG
{
    let svg = [NSMutableString stringWithCapacity:100];
    [svg appendFormat:@"%@<path stroke=\"black\" fill=\"none\" d=\"M %g %g v -8 m 0 -6 v -8 m 0 -18 v -8 m -4 30 h 8 m 0 -14 h -8\"/>",
        [super shapeToSVG], self.position.x, self.position.y + 24.0f];
    [svg appendFormat:@"\n<circle stroke=\"black\" fill=\"none\" cx=\"%g\" cy=\"%g\" r=\"16\"/>%@",
        self.position.x, self.position.y, [super endSVG]];
    return svg;
}

@end

// MARK: -

@implementation MI_ACVoltageSourceElement

- (instancetype) init
{
  if (self = [super initWithSize:NSMakeSize(32.0f, 48.0f)])
  {
    self.name = @"AC Voltage Source";
    self.label = @"Vac";
    self.labelPosition = MI_DirectionRight;
    let anode = [[MI_ConnectionPoint alloc]
        initWithPosition:NSMakePoint(0.0f, 24.0f)
                    size:NSMakeSize(6.0f, 6.0f)
                    name:@"Anode"
     nodeNumberPlacement:MI_DirectionNortheast];
    let cathode = [[MI_ConnectionPoint alloc]
        initWithPosition:NSMakePoint(0.0f, -24.0f)
                    size:NSMakeSize(6.0f, 6.0f)
                    name:@"Cathode"
     nodeNumberPlacement:MI_DirectionSoutheast];
    self.connectionPoints = @{@"Anode": anode, @"Cathode": cathode };
    self.parameters[@"Magnitude"] = @"380";
    self.parameters[@"Phase"] = @"0";
  }
  return self;
}

- (void) draw
{
  [super draw];
  let bp = [NSBezierPath bezierPath];
  [bp moveToPoint:NSMakePoint(self.position.x, self.position.y + 24.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp relativeMoveToPoint:NSMakePoint(-10.0f, -16.0f)];
  [bp relativeCurveToPoint:NSMakePoint( 10.0f,  0.0f)
             controlPoint1:NSMakePoint(  3.0f,  5.0f)
             controlPoint2:NSMakePoint(  7.0f,  5.0f)];
  [bp relativeCurveToPoint:NSMakePoint( 10.0f,  0.0f)
             controlPoint1:NSMakePoint(  3.0f, -5.0f)
             controlPoint2:NSMakePoint(  7.0f, -5.0f)];
  [bp relativeMoveToPoint:NSMakePoint(-10.0f, -16.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp stroke];
  [[NSBezierPath bezierPathWithOvalInRect:NSMakeRect(self.position.x - 16.0f, self.position.y - 16.0f, 32.0f, 32.0f)] stroke];
  [super endDraw];
}

- (NSString*) shapeToSVG
{
  let svg = [NSMutableString stringWithCapacity:100];
  [svg appendFormat:@"%@<path stroke=\"black\" fill=\"none\" d=\"M %g %g v -8 m -10 -16 c 3 5 7 5 10 0 c 3 -5 7 -5 10 0 m -10 -16 v -8\"/>",
    [super shapeToSVG], self.position.x, self.position.y + 24.0f];
  [svg appendFormat:@"\n<circle stroke=\"black\" fill=\"none\" cx=\"%g\" cy=\"%g\" r=\"16\"/>%@",
    self.position.x, self.position.y, [super endSVG]];
  return svg;
}

@end

// MARK: -

@implementation MI_PulseVoltageSourceElement

- (instancetype) init
{
  if (self = [super initWithSize:NSMakeSize(32.0f, 48.0f)])
  {
    self.name = @"Pulse Voltage Source";
    self.label = @"VPulse";
    self.labelPosition = MI_DirectionRight;
    let anode = [[MI_ConnectionPoint alloc]
        initWithPosition:NSMakePoint(0.0f, 24.0f)
                    size:NSMakeSize(6.0f, 6.0f)
                    name:@"Anode"
     nodeNumberPlacement:MI_DirectionNortheast];
    let cathode = [[MI_ConnectionPoint alloc]
        initWithPosition:NSMakePoint(0.0f, -24.0f)
                    size:NSMakeSize(6.0f, 6.0f)
                    name:@"Cathode"
     nodeNumberPlacement:MI_DirectionSoutheast];
    self.connectionPoints = @{@"Anode": anode, @"Cathode": cathode};
    self.parameters[@"Initial Value"] = @"0";
    self.parameters[@"Pulsed Value"] = @"5";
    self.parameters[@"Delay Time"] = @"0";
    self.parameters[@"Rise Time"] = @"0";
    self.parameters[@"Fall Time"] = @"0";
    self.parameters[@"Pulse Width"] = @"0.001";
    self.parameters[@"Period"] = @"1";
  }
  return self;
}

- (void) draw
{
  [super draw];
  let bp = [NSBezierPath bezierPath];
  [bp moveToPoint:NSMakePoint(self.position.x, self.position.y + 24.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp relativeMoveToPoint:NSMakePoint( -9.0f, -22.0f)];
  [bp relativeLineToPoint:NSMakePoint(  5.0f,   0.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  12.0f)];
  [bp relativeLineToPoint:NSMakePoint(  8.0f,   0.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f, -12.0f)];
  [bp relativeLineToPoint:NSMakePoint(  5.0f,   0.0f)];
  [bp relativeMoveToPoint:NSMakePoint( -9.0f, -10.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp stroke];
  [[NSBezierPath bezierPathWithOvalInRect:NSMakeRect(self.position.x - 16.0f, self.position.y - 16.0f, 32.0f, 32.0f)] stroke];
  [super endDraw];
}

- (NSString*) shapeToSVG
{
  let svg = [NSMutableString stringWithCapacity:100];
  [svg appendFormat:@"%@<path stroke=\"black\" fill=\"none\" d=\"M %g %g v -8 m -9 -22 h 5 v 12 h 8 v -12 h 5 m -9 -10 v -8\"/>",
    [super shapeToSVG], self.position.x, self.position.y + 24.0f];
  [svg appendFormat:@"\n<circle stroke=\"black\" fill=\"none\" cx=\"%g\" cy=\"%g\" r=\"16\"/>%@",
    self.position.x, self.position.y, [super endSVG]];
  return svg;
}

@end

// MARK: -

@implementation MI_SinusoidalVoltageSourceElement

- (instancetype) init
{
  if (self = [super initWithSize:NSMakeSize(32.0f, 48.0f)])
  {
    self.name = @"Sinusoidal Voltage Source";
    self.label = @"VSin";
    self.labelPosition = MI_DirectionRight;
    let anode = [[MI_ConnectionPoint alloc]
        initWithPosition:NSMakePoint(0.0f, 24.0f)
                    size:NSMakeSize(6.0f, 6.0f)
                    name:@"Anode"
     nodeNumberPlacement:MI_DirectionNortheast];
    let cathode = [[MI_ConnectionPoint alloc]
        initWithPosition:NSMakePoint(0.0f, -24.0f)
                    size:NSMakeSize(6.0f, 6.0f)
                    name:@"Cathode"
     nodeNumberPlacement:MI_DirectionSoutheast];
    self.connectionPoints = @{@"Anode": anode, @"Cathode": cathode};
    self.parameters[@"Offset"] = @"0.0";
    self.parameters[@"Amplitude"] = @"1.0";
    self.parameters[@"Frequency"] = @"50.0";
    self.parameters[@"Delay"] = @"0.0";
    self.parameters[@"Damping Factor"] = @"0.0";
  }
  return self;
}

- (void) draw
{
  [super draw];
  let bp = [NSBezierPath bezierPath];
  [bp moveToPoint:NSMakePoint(self.position.x, self.position.y + 24.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  0.0f, -32.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp relativeMoveToPoint:NSMakePoint( -8.0f,  24.0f)];
  [bp relativeCurveToPoint:NSMakePoint( 8.0f,   0.0f)
             controlPoint1:NSMakePoint( 3.0f, -10.0f)
             controlPoint2:NSMakePoint( 5.0f, -10.0f)];
  [bp relativeCurveToPoint:NSMakePoint( 8.0f,   0.0f)
             controlPoint1:NSMakePoint( 3.0f,  10.0f)
             controlPoint2:NSMakePoint( 5.0f,  10.0f)];
  [bp stroke];

  [[NSBezierPath bezierPathWithOvalInRect:NSMakeRect(self.position.x - 16.0f, self.position.y - 16.0f, 32.0f, 32.0f)] stroke];
  [super endDraw];
}

- (NSString*) shapeToSVG
{
  let svg = [NSMutableString stringWithCapacity:100];
  [svg appendFormat:@"%@<path stroke=\"black\" fill=\"none\" d=\"M %g %g v -8 m 0 -32 v -8 m -8 24 c 3 -10 5 -10 8 0 c 3 10 5 10 8 0\"/>",
      [super shapeToSVG], self.position.x, self.position.y + 24.0f];
  [svg appendFormat:@"\n<circle stroke=\"black\" fill=\"none\" cx=\"%g\" cy=\"%g\" r=\"16\"/>%@",
      self.position.x, self.position.y, [super endSVG]];
  return svg;
}

@end

// MARK: -

@implementation MI_CurrentSourceElement

- (instancetype) init
{
  if (self = [super initWithSize:NSMakeSize(32.0f, 48.0f)])
  {
    self.name = @"Current Source";
    self.label = @"I";
    self.labelPosition = MI_DirectionRight;
    let anode = [[MI_ConnectionPoint alloc]
        initWithPosition:NSMakePoint(0.0f, -24.0f)
                    size:NSMakeSize(6.0f, 6.0f)
                    name:@"Anode"
     nodeNumberPlacement:MI_DirectionNortheast];
    let cathode = [[MI_ConnectionPoint alloc]
        initWithPosition:NSMakePoint(0.0f, 24.0f)
                    size:NSMakeSize(6.0f, 6.0f)
                    name:@"Cathode"
     nodeNumberPlacement:MI_DirectionSoutheast];
    self.connectionPoints = @{@"Anode": anode, @"Cathode": cathode};
    self.parameters[@"DC_Current"] = @"1m";
    self.parameters[@"AC_Magnitude"] = @"0";
    self.parameters[@"AC_Phase"] = @"0";
  }
  return self;
}

- (void) draw
{
  [super draw];
  var bp = [NSBezierPath bezierPath];
  [bp moveToPoint:NSMakePoint(self.position.x, self.position.y + 24.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  0.0f,  -6.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f, -20.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  0.0f,  -6.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp stroke];
  // draw arrow head
  bp = [NSBezierPath bezierPath];
  [bp moveToPoint:NSMakePoint(self.position.x, self.position.y + 10.9f)];
  [bp relativeLineToPoint:NSMakePoint(3.0f, -6.0f)];
  [bp relativeLineToPoint:NSMakePoint(-6.0f, 0.0f)];
  [bp closePath];
  [bp fill];
  [[NSBezierPath bezierPathWithOvalInRect:NSMakeRect(self.position.x - 16.0f, self.position.y - 16.0f, 32.0f, 32.0f)] stroke];
  [super endDraw];
}

- (NSString*) shapeToSVG
{
  let svg = [NSMutableString stringWithCapacity:100];
  [svg appendFormat:@"%@<path stroke=\"black\" fill=\"none\" d=\"M %g %g v -8 m 0 -6 v -20 m 0 -6 v -8\"/>",
    [super shapeToSVG], self.position.x, self.position.y + 24.0f];
  // arrow head
  [svg appendFormat:@"\n<path stroke=\"none\" fill=\"black\" d=\"M %g %g l 3 -6 h -6 z\"/>",
    self.position.x, self.position.y + 10.9f];
  [svg appendFormat:@"\n<circle stroke=\"black\" fill=\"none\" cx=\"%g\" cy=\"%g\" r=\"16\"/>%@",
    self.position.x, self.position.y, [super endSVG]];
  return svg;
}

@end

// MARK: -

@implementation MI_PulseCurrentSourceElement

- (instancetype) init
{
  if (self = [super initWithSize:NSMakeSize(32.0f, 48.0f)])
  {
    self.name = @"Pulse Current Source";
    self.label = @"IPulse";
    self.labelPosition = MI_DirectionRight;
    let anode = [[MI_ConnectionPoint alloc]
        initWithPosition:NSMakePoint(0.0f, -24.0f)
                    size:NSMakeSize(6.0f, 6.0f)
                    name:@"Anode"
     nodeNumberPlacement:MI_DirectionNortheast];
    let cathode = [[MI_ConnectionPoint alloc]
        initWithPosition:NSMakePoint(0.0f, 24.0f)
                    size:NSMakeSize(6.0f, 6.0f)
                    name:@"Cathode"
     nodeNumberPlacement:MI_DirectionSoutheast];
    self.connectionPoints = @{ @"Anode" : anode, @"Cathode" : cathode };
    self.parameters[@"Initial Value"] = @"0";
    self.parameters[@"Pulsed Value"] = @"5";
    self.parameters[@"Delay Time"] = @"0";
    self.parameters[@"Rise Time"] = @"0";
    self.parameters[@"Fall Time"] = @"0";
    self.parameters[@"Pulse Width"] = @"0.001";
    self.parameters[@"Period"] = @"1";
  }
  return self;
}

- (void) draw
{
  [super draw];
  var bp = [NSBezierPath bezierPath];
  [bp moveToPoint:NSMakePoint(self.position.x, self.position.y + 24.0f)];
  // Draw top handle
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  0.0f,  -5.0f)];
  // Draw body of arrow and the pulse symbol
#ifdef standard_pulseCurrentSourceShape
  [bp relativeLineToPoint:NSMakePoint(  0.0f, -22.0f)];
  [bp relativeMoveToPoint:NSMakePoint( -5.0f,   0.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,   7.0f)];
  [bp relativeLineToPoint:NSMakePoint( -5.0f,   0.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,   8.0f)];
  [bp relativeLineToPoint:NSMakePoint(  5.0f,   0.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,   7.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  5.0f, -22.0f)];
#else
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -9.0f)];
  [bp relativeLineToPoint:NSMakePoint( -8.0f,   0.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -6.0f)];
  [bp relativeLineToPoint:NSMakePoint(  8.0f,   0.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -6.0f)];
#endif
  // Draw bottom handle
  [bp relativeMoveToPoint:NSMakePoint(  0.0f,  -6.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp stroke];
  // draw arrow head
  bp = [NSBezierPath bezierPath];
  [bp moveToPoint:NSMakePoint(self.position.x, self.position.y + 12.9f)];
  [bp relativeLineToPoint:NSMakePoint(3.0f, -6.0f)];
  [bp relativeLineToPoint:NSMakePoint(-6.0f, 0.0f)];
  [bp closePath];
  [bp fill];
  [[NSBezierPath bezierPathWithOvalInRect:
      NSMakeRect(self.position.x - 16.0f, self.position.y - 16.0f, 32.0f, 32.0f)] stroke];
  [super endDraw];
}

- (NSString*) shapeToSVG
{
  let svg = [NSMutableString stringWithCapacity:100];
  [svg appendFormat:@"%@<path stroke=\"black\" fill=\"none\" d=\"M %g %g v -8 m 0 -5 v -9 h -8 v -6 h 8 v -6 m 0 -6 v -8\"/>",
    [super shapeToSVG], self.position.x, self.position.y + 24.0f];
  // arrow head
  [svg appendFormat:@"\n<path stroke=\"none\" fill=\"black\" d=\"M %g %g l 3 -6 h -6 z\"/>",
    self.position.x, self.position.y + 12.9f];
  [svg appendFormat:@"\n<circle stroke=\"black\" fill=\"none\" cx=\"%g\" cy=\"%g\" r=\"16\"/>%@",
    self.position.x, self.position.y, [super endSVG]];
  return svg;
}

@end

// MARK: -

@implementation MI_VoltageControlledCurrentSource

- (instancetype) init
{
    if (self = [super initWithSize:NSMakeSize(36.0f, 48.0f)])
    {
        self.name = @"Voltage-Controlled Current Source";
        self.label = @"G";
        self.labelPosition = MI_DirectionRight;
        let np = [[MI_ConnectionPoint alloc]
            initWithPosition:NSMakePoint(2.0f, 24.0f)
                        size:NSMakeSize(6.0f, 6.0f)
                        name:@"N+"
         nodeNumberPlacement:MI_DirectionNortheast];
        let nm = [[MI_ConnectionPoint alloc]
            initWithPosition:NSMakePoint(2.0f, -24.0f)
                        size:NSMakeSize(6.0f, 6.0f)
                        name:@"N-"
         nodeNumberPlacement:MI_DirectionSoutheast];
        let ncp = [[MI_ConnectionPoint alloc]
            initWithPosition:NSMakePoint(-18.0f, 8.0f)
                        size:NSMakeSize(6.0f, 6.0f)
                        name:@"NC+"
         nodeNumberPlacement:MI_DirectionNorthwest];
        let ncm = [[MI_ConnectionPoint alloc]
            initWithPosition:NSMakePoint(-18.0f, -8.0f)
                        size:NSMakeSize(6.0f, 6.0f)
                        name:@"NC-"
         nodeNumberPlacement:MI_DirectionSouthwest];
        self.connectionPoints = @{ @"N+" : np, @"N-" : nm, @"NC+" : ncp, @"NC-" : ncm };
        self.parameters[@"Transconductance"] = @"0.5";
    }
    return self;
}

- (void) draw
{
  [super draw];
  var bp = [NSBezierPath bezierPath];
  [bp moveToPoint:NSMakePoint(self.position.x + 2.0f, self.position.y + 24.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  0.0f,  -6.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f, -20.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  0.0f,  -6.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  // Draw the voltage-control connector hooks
  [bp relativeMoveToPoint:NSMakePoint( -13.86f, 16.0f)]; // (-13.86, -8) on the circle
  [bp relativeLineToPoint:NSMakePoint(  -6.14f, 0.0f)]; // (-20, -8)
  [bp relativeMoveToPoint:NSMakePoint(   0.0f, 16.0f)];  // (-20, 8)
  [bp relativeLineToPoint:NSMakePoint(   6.14f, 0.0f)]; // (-13.86, 8) on the circle
  [bp relativeMoveToPoint:NSMakePoint(  -5.14f, 4.5f)];
  [bp relativeLineToPoint:NSMakePoint(   5.0f,  0.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  -2.5f,  2.5f)];
  [bp relativeLineToPoint:NSMakePoint(   0.0f, -5.0f)];
  [bp stroke];
  // draw arrow head
  bp = [NSBezierPath bezierPath];
  [bp moveToPoint:NSMakePoint(self.position.x + 2.0f, self.position.y + 10.9f)];
  [bp relativeLineToPoint:NSMakePoint(3.0f, -6.0f)];
  [bp relativeLineToPoint:NSMakePoint(-6.0f, 0.0f)];
  [bp closePath];
  [bp fill];
  [[NSBezierPath bezierPathWithOvalInRect:
      NSMakeRect(self.position.x - 14.0f, self.position.y - 16.0f, 32.0f, 32.0f)] stroke];
  [super endDraw];
}

- (NSString*) shapeToSVG
{
  let svg = [NSMutableString stringWithCapacity:100];
  [svg appendFormat:@"%@<path stroke=\"black\" fill=\"none\" d=\"M %g %g v -8 m 0 -6 v -20 m 0 -6 v -8 m -13.86 16 h -6.14 m 0 16 h 6.14 m -5.14 4.5 h 5 m -2.5 2.5 v -5\"/>",
    [super shapeToSVG], self.position.x + 2.0f, self.position.y + 24.0f];
  // arrow head
  [svg appendFormat:@"\n<path stroke=\"none\" fill=\"black\" d=\"M %g %g l 3 -6 h -6 z\"/>",
    self.position.x + 2.0f, self.position.y + 10.9f];
  [svg appendFormat:@"\n<circle stroke=\"black\" fill=\"none\" cx=\"%g\" cy=\"%g\" r=\"16\"/>%@",
    self.position.x + 2.0f, self.position.y, [super endSVG]];
  return svg;
}

@end

// MARK: -

@implementation MI_VoltageControlledVoltageSource

- (instancetype) init
{
    if (self = [super initWithSize:NSMakeSize(36.0f, 48.0f)])
    {
        self.name = @"Voltage-Controlled Voltage Source";
        self.label = @"E";
        self.labelPosition = MI_DirectionRight;
        MI_ConnectionPoint* np = [[MI_ConnectionPoint alloc]
            initWithPosition:NSMakePoint(2.0f, 24.0f)
                        size:NSMakeSize(6.0f, 6.0f)
                        name:@"N+"
         nodeNumberPlacement:MI_DirectionNortheast];
        MI_ConnectionPoint* nm = [[MI_ConnectionPoint alloc]
            initWithPosition:NSMakePoint(2.0f, -24.0f)
                        size:NSMakeSize(6.0f, 6.0f)
                        name:@"N-"
         nodeNumberPlacement:MI_DirectionSoutheast];
        MI_ConnectionPoint* ncp = [[MI_ConnectionPoint alloc]
            initWithPosition:NSMakePoint(-18.0f, 8.0f)
                        size:NSMakeSize(6.0f, 6.0f)
                        name:@"NC+"
         nodeNumberPlacement:MI_DirectionNorthwest];
        MI_ConnectionPoint* ncm = [[MI_ConnectionPoint alloc]
            initWithPosition:NSMakePoint(-18.0f, -8.0f)
                        size:NSMakeSize(6.0f, 6.0f)
                        name:@"NC-"
         nodeNumberPlacement:MI_DirectionSouthwest];
        self.connectionPoints = @{ @"N+" : np, @"N-" : nm, @"NC+" : ncp, @"NC-" : ncm };
        [self.parameters setObject:@"0.5" forKey:@"Gain"];
    }
    return self;
}

- (void) draw
{
  [super draw];
  let bp = [NSBezierPath bezierPath];
  [bp moveToPoint:NSMakePoint(self.position.x + 2.0f, self.position.y + 24.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)]; // ( 0, 16)
  [bp relativeMoveToPoint:NSMakePoint(  0.0f,  -6.0f)]; // ( 0, 10)
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)]; // ( 0, 2)
  [bp relativeMoveToPoint:NSMakePoint(  0.0f, -18.0f)]; // ( 0, -16)
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)]; // ( 0, -24)
  [bp relativeMoveToPoint:NSMakePoint( -4.0f, +30.0f)]; // (-4, 6)
  [bp relativeLineToPoint:NSMakePoint(  8.0f,   0.0f)]; // ( 4, 6)
  [bp relativeMoveToPoint:NSMakePoint(  0.0f, -14.0f)]; // ( 4, -8)
  [bp relativeLineToPoint:NSMakePoint( -8.0f,   0.0f)]; // (-4, -8)
  // Draw the voltage-control connector hooks
  [bp relativeMoveToPoint:NSMakePoint( -9.86f, 0.0f)]; // (-13.86, -8) on the circle
  [bp relativeLineToPoint:NSMakePoint( -6.14f, 0.0f)]; // (-16, -8)
  [bp relativeMoveToPoint:NSMakePoint( 0.0f, 16.0f)];  // (-16, 8)
  [bp relativeLineToPoint:NSMakePoint( 6.14f, 0.0f)]; // (-13.86, 8) on the circle
  [bp relativeMoveToPoint:NSMakePoint(  -5.14f, 4.5f)];
  [bp relativeLineToPoint:NSMakePoint(   5.0f,  0.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  -2.5f,  2.5f)];
  [bp relativeLineToPoint:NSMakePoint(   0.0f, -5.0f)];
  [bp stroke];
  [[NSBezierPath bezierPathWithOvalInRect:NSMakeRect(self.position.x - 14.0f, self.position.y - 16.0f, 32.0f, 32.0f)] stroke];
  [super endDraw];
}

- (NSString*) shapeToSVG
{
  let svg = [NSMutableString stringWithCapacity:100];
  [svg appendFormat:@"%@<path stroke=\"black\" fill=\"none\" d=\"M %g %g v -8 m 0 -6 v -8 m 0 -18 v -8 m -4 30 h 8 m 0 -14 h -8 m -9.86 0 h -6.14 m 0 16 h 6.14 m -5.14 4.5 h 5 m -2.5 2.5 v -5\"/>",
    [super shapeToSVG], self.position.x + 2.0f, self.position.y + 24.0f];
  [svg appendFormat:@"\n<circle stroke=\"black\" fill=\"none\" cx=\"%g\" cy=\"%g\" r=\"16\"/>%@",
    self.position.x + 2.0f, self.position.y, [super endSVG]];
  return svg;
}

@end

// MARK: -

@implementation MI_CurrentControlledCurrentSource

- (instancetype) init
{
  if (self = [super initWithSize:NSMakeSize(32.0f, 48.0f)])
  {
    self.name = @"Current-Controlled Current Source";
    self.label = @"F";
    self.labelPosition = MI_DirectionRight;
    MI_ConnectionPoint* anode = [[MI_ConnectionPoint alloc]
        initWithPosition:NSMakePoint(0.0f, -24.0f)
                    size:NSMakeSize(6.0f, 6.0f)
                    name:@"N+"
     nodeNumberPlacement:MI_DirectionNortheast];
    MI_ConnectionPoint* cathode = [[MI_ConnectionPoint alloc]
        initWithPosition:NSMakePoint(0.0f, 24.0f)
                    size:NSMakeSize(6.0f, 6.0f)
                    name:@"N-"
     nodeNumberPlacement:MI_DirectionSoutheast];
    self.connectionPoints = @{@"N+": anode, @"N-": cathode};
    [self.parameters setObject:@"0.5" forKey:@"Gain"];
    [self.parameters setObject:@""    forKey:@"VNAM"];
  }
  return self;
}

- (void) draw
{
  [super draw];
  var bp = [NSBezierPath bezierPath];
  [bp moveToPoint:NSMakePoint(self.position.x, self.position.y + 24.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  0.0f,  -6.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f, -20.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  0.0f,  -6.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp relativeMoveToPoint:NSMakePoint(-14.0f,   1.0f)];
  [bp relativeLineToPoint:NSMakePoint( 28.0f,  46.0f)];
  [bp stroke];
  // draw arrow head of current direction inside the circle
  bp = [NSBezierPath bezierPath];
  [bp moveToPoint:NSMakePoint(self.position.x, self.position.y + 10.9f)];
  [bp relativeLineToPoint:NSMakePoint(3.0f, -6.0f)];
  [bp relativeLineToPoint:NSMakePoint(-6.0f, 0.0f)];
  [bp closePath];
  [bp fill];
  // draw the circle
  [[NSBezierPath bezierPathWithOvalInRect:NSMakeRect(self.position.x - 16.0f, self.position.y - 16.0f, 32.0f, 32.0f)] stroke];
  // Draw the arrow head of the diagonal line
  bp = [NSBezierPath bezierPath];
  [bp moveToPoint:NSMakePoint(self.position.x + 7.0f, self.position.y + 18.0f)];
  [bp relativeLineToPoint:NSMakePoint(8.0f, 6.0f)];
  [bp relativeLineToPoint:NSMakePoint(-1.8f, -9.5f)];
  [bp closePath];
  [bp fill];
  [super endDraw];
}

- (NSString*) shapeToSVG
{
  let svg = [NSMutableString stringWithCapacity:100];
  [svg appendFormat:@"%@<path stroke=\"black\" fill=\"none\" d=\"M %g %g v -8 m 0 -6 v -20 m 0 -6 v -8 m -14 1 l 28 46\"/>",
    [super shapeToSVG], self.position.x, self.position.y + 24.0f];
  // arrow head of current direction inside the circle
  [svg appendFormat:@"\n<path stroke=\"none\" fill=\"black\" d=\"M %g %g l 3 -6 h -6 z\"/>",
    self.position.x, self.position.y + 10.9f];
  // arrow head of diagonal line
  [svg appendFormat:@"\n<path stroke=\"none\" fill=\"black\" d=\"M %g %g l 8 6 l -1.8 -9.5 z\"/>",
    self.position.x + 7.0f, self.position.y + 18.0f];
  [svg appendFormat:@"\n<circle stroke=\"black\" fill=\"none\" cx=\"%g\" cy=\"%g\" r=\"16\"/>%@",
    self.position.x, self.position.y, [super endSVG]];
  return svg;
}

@end

// MARK: -

@implementation MI_CurrentControlledVoltageSource

- (instancetype) init
{
  if (self = [super initWithSize:NSMakeSize(32.0f, 48.0f)])
  {
    self.name = @"Current-Controlled Voltage Source";
    self.label = @"H";
    self.labelPosition = MI_DirectionRight;
    let anode = [[MI_ConnectionPoint alloc]
        initWithPosition:NSMakePoint(0.0f, -24.0f)
                    size:NSMakeSize(6.0f, 6.0f)
                    name:@"N+"
     nodeNumberPlacement:MI_DirectionNortheast];
    let cathode = [[MI_ConnectionPoint alloc]
        initWithPosition:NSMakePoint(0.0f, 24.0f)
                    size:NSMakeSize(6.0f, 6.0f)
                    name:@"N-"
     nodeNumberPlacement:MI_DirectionSoutheast];
    self.connectionPoints = @{ @"N+" : anode, @"N-" : cathode };
    self.parameters[@"Transresistance"] = @"0.5";
    self.parameters[@"VNAM"] = @"";
  }
  return self;
}

- (void) draw
{
  [super draw];
  var bp = [NSBezierPath bezierPath];
  [bp moveToPoint:NSMakePoint(self.position.x, self.position.y + 24.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  0.0f,  -6.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  0.0f, -18.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp relativeMoveToPoint:NSMakePoint( -4.0f, +30.0f)];
  [bp relativeLineToPoint:NSMakePoint(  8.0f,   0.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  0.0f, -14.0f)];
  [bp relativeLineToPoint:NSMakePoint( -8.0f,   0.0f)];
  [bp relativeMoveToPoint:NSMakePoint(-10.0f, -15.0f)];
  [bp relativeLineToPoint:NSMakePoint( 28.0f,  46.0f)];
  [bp stroke];
  [[NSBezierPath bezierPathWithOvalInRect:NSMakeRect(self.position.x - 16.0f, self.position.y - 16.0f, 32.0f, 32.0f)] stroke];
  // Drawing the arrow head of the diagonal line
  bp = [NSBezierPath bezierPath];
  [bp moveToPoint:NSMakePoint(self.position.x + 7.0f, self.position.y + 18.0f)];
  [bp relativeLineToPoint:NSMakePoint(8.0f, 6.0f)];
  [bp relativeLineToPoint:NSMakePoint(-1.8f, -9.5f)];
  [bp closePath];
  [bp fill];
  [super endDraw];
}

- (NSString*) shapeToSVG
{
  let svg = [NSMutableString stringWithCapacity:100];
  [svg appendFormat:@"%@<path stroke=\"black\" fill=\"none\" d=\"M %g %g v -8 m 0 -6 v -8 m 0 -18 v -8 m -4 30 h 8 m 0 -14 h -8 m -10 -15 l 28 46\"/>",
    [super shapeToSVG], self.position.x, self.position.y + 24.0f];
  // arrow head of diagonal line
  [svg appendFormat:@"\n<path stroke=\"none\" fill=\"black\" d=\"M %g %g l 8 6 l -1.8 -9.5 z\"/>",
    self.position.x + 7.0f, self.position.y + 18.0f];
  [svg appendFormat:@"\n<circle stroke=\"black\" fill=\"none\" cx=\"%g\" cy=\"%g\" r=\"16\"/>%@",
    self.position.x, self.position.y, [super endSVG]];
  return svg;
}

@end

// MARK: -

@implementation MI_NonlinearDependentSource

- (instancetype) init
{
  if (self = [super initWithSize:NSMakeSize(32.0f, 48.0f)])
  {
    self.name = @"Nonlinear Dependent Power Source";
    self.label = @"B";
    self.labelPosition = MI_DirectionRight;
    MI_ConnectionPoint* anode = [[MI_ConnectionPoint alloc]
        initWithPosition:NSMakePoint(0.0f, 24.0f)
                    size:NSMakeSize(6.0f, 6.0f)
                    name:@"N+"
     nodeNumberPlacement:MI_DirectionNortheast];
    MI_ConnectionPoint* cathode = [[MI_ConnectionPoint alloc]
        initWithPosition:NSMakePoint(0.0f, -24.0f)
                    size:NSMakeSize(6.0f, 6.0f)
                    name:@"N-"
     nodeNumberPlacement:MI_DirectionSoutheast];
    self.connectionPoints = @{ @"N+" : anode, @"N-" : cathode };
    self.parameters[@"Expression"] = @"V=1+V(0)";
  }
  return self;
}

- (void) draw
{
  [super draw];
  let bp = [NSBezierPath bezierPath];
  [bp moveToPoint:NSMakePoint(self.position.x, self.position.y + 24.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  0.0f,  -6.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  0.0f, -18.0f)];
  [bp relativeLineToPoint:NSMakePoint(  0.0f,  -8.0f)];
  [bp relativeMoveToPoint:NSMakePoint( -4.0f, +30.0f)];
  [bp relativeLineToPoint:NSMakePoint(  8.0f,   0.0f)];
  [bp relativeMoveToPoint:NSMakePoint(  0.0f, -14.0f)];
  [bp relativeLineToPoint:NSMakePoint( -8.0f,   0.0f)];
  [bp stroke];
  [NSBezierPath strokeRect:NSMakeRect(self.position.x - 16.0f, self.position.y - 16.0f, 32.0f, 32.0f)];
  [super endDraw];
}

- (NSString*) shapeToSVG
{
  let svg = [NSMutableString stringWithCapacity:100];
  [svg appendFormat:@"%@<path stroke=\"black\" fill=\"none\" d=\"M %g %g v -8 m 0 -6 v -8 m 0 -18 v -8 m -4 30 h 8 m 0 -14 h -8\"/>",
    [super shapeToSVG], self.position.x, self.position.y + 24.0f];
  [svg appendFormat:@"\n<rect stroke=\"black\" fill=\"none\" x=\"%g\" y=\"%g\" width=\"32\" height=\"32\"/>%@",
    self.position.x - 16.0f, self.position.y - 16.0f, [super endSVG]];
  return svg;
}

@end

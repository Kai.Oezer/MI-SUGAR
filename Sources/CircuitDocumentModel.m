/**
*
*   Copyright Kai Özer, 2003-2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import "CircuitDocumentModel.h"
#import "MI_CircuitElement.h"
#import "MI_DeviceModelManager.h"

static NSUInteger const kNumVariants = 4;

@interface CircuitDocumentModel ()
@end

@implementation CircuitDocumentModel
{
  int MI_version;
  NSString* _source;
  MI_CircuitSchematic* _schematic;
  NSMutableArray<MI_CircuitSchematic*>* _schematicVariants; // array which holds all schematic variants since 0.5.6
  NSMutableArray<NSString*>* _analyses;
}

- (instancetype) init
{
  if (self = [super init])
  {
    MI_version = MI_CIRCUIT_DOCUMENT_MODEL_VERSION;
    self.source = [[NSMutableString alloc] initWithCapacity:200];
    self.rawOutput = nil;
    self.output = nil;
    self.circuitTitle = nil;
    self.circuitName = @"";
    self.circuitNamespace = @"";
    self.schematicScale = 1.0f;
    self.schematicViewportOffset = NSMakePoint(0, 0);
    _analyses = [[NSMutableArray<NSString*> alloc] initWithCapacity:2];
    self.comment = [[NSMutableString alloc] initWithCapacity:25];
    self.revision = [[NSMutableString alloc] initWithCapacity:4];

    self.activeSchematicVariant = 0;
    _schematicVariants = [[NSMutableArray<MI_CircuitSchematic*> alloc] initWithCapacity:kNumVariants];
    for (NSUInteger counter = 0; counter < kNumVariants; counter++)
    {
      [_schematicVariants addObject:[MI_CircuitSchematic new]];
    }
  }
  return self;
}

- (NSString*) source
{
  return _source;
}

- (void) setSource:(NSString*)newSource
{
    NSUInteger lineStart = 0, nextLineStart, lineEnd;
    NSUInteger const limit = [newSource length];
    NSString *line, *uppercaseLine;
    BOOL gotTitle = NO;
    int tranCount = 0,
        acCount = 0,
        opCount = 0,
        tfCount = 0,
        dcCount = 0,
        distoCount = 0,
        noiseCount = 0;

    _source = newSource;
    [_analyses removeAllObjects];
    // Scan line by line, extract commands
    while (lineStart < limit)
    {
           [_source getLineStart:&lineStart
                            end:&nextLineStart
                    contentsEnd:&lineEnd
                       forRange:NSMakeRange(lineStart, 1)];
        line = [_source substringWithRange:NSMakeRange(lineStart, lineEnd - lineStart)];
        lineStart = nextLineStart;
        
        if (!gotTitle)
        {
            // Extract the title of the circuit
            [self setCircuitTitle:line];
            gotTitle = YES;
            continue;
        }

        // Check for analysis commands
        // In Gnucap, a ">" following a "*" turns the comment line into a normal line again
        if ([line hasPrefix:@"*>"])
            line = [line substringFromIndex:2];
        uppercaseLine = [[line stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] uppercaseString];
        if ( [uppercaseLine hasPrefix:@".TRAN "] || [uppercaseLine isEqualToString:@".TRAN"])
        {
            [_analyses addObject:
                (tranCount ? [@"Transient" stringByAppendingFormat:@" %d", tranCount + 1] : @"Transient")];
            tranCount++;
        }
        else if ([uppercaseLine hasPrefix:@".AC "] || [uppercaseLine isEqualToString:@".AC"])
        {
            [_analyses addObject:
                (acCount ? [@"AC" stringByAppendingFormat:@" %d", acCount + 1] : @"AC")];
            acCount++;
        }
        else if ( [uppercaseLine isEqualToString:@".OP"] || [uppercaseLine hasPrefix:@".OP "])
        {
            [_analyses addObject:
                (opCount ? [@"Operating Point" stringByAppendingFormat:@" %d", opCount + 1] : @"Operating Point")];
            opCount++;
        }
        else if ( [uppercaseLine hasPrefix:@".TF "] )
        {
            [_analyses addObject:
                (tfCount ? [@"Small-Signal" stringByAppendingFormat:@" %d", tfCount + 1] : @"Small-Signal")];
            tfCount++;
        }
        else if ( [uppercaseLine hasPrefix:@".DC "] )
        {
            [_analyses addObject:
                (dcCount ? [@"DC" stringByAppendingFormat:@" %d", dcCount + 1] : @"DC")];
            dcCount++;
        }
        else if ( [uppercaseLine hasPrefix:@".DISTO "] )
        {
            [_analyses addObject:
                (distoCount ? [@"Distortion" stringByAppendingFormat:@" %d", distoCount + 1] : @"Distortion")];
            distoCount++;
        }
        else if ( [uppercaseLine hasPrefix:@".NOISE "] )
        {
            [_analyses addObject:
                (noiseCount ? [@"Noise" stringByAppendingFormat:@" %d", noiseCount + 1] : @"Noise")];
            noiseCount++;
        }
    }
}

- (NSString*) gnucapFilteredSource
{
    // Look for commands or syntax which cause clutter or produce pitfalls for the postprocessor's parser
    NSUInteger lineStart = 0, nextLineStart, lineEnd;
    NSString* filteredSource = @"";
    NSString* line;
    BOOL firstLine = YES;
    NSUInteger const limit = self.source.length;
    // Scan line by line
    while (lineStart < limit)
    {
        [self.source getLineStart:&lineStart
                         end:&nextLineStart
                 contentsEnd:&lineEnd
                    forRange:NSMakeRange(lineStart, 1)];
        line = [self.source substringWithRange:NSMakeRange(lineStart, nextLineStart - lineStart)];
        if (firstLine)
        {
            while ([line hasPrefix:@"#"])
                line = [line substringFromIndex:1];
            filteredSource = [filteredSource stringByAppendingString:line];
            firstLine = NO;
        }
        else if (![[line uppercaseString] hasPrefix:@".LIST"] &&
                 ![[line uppercaseString] hasPrefix:@".PLOT"] &&
                 !([[line uppercaseString] hasPrefix:@"*"] || [[line uppercaseString] hasPrefix:@"*>"])
                 )
            filteredSource = [filteredSource stringByAppendingString:line];
        lineStart = nextLineStart;
    }
    return filteredSource;
}


- (NSString*) spiceFilteredSource
{
    NSUInteger lineStart = 0, nextLineStart = 0, lineEnd = 0;
    NSString* filteredSource = @"";
    NSString* line;
    NSUInteger const limit = self.source.length;
    BOOL firstLine = YES;
    // Filtering the .PLOT commands from the SPICE input
    while (nextLineStart < limit)
    {
        [self.source getLineStart:&lineStart
                         end:&nextLineStart
                 contentsEnd:&lineEnd
                    forRange:NSMakeRange(nextLineStart, 1)];
        line = [self.source substringWithRange:NSMakeRange(lineStart, nextLineStart - lineStart)];
        if (firstLine)
        {
            filteredSource = [filteredSource stringByAppendingString:line];
            firstLine = NO;
        }
        else if (![[line uppercaseString] hasPrefix:@".PLOT"] &&
            ![[line uppercaseString] hasPrefix:@"*"])
            filteredSource = [filteredSource stringByAppendingString:line];
        lineStart = nextLineStart;
    }
    return filteredSource;
}


- (NSString*) fullyQualifiedCircuitName;
{
    if (self.circuitNamespace && (self.circuitNamespace.length > 0))
        return [NSString stringWithFormat:@"%@.%@", self.circuitNamespace, self.circuitName];
    else
        return [self.circuitName copy];
}


- (void) setSchematic:(MI_CircuitSchematic*)newSchematic
{
  if (newSchematic != nil)
  {
    [_schematicVariants replaceObjectAtIndex:self.activeSchematicVariant withObject:newSchematic];
  }
}


- (MI_CircuitSchematic*) schematic
{
  return [_schematicVariants objectAtIndex:self.activeSchematicVariant];
}


- (NSArray<NSString*>*) analyses
{
  return _analyses;
}


- (NSArray<MI_CircuitElementDeviceModel*>*) circuitDeviceModels
{
  let circuitModels = [NSMutableArray<MI_CircuitElementDeviceModel*> arrayWithCapacity:2];
  for (MI_Schematic* currentSchematic in _schematicVariants)
  {
    MI_SchematicElement* currentElement;
    NSEnumerator* elementEnum = [currentSchematic elementEnumerator];
    while (currentElement = [elementEnum nextObject])
    {
      if (![currentElement isKindOfClass:[MI_CircuitElement class]]) continue;

      NSString* currentModelName = [[(MI_CircuitElement*)currentElement parameters] objectForKey:@"Model"];
      if (currentModelName != nil && ![currentModelName hasPrefix:@"Default"])
      {
        MI_CircuitElementDeviceModel* currentModel = [[MI_DeviceModelManager sharedManager] modelForName:currentModelName];
        if (currentModel != nil)
        {
          BOOL alreadyAdded = NO;
          for (MI_CircuitElementDeviceModel* addedModel in circuitModels)
          {
            if ([addedModel.modelName isEqualToString:currentModel.modelName])
            {
              alreadyAdded = YES;
              break;
            }
          }
          if (!alreadyAdded)
          {
            [circuitModels addObject:currentModel];
          }
        }
      }
    } // end iterate over elements
  } // end iterate over schematics
  return [NSArray<MI_CircuitElementDeviceModel*> arrayWithArray:circuitModels];
}


// MARK: NSSecureCoding

let CircuitDocumentModel_Version = @"Version";
let CircuitDocumentModel_Schematic = @"Schematic";
let CircuitDocumentModel_ActiveSchematicVariant = @"ActiveSchematicVariant";
let CircuitDocumentModel_SchematicVariants = @"SchematicVariants";
let CircuitDocumentModel_Source = @"Source";
let CircuitDocumentModel_Analyses = @"Analyses";
let CircuitDocumentModel_Title = @"Title";
let CircuitDocumentModel_Name = @"CircuitName"; // since version 0.5.3
let CircuitDocumentModel_Namespace = @"CircuitNamespace"; // since version 0.5.3
let CircuitDocumentModel_Comment = @"Comment";
let CircuitDocumentModel_Revision = @"Revision";
let CircuitDocumentModel_Scale = @"SchematicScale";
let CircuitDocumentModel_ViewportOffset = @"SchematicViewportOffset";

- (id)initWithCoder:(NSCoder *)decoder
{
  if (self = [super init])
  {
    int const decodedVersion = [decoder decodeIntForKey:CircuitDocumentModel_Version];
    // The decoding of the remaining properties depends on the version.
    if (decodedVersion < 3)
    {
      self.activeSchematicVariant = 0;
      _schematicVariants = [[NSMutableArray<MI_CircuitSchematic*> alloc] initWithCapacity:kNumVariants];
      let decodedSchematic = (MI_CircuitSchematic*)[decoder decodeObjectOfClass:[MI_CircuitSchematic class] forKey:CircuitDocumentModel_Schematic];
      [_schematicVariants addObject:(decodedSchematic != nil) ? decodedSchematic : [MI_CircuitSchematic new]];
      for (NSUInteger i = 0; i < kNumVariants - 1; i++)
      {
        [_schematicVariants addObject:[MI_CircuitSchematic new]];
      }
    }
    else
    {
      self.activeSchematicVariant = [decoder decodeIntForKey:CircuitDocumentModel_ActiveSchematicVariant];
      let decodedVariants = [decoder decodeArrayOfObjectsOfClass:[MI_CircuitSchematic class] forKey:CircuitDocumentModel_SchematicVariants];
      NSAssert(decodedVariants != nil, @"In this format version, the archive is expected to contain an array of circuit schematics (variants).");
      _schematicVariants = [NSMutableArray arrayWithArray: decodedVariants];
    }
    self.schematic = nil; // legacy variable
    self.source = [decoder decodeObjectOfClass:[NSString class] forKey:CircuitDocumentModel_Source];
    let decodedAnalyses = [decoder decodeArrayOfObjectsOfClass:[NSString class] forKey:CircuitDocumentModel_Analyses];
    _analyses = (decodedAnalyses != nil) ? [NSMutableArray arrayWithArray:decodedAnalyses] : [NSMutableArray array];
    self.circuitTitle = [decoder decodeObjectOfClass:[NSString class] forKey:CircuitDocumentModel_Title];
    if (self.circuitTitle == nil) { self.circuitTitle = @""; }
    self.circuitName = [decoder decodeObjectOfClass:[NSString class] forKey:CircuitDocumentModel_Name];
    if (self.circuitName == nil) { self.circuitName = @""; }
    self.circuitNamespace = [decoder decodeObjectOfClass:[NSString class] forKey:CircuitDocumentModel_Namespace];
    if (self.circuitNamespace == nil) { self.circuitNamespace = @""; }
    self.comment = [decoder decodeObjectOfClass:[NSString class] forKey:CircuitDocumentModel_Comment];
    if (self.comment == nil) { self.comment = [[NSMutableString alloc] initWithCapacity:25]; }
    self.revision = [decoder decodeObjectOfClass:[NSString class] forKey:CircuitDocumentModel_Revision];
    if (self.revision == nil) { self.revision = [[NSMutableString alloc] initWithCapacity:4]; }
    self.schematicScale = [decoder decodeFloatForKey:CircuitDocumentModel_Scale];
    self.schematicViewportOffset = [decoder decodePointForKey:CircuitDocumentModel_ViewportOffset];
    MI_version = MI_CIRCUIT_DOCUMENT_MODEL_VERSION;
#ifdef DEBUG
    if (decodedVersion < MI_version) {
      NSLog(@"Warning: Circuit \"%@\" upgraded from version %d to version %d while loading.", self.circuitName, decodedVersion, MI_version);
    }
#endif
  }
  return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
  [encoder encodeInt:MI_version                     forKey:CircuitDocumentModel_Version];
  [encoder encodeInt:self.activeSchematicVariant    forKey:CircuitDocumentModel_ActiveSchematicVariant];
  [encoder encodeObject:_schematicVariants          forKey:CircuitDocumentModel_SchematicVariants];
  [encoder encodeObject:self.source                 forKey:CircuitDocumentModel_Source];
  [encoder encodeObject:_analyses                   forKey:CircuitDocumentModel_Analyses];
  [encoder encodeObject:self.circuitTitle           forKey:CircuitDocumentModel_Title];
  [encoder encodeObject:self.circuitName            forKey:CircuitDocumentModel_Name];
  [encoder encodeObject:self.circuitNamespace       forKey:CircuitDocumentModel_Namespace];
  [encoder encodeObject:self.comment                forKey:CircuitDocumentModel_Comment];
  [encoder encodeObject:self.revision               forKey:CircuitDocumentModel_Revision];
  [encoder encodeFloat:self.schematicScale          forKey:CircuitDocumentModel_Scale];
  [encoder encodePoint:self.schematicViewportOffset forKey:CircuitDocumentModel_ViewportOffset];
}

+ (BOOL) supportsSecureCoding { return YES; }

@end

/**
*
*   Copyright Kai Özer, 2003-2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import "MI_Shape.h"

NS_ASSUME_NONNULL_BEGIN

/// Provides a Dual In-line Package (DIP) shape for use with subcircuits.
///
/// In its base orientation, the long axis of the rectangular DIP shape is
/// horizontally aligned and the notch points to the left, as can be seen
/// in the figure of an 8-pin DIP below. The pins are laid out at the bottom
/// and top, with pin #1 located at the bottom left, and the other pin numbers
/// assigned counter-clockwise.
/// ```
///    8  7  6  5
///  ┌─┴──┴──┴──┴─┐
///  │╮           │
///  ││  EXAMPLE  │
///  │╯           │
///  └─┬──┬──┬──┬─┘
///    1  2  3  4
/// ```
/// The name of the subcircuit type is placed horizontally in the center.
/// The length of the shape depends on the number of pins.
/// The width is a constant 36 units (including pins).
/// The pins are thin lines, 5 units long.
/// The distance between two neighboring pins is 10 units.
/// The distance between the last pin in a line and the nearest edge is 5 units.
@interface MI_DIPShape : MI_Shape <NSSecureCoding, NSCopying>

@property (nullable) NSString* name;

- (instancetype) initWithNumberOfPins:(NSInteger)numPins;

@end

NS_ASSUME_NONNULL_END

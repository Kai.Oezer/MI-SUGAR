/**
*
*   Copyright Kai Özer, 2003-2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import "MI_SubcircuitDocumentModel.h"

#define SUBCIRCUIT_DOCUMENT_MODEL_VERSION 1
// Version History
// 0 -> up to and including 0.5.6
// 1 -> 0.5.7

@interface MI_SubcircuitDocumentModel ()
@property (readwrite) NSDictionary<NSString*,NSString*>* pinMap;
@end

@implementation MI_SubcircuitDocumentModel

- (instancetype) initWithCircuitDocumentModel:(CircuitDocumentModel*)model
                                       pinMap:(NSDictionary<NSString*,NSString*>*)pinMap
{
  if (self = [super init])
  {
    self.source = model.source;
    self.schematic = model.schematic;
    self.circuitTitle = model.circuitTitle;
    self.pinMap = pinMap;
    self.shape = nil;
  }
  return self;
}

- (NSUInteger) numberOfPins
{
  return [[_pinMap allKeys] count];
}

- (NSString*) nodeNameForPin:(NSString*)portName
{
  return [_pinMap objectForKey:portName];
}

// MARK: NSSecureCoding

let SubcircuitDocumentModelCodingKey_Version = @"SubcircuitDocumentModelVersion";
let SubcircuitDocumentModelCodingKey_DeviceModels = @"DeviceModelsUsed";
let SubcircuitDocumentModelCodingKey_Subcircuits = @"SubcircuitsUsed";
let SubcircuitDocumentModelCodingKey_Shape = @"SubcircuitShape";
let SubcircuitDocumentModelCodingKey_PinMap = @"PinMap";

+ (BOOL) supportsSecureCoding
{
  return YES;
}

- (id)initWithCoder:(NSCoder *)decoder
{
  if (self = [super initWithCoder:decoder])
  {
    int version = [decoder decodeIntForKey:SubcircuitDocumentModelCodingKey_Version];
    self.usedDeviceModels = [decoder decodeDictionaryWithKeysOfClass:[NSString class]
                                                      objectsOfClass:[NSNumber class]
                                                              forKey:SubcircuitDocumentModelCodingKey_DeviceModels];
    self.usedSubcircuits = [decoder decodeObjectOfClass:[NSSet<NSString*> class] forKey:SubcircuitDocumentModelCodingKey_Subcircuits];
    self.shape = [decoder decodeObjectOfClass:[MI_Shape class] forKey:SubcircuitDocumentModelCodingKey_Shape];
    if (version == 0)
    {
      // For version 0 of this class the pinToNodeNameMap dictionary
      // was mapping NSNumber objects to named nodes because only DIP
      // shaped subcircuits were available and it made sense to use
      // pin numbering. Starting with version 0.5.7 the external ports
      // assigned names, not numbers. This block converts the mapping
      // of old files on the fly by assign the name "PinX" to an
      // external port, where X is the number of the pin.
      let tmp = (NSDictionary<NSString*,NSString*>*)[decoder decodeDictionaryWithKeysOfClass:[NSString class]
                                                                              objectsOfClass:[NSString class]
                                                                                      forKey:SubcircuitDocumentModelCodingKey_PinMap];
      if (tmp != nil)
      {
        let keys = [tmp allKeys];
        let tmpResult = [NSMutableDictionary<NSString*,NSString*> dictionaryWithCapacity:[keys count]];
        for (NSString* currentPinNumber in keys)
        {
          [tmpResult setObject:[tmp objectForKey:currentPinNumber]
                        forKey:[NSString stringWithFormat:@"Pin%d", [currentPinNumber intValue]]];
        }
        _pinMap = [[NSDictionary<NSString*,NSString*> alloc] initWithDictionary:tmpResult];
      }
    }
    else
    {
      _pinMap = [decoder decodeDictionaryWithKeysOfClass:[NSString class]
                                          objectsOfClass:[NSString class]
                                                  forKey:SubcircuitDocumentModelCodingKey_PinMap];
    }
  }
  return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
  [super encodeWithCoder:encoder];
  [encoder encodeObject:self.pinMap forKey:SubcircuitDocumentModelCodingKey_PinMap];
  [encoder encodeObject:self.usedDeviceModels forKey:SubcircuitDocumentModelCodingKey_DeviceModels];
  [encoder encodeObject:self.usedSubcircuits forKey:SubcircuitDocumentModelCodingKey_Subcircuits];
  [encoder encodeObject:self.shape forKey:SubcircuitDocumentModelCodingKey_Shape];
  [encoder encodeInt:SUBCIRCUIT_DOCUMENT_MODEL_VERSION forKey:SubcircuitDocumentModelCodingKey_Version];
}

@end

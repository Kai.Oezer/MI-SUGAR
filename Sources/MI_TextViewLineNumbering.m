/**
*
*   Copyright Kai Özer, 2003-2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import "MI_TextViewLineNumbering.h"
#import "MI_TextView.h"

@implementation MI_TextViewLineNumbering
{
    IBOutlet NSTextView* textView;
    NSDictionary<NSAttributedStringKey, NSObject*>* lineNumberStringAttributes;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) awakeFromNib
{
    lineNumberStringAttributes = [NSDictionary<NSAttributedStringKey, NSObject*> dictionaryWithObjectsAndKeys:
        [NSFont monospacedSystemFontOfSize:11.0f weight:NSFontWeightRegular], NSFontAttributeName,
        [NSColor colorWithDeviceWhite:0.6 alpha:1.0], NSForegroundColorAttributeName,
        nil];

    NSAssert(textView != nil, @"The outlet to the tracked text view must be set.");
    let clipView = [[textView enclosingScrollView] contentView];
    let notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserverForName:NSViewBoundsDidChangeNotification
                                    object:clipView
                                     queue:nil
                                usingBlock:^(NSNotification * _Nonnull n) { [self setNeedsDisplay:YES]; }];
    [clipView setPostsBoundsChangedNotifications:YES];
}

- (void) drawRect:(NSRect)rect
{
    let bounds = self.bounds;

    // drawing background fill
    [[NSColor colorWithDeviceWhite:0.95f alpha:1.0f] set];
    [NSBezierPath fillRect:bounds];

    NSRange glyphRange;
    int glyphIndex = 0;
    __block int lineCounter = 1;
    let text = [textView string];
    let layoutManager = [textView layoutManager];
    if (layoutManager == nil) { return; }
    let numGlyphs = [layoutManager numberOfGlyphs];
    __block CGFloat lastVerticalGlyphPosition = 0.0;
    __block CGFloat lastGlyphHeight = 0.0;

    void (^drawLineNumber)(void) = ^ {
        NSString* currentLineNumberString = [NSString stringWithFormat:@"%d", lineCounter];
        CGFloat lineWidth = [currentLineNumberString sizeWithAttributes:self->lineNumberStringAttributes].width + 5;
        CGFloat lineHeight = [currentLineNumberString sizeWithAttributes:self->lineNumberStringAttributes].height;
        [currentLineNumberString drawAtPoint:NSMakePoint([self frame].size.width - lineWidth,
                                                         lastVerticalGlyphPosition + (lastGlyphHeight - lineHeight)/2.0)
                              withAttributes:self->lineNumberStringAttributes];
    };

    while ( glyphIndex < numGlyphs )
    {
        // Getting the frame for the current glyph's entire line, in local coordinate space.
        let fragmentFrame = [layoutManager lineFragmentRectForGlyphAtIndex:glyphIndex effectiveRange:&glyphRange];
        let r = [self convertRect:fragmentFrame fromView:textView];
        lastVerticalGlyphPosition = r.origin.y;
        lastGlyphHeight = r.size.height;
        let bottomIsVisible = NSPointInRect(NSMakePoint(rect.origin.x, lastVerticalGlyphPosition), rect);
        let topIsVisible = NSPointInRect(NSMakePoint(rect.origin.x, lastVerticalGlyphPosition + lastGlyphHeight), rect);
        if (bottomIsVisible || topIsVisible)
        {
            drawLineNumber();
        }
        glyphIndex += [[text substringWithRange:glyphRange] length]; // advance glyph counter to EOL
        lineCounter++;
    }

    // Drawing one extra line number if there is an empty last line.
    if ([text hasSuffix:@"\n"]) {
        lastVerticalGlyphPosition -= lastGlyphHeight;
        drawLineNumber();
    }

#if 0
    [self drawFrameInRect:rect];
#endif
}

- (void) drawFrameInRect:(CGRect)rect
{
    [[NSColor blackColor] set];
    var bp = [NSBezierPath bezierPath];
    [bp moveToPoint:NSMakePoint(0, rect.size.height)];
    [bp relativeLineToPoint:NSMakePoint(rect.size.width, 0)];
    [bp stroke];
    [[NSColor grayColor] set];
    bp = [NSBezierPath bezierPath];
    [bp moveToPoint:NSMakePoint(rect.size.width, rect.size.height)];
    [bp relativeLineToPoint:NSMakePoint(0, -rect.size.height)];
    [bp relativeLineToPoint:NSMakePoint(-rect.size.width, 0)];
    [bp relativeLineToPoint:NSMakePoint(0, rect.size.height)];
    [bp stroke];
}

@end

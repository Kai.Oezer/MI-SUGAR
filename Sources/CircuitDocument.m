/**
*
*   Copyright Kai Özer, 2003-2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import "CircuitDocument.h"
#import "SpiceSimulator.h"
#import "AnalysisVariable.h"
#import "SugarPlotter.h"
#import "MI_Converter.h"
#import "SugarManager.h"
#import "MI_DeviceModelManager.h"
#import "MI_CircuitElementDeviceModel.h"
#import "MI_SubcircuitDocumentModel.h"
#import "MI_SubcircuitCreator.h"
#import "MI_CircuitDocumentPrinter.h"
#import "SpicePrintOutputParser.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define PLOT_CUSTOM_SIMULATOR_RESULTS
// above define is needed for allowing plotting of results from custom simulators

NSString* MISUGAR_RunItem                        = @"Analyze";
NSString* MISUGAR_PlotItem                       = @"Plot";
NSString* MISUGAR_Schematic2NetlistItem          = @"Capture";
NSString* MISUGAR_ElementsPanelItem              = @"ShowElementsPanel";
NSString* MISUGAR_InfoPanelItem                  = @"ShowInfoPanel";
NSString* MISUGAR_CanvasScaleSliderItem          = @"canvas scale";
NSString* MISUGAR_CanvasZoomInItem               = @"canvas zoom in";
NSString* MISUGAR_CanvasZoomOutItem              = @"canvas zoom out";
NSString* MISUGAR_SchematicVariantDisplayerItem  = @"schematic variant";
NSString* MISUGAR_SchematicHomePositionItem      = @"schematic home position";
NSString* MISUGAR_FitToViewItem                  = @"fit to view";
NSString* MISUGAR_SubcircuitIndicatorItem        = @"subcircuit indicator";
NSString* SUGAR_FILE_TYPE                        = @"SugarFileType";

@interface CircuitDocument () <NSToolbarDelegate, NSToolbarItemValidation>
@end

@implementation CircuitDocument
{
  IBOutlet MI_TextView* inputView;
  IBOutlet MI_Window* window;
  IBOutlet NSSplitView* splitter; // for vertical layout
  IBOutlet NSSplitView* verticalSplitter; // for horizontal layout
  IBOutlet NSSplitView* horizontalSplitter; // for horizontal layout
  IBOutlet NSTextView* shellOutputView;
  NSDictionary<NSAttributedStringKey, id> *printAttributes, *commentAttributes, *defaultAttributes,
  *analysisCommandAttributes, *modelAttributes, *subcircuitAttributes;
  BOOL highlightingAttributesCreated;

  SpiceSimulator* _simulator;

  MI_CustomViewToolbarItem* run;
  MI_AnalysisButton* analysisProgressIndicator;
  NSToolbarItem* plot;
  NSToolbarItem* schematic2Netlist;
  NSToolbarItem* elementsPanelShortcut;
  NSToolbarItem* infoPanelShortcut;
  NSToolbarItem* variantDisplayer;
  MI_VariantSelectionView* variantSelectionViewer;
  MI_CustomViewToolbarItem* canvasScaler;
  MI_FitToViewButton* fitButton;
  MI_CustomViewToolbarItem* fitToView;
  NSToolbarItem *zoomIn, *zoomOut;
  NSToolbarItem* home;
  NSToolbarItem* subcircuitIndicator;
  CircuitDocumentModel* _model;
  BOOL textEdited; // indicates whether the netlist has been edited since the last save
  BOOL hasVerticalLayout;
  BOOL scaleHasChanged;

  // SCHEMATIC - RELATED *******************************
  IBOutlet MI_SchematicsCanvas* canvas;
  NSSlider* canvasScaleSlider;
}

- (instancetype) init
{
    if (self = [super init]) {
        // If an error occurs here, send a [self dealloc] message and return nil.
        _model = [[CircuitDocumentModel alloc] init];
        textEdited = NO;
        hasVerticalLayout = YES;
        printAttributes = nil;
        commentAttributes = nil;
        analysisCommandAttributes = nil;
        printAttributes = nil;
        modelAttributes = nil;
        subcircuitAttributes = nil;
        highlightingAttributesCreated = NO;
        _simulator = nil;
        run = nil;
        plot = nil;
        schematic2Netlist = nil;
        canvasScaler = nil;
        elementsPanelShortcut = nil;
        infoPanelShortcut = nil;
        variantDisplayer = nil;
        analysisProgressIndicator = nil;
        fitToView = nil;
        home = nil;
        subcircuitIndicator = nil;
        zoomIn = zoomOut = nil;
        scaleHasChanged = NO;
    }
    return self;
}

- (void) dealloc
{
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (NSString*) windowNibName
{
    /* Override returning the nib file name of the document
       If you need to use a subclass of NSWindowController or if your document
       supports multiple NSWindowControllers, you should remove this method and
       override -makeWindowControllers instead. */
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:MISUGAR_DOCUMENT_LAYOUT] isEqualToString:@"Vertical"])
        return @"CircuitDocument";
    else
        return @"CircuitDocument2";
}

- (void) windowControllerDidLoadNib:(NSWindowController*)aController
{
    // Note: The window is not visible yet. This method prepares it for display.
    // This method is called after the opened file is read.
    [super windowControllerDidLoadNib:aController];

    let myWindow = (MI_Window*)[[[self windowControllers] objectAtIndex:0] window];
    [myWindow setDropHandler:self];

    // Applying the window size from the last session
    [myWindow setFrameUsingName:MISUGAR_DOCUMENT_WINDOW_FRAME];

    let toolbar = [self _createToolbar];
    [myWindow setToolbar:toolbar];

    [self _setUpNetlistInputView];
    [self _setUpSimulationOutputView];
    [self _setUpSchematicCanvas];
    [self _setUpSplitViews];
    [self updateViews];

    let nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(setFont:) name:MISUGAR_FONT_CHANGE_NOTIFICATION object:nil];
    [nc addObserver:self selector:@selector(placementGuideVisibilityChanged:) name:MISUGAR_PLACEMENT_GUIDE_VISIBILITY_CHANGE_NOTIFICATION object:nil];
    [nc addObserver:self selector:@selector(canvasBackgroundChanged:) name:MISUGAR_CANVAS_BACKGROUND_CHANGE_NOTIFICATION object:nil];
    let userdefs = [NSUserDefaults standardUserDefaults];
    [shellOutputView setFont:
      [NSFont fontWithName:[userdefs objectForKey:MISUGAR_RAW_OUTPUT_VIEW_FONT_NAME]
                      size:[[userdefs objectForKey:MISUGAR_RAW_OUTPUT_VIEW_FONT_SIZE] floatValue]]];
}

- (CircuitDocumentModel*) model
{
    return _model;
}

- (void) setModel:(CircuitDocumentModel*)newModel
{
    _model = newModel;
    [self updateViews];
}

- (void) updateViews
{
    if ([_model source] != nil)
    {
        [inputView setString:[_model source]];
        [self highlightInput];
    }
    else
        [inputView setString:@""];
    
    [canvas setScale:[_model schematicScale]];
    [canvasScaleSlider setFloatValue:[_model schematicScale]];
    [canvas setViewportOffset:[_model schematicViewportOffset]];
    [canvas setNeedsDisplay:YES];
    
    // update schematic variant view
    [variantSelectionViewer setSelectedVariant:[_model activeSchematicVariant]];
}

/// Silences runtime warning
+ (BOOL) autosavesInPlace
{
    return YES;
}

// MARK: Setup

- (NSToolbar*) _createToolbar
{
  variantSelectionViewer = [[MI_VariantSelectionView alloc] init];
  [variantSelectionViewer setSelectedVariant:0];
  let toolbar = [[NSToolbar alloc] initWithIdentifier:@"SugarMainDocumentWindowToolbar"];
  [toolbar setAllowsUserCustomization:NO];
  [toolbar setAutosavesConfiguration:NO];
  [toolbar setDisplayMode:NSToolbarDisplayModeIconAndLabel];
  [toolbar setDelegate:self];
  return toolbar;
}

- (void) _setUpNetlistInputView
{
  [inputView setDropHandler:self];

  // text view settings
  [inputView setAllowsUndo:YES];
  [inputView setRichText:YES];
  [inputView setUsesFontPanel:NO];
  //[[inputView textContainer] setLineFragmentPadding:10.0f];

  let scrollView = [inputView enclosingScrollView];
  NSAssert(scrollView != nil, @"The netlist editor must have an enclosing scroll view.");
  [scrollView setVerticalScrollElasticity: NSScrollElasticityNone];
  [scrollView setHorizontalScrollElasticity: NSScrollElasticityNone];

  // no line wrapping in netlist editor
  [[inputView textContainer] setWidthTracksTextView:NO];
  [[inputView textContainer] setHeightTracksTextView:NO];
  [[inputView textContainer] setContainerSize:NSMakeSize(20000, 10000000)];
  [inputView setHorizontallyResizable:YES];
  [inputView setVerticallyResizable:YES];
  [inputView setMaxSize:NSMakeSize(100000, 100000)];
  [inputView setMinSize:[scrollView contentSize]];

  // font
  let userdefs = [NSUserDefaults standardUserDefaults];
  [inputView setFont:
      [NSFont fontWithName:[userdefs objectForKey:MISUGAR_SOURCE_VIEW_FONT_NAME]
                      size:[[userdefs objectForKey:MISUGAR_SOURCE_VIEW_FONT_SIZE] floatValue]]];
}

- (void) _setUpSimulationOutputView
{
  let userdefs = [NSUserDefaults standardUserDefaults];
  [shellOutputView setFont:
      [NSFont fontWithName:[userdefs objectForKey:MISUGAR_RAW_OUTPUT_VIEW_FONT_NAME]
                      size:[[userdefs objectForKey:MISUGAR_RAW_OUTPUT_VIEW_FONT_SIZE] floatValue]]];
}

- (void) _setUpSchematicCanvas
{
  //[canvasScaleAdjustment setMaxValue:MI_SCHEMATIC_CANVAS_MAX_SCALE];
  //[canvasScaleAdjustment setMinValue:MI_SCHEMATIC_CANVAS_MIN_SCALE];
  //[canvasScaleAdjustment setFloatValue:1.0f];

  let userdefs = [NSUserDefaults standardUserDefaults];
  let colorData = (NSData*)[userdefs objectForKey:MISUGAR_SCHEMATIC_CANVAS_BACKGROUND_COLOR];
  let color = (NSColor*)[NSKeyedUnarchiver unarchivedObjectOfClass:[NSColor class] fromData:colorData error:nil];
  [canvas setBackgroundColor:color];
  [canvas setFrameSize:NSMakeSize(1600, 1200)];
  [canvas setController:self];
}

- (void) _setUpSplitViews
{
  let userdefs = [NSUserDefaults standardUserDefaults];
  hasVerticalLayout = [[userdefs objectForKey:MISUGAR_DOCUMENT_LAYOUT] isEqualToString:@"Vertical"];
  if (hasVerticalLayout)
  {
    float netHeight = [splitter frame].size.height - 2 * [splitter dividerThickness];
    float fraction = [[userdefs objectForKey:MISUGAR_DOCUMENT_FRACTIONAL_SIZE_OF_CANVAS] floatValue];
    var tmpFrame = [canvas frame];
    tmpFrame.size.height = (fraction < 0.1f) ? 0.0f : netHeight * fraction;
    [canvas setFrame:tmpFrame];
    tmpFrame = [[[inputView enclosingScrollView] superview] frame];
    fraction = [[userdefs objectForKey:MISUGAR_DOCUMENT_FRACTIONAL_HEIGHT_OF_NETLIST_FIELD] floatValue];
    tmpFrame.size.height = (fraction < 0.1f) ? 0.0f : netHeight * fraction;
    [[[inputView enclosingScrollView] superview] setFrame:tmpFrame];
    tmpFrame = [[shellOutputView enclosingScrollView] frame];
    fraction = [[userdefs objectForKey:MISUGAR_DOCUMENT_FRACTIONAL_HEIGHT_OF_OUTPUT_FIELD] floatValue];
    tmpFrame.size.height = (fraction < 0.1f) ? 0.0f : netHeight * fraction;
    [[shellOutputView enclosingScrollView] setFrame:tmpFrame];
    [splitter adjustSubviews];
    [splitter setNeedsDisplay:YES];
  }
  else // horizontal layout
  {
    float netWidth = [verticalSplitter frame].size.width - [verticalSplitter dividerThickness];
    float netHeight = [horizontalSplitter frame].size.height - [horizontalSplitter dividerThickness];
    float fraction = [[userdefs objectForKey:MISUGAR_DOCUMENT_FRACTIONAL_SIZE_OF_CANVAS] floatValue];
    var tmpFrame = [canvas frame];
    tmpFrame.size.width = (fraction < 0.1f) ? 0.0f : netWidth * fraction;
    [canvas setFrame:tmpFrame];
    tmpFrame = [[[shellOutputView enclosingScrollView] superview] frame];
    tmpFrame.size.width = (fraction < 0.1f) ? netWidth : (netWidth - netWidth * fraction);
    [[[shellOutputView enclosingScrollView] superview] setFrame:tmpFrame];
    tmpFrame = [[[inputView enclosingScrollView] superview] frame];
    fraction = [[userdefs objectForKey:MISUGAR_DOCUMENT_FRACTIONAL_HEIGHT_OF_NETLIST_FIELD] floatValue];
    tmpFrame.size.height = (fraction < 0.1f) ? 0.0f : netHeight * fraction;
    [[[inputView enclosingScrollView] superview] setFrame:tmpFrame];
    tmpFrame = [[shellOutputView enclosingScrollView] frame];
    fraction = [[userdefs objectForKey:MISUGAR_DOCUMENT_FRACTIONAL_HEIGHT_OF_OUTPUT_FIELD] floatValue];
    tmpFrame.size.height = (fraction < 0.1f) ? 0.0f : netHeight * fraction;
    [[shellOutputView enclosingScrollView] setFrame:tmpFrame];
    [horizontalSplitter adjustSubviews];
    [verticalSplitter adjustSubviews];
    [horizontalSplitter setNeedsDisplay: YES];
    [verticalSplitter setNeedsDisplay: YES];
  }
}

// MARK: Persistence

NSString* const CircuitDocumentCodingKey_Version = @"Version";
NSString* const CircuitDocumentCodingKey_Model = @"MI-SUGAR Document Model";
NSString* const CircuitDocumentCodingKey_DeviceModels = @"MI-SUGAR Circuit Device Models";

- (NSFileWrapper*) fileWrapperOfType:(NSString *)typeName
                               error:(NSError *__autoreleasing  _Nullable *)outError
{
	NSData* fileContents = nil;
	if ([typeName isEqualToString:SUGAR_FILE_TYPE])
	{
        NSKeyedArchiver* archiver = [[NSKeyedArchiver alloc] initRequiringSecureCoding:YES];
        [_model setSchematicViewportOffset:[canvas viewportOffset]];
        [archiver encodeObject:[NSNumber numberWithInt:MISUGAR_DOCUMENT_VERSION] forKey:CircuitDocumentCodingKey_Version];
        [archiver encodeObject:_model forKey:CircuitDocumentCodingKey_Model];
        [archiver encodeObject:[_model circuitDeviceModels] forKey:CircuitDocumentCodingKey_DeviceModels];
        [archiver finishEncoding];
        fileContents = archiver.encodedData;
        [[_model schematic] markAsModified:NO];
	}
	return [[NSFileWrapper alloc] initRegularFileWithContents:fileContents];
}


- (BOOL) readFromFileWrapper:(NSFileWrapper *)fileWrapper
                      ofType:(NSString *)typeName
                       error:(NSError *__autoreleasing  _Nullable *)outError
{
    if (![typeName isEqualToString:SUGAR_FILE_TYPE]) {
        return NO;
    }
    let fileData = [fileWrapper regularFileContents];
    if (fileData == nil) {
        return NO;
    }

    CircuitDocumentModel* newModel = nil;
    int fileVersion;
    NSArray<MI_CircuitElementDeviceModel*>* deviceModels;
    @try
    {
        let unarchiver = [[NSKeyedUnarchiver alloc] initForReadingFromData:fileData error:outError];

        // Convert or purge deprecated classes
        [unarchiver setClass:[MI_MOSDeviceModel class]
                forClassName:@"MI_BSIM3_MOSDeviceModel"];
        [unarchiver setClass:[MI_MOSDeviceModel class]
                forClassName:@"MI_BSIM4_NMOSDeviceModel"];
        [unarchiver setClass:[MI_MOSDeviceModel class]
                forClassName:@"MI_BSIM4_PMOSDeviceModel"];

        fileVersion = [[unarchiver decodeObjectOfClass:[NSNumber class] forKey:CircuitDocumentCodingKey_Version] intValue];
        newModel = [unarchiver decodeObjectOfClass:[CircuitDocumentModel class] forKey:CircuitDocumentCodingKey_Model];
        deviceModels = [unarchiver decodeArrayOfObjectsOfClass:[MI_CircuitElementDeviceModel class] forKey:CircuitDocumentCodingKey_DeviceModels];
        [unarchiver finishDecoding];
    }
    @catch (id)
    {
        NSAlert* infoAlert = [[NSAlert alloc] init];
        infoAlert.alertStyle = NSAlertStyleWarning;
        infoAlert.messageText = @"Invalid or corrupt file.";
        infoAlert.informativeText = [NSString stringWithFormat:@"Could not load %@. "
            "It was probably created by a newer version of MI-SUGAR and contains objects unknown to this version. "
            "It may also have been corrupted or maybe it's not a MI-SUGAR file at all.",
            [fileWrapper filename]];
        [infoAlert addButtonWithTitle:@"OK"];
        [infoAlert runModal];
        return NO;
    }

    if (![newModel isKindOfClass:[CircuitDocumentModel class]])
    {
        NSAlert* infoAlert = [[NSAlert alloc] init];
        infoAlert.alertStyle = NSAlertStyleWarning;
        infoAlert.messageText = @"Invalid file format.";
        infoAlert.informativeText = @"This file is not in a format which MI-SUGAR understands.";
        [infoAlert addButtonWithTitle:@"OK"];
        [infoAlert runModal];
        return NO;
    }

    if (fileVersion > MISUGAR_DOCUMENT_VERSION)
    {
        NSAlert* infoAlert = [[NSAlert alloc] init];
        infoAlert.alertStyle = NSAlertStyleWarning;
        infoAlert.messageText = @"Encountered newer file version.";
        infoAlert.informativeText = [NSString stringWithFormat:@"%@ has been created by a newer version of MI-SUGAR. "
            "You may experience problems with this file.",
            [fileWrapper filename]];
        [infoAlert addButtonWithTitle:@"Abort"];
        [infoAlert addButtonWithTitle:@"Open Anyway"];
        if ([infoAlert runModal] == NSAlertFirstButtonReturn)
        {
            return NO;
        }
    }
    [self setModel:newModel];
    [_model setRawOutput:nil];

    // If the opened file includes new device models they are added to the local repository
    [[MI_DeviceModelManager sharedManager] importDeviceModels:deviceModels];

    return YES;
}


- (void) processDrop:(id <NSDraggingInfo>)sender
{
    // A file is dropped onto the document window
    NSString *pathToDroppedFile = [[[sender draggingPasteboard] propertyListForType:@"NSFilenamesPboardType"] objectAtIndex:0];
    if ([[pathToDroppedFile pathExtension] caseInsensitiveCompare:@"sugar"] == NSOrderedSame)
    {
        [self readFromURL:[NSURL fileURLWithPath:pathToDroppedFile] ofType:SUGAR_FILE_TYPE error:nil];
    }
}


// NSTextView delegate method
- (void) textDidChange:(NSNotification*)aNotification
{
    [_model setSource:[inputView string]];
    [self highlightInput];
    if (![run isEnabled]) [run setEnabled:YES];
    textEdited = YES;
}


- (BOOL) isDocumentEdited
{
    return [[_model schematic] hasBeenModified] || textEdited || scaleHasChanged;
}


- (IBAction) convertSchematicToNetlist:(id)sender
{
    NSString* myNetlist = [_model source];
    NSString *line, *lcline;
    NSMutableString* dotLines = [NSMutableString stringWithCapacity:100];
    NSUInteger lineStart = 0, nextLineStart, lineEnd;
    NSUInteger limit = [myNetlist length];
    
    if ([_model isKindOfClass:[MI_SubcircuitDocumentModel class]])
    {
        // This is a subcircuit - no analysis commands are used
        NSMutableString* newNetlist = [NSMutableString stringWithCapacity:100];
        // Preserve comment lines - lump them together and put on top
        if ([[_model source] length] > 0)
        {
            while (lineStart < limit)
            {
                [myNetlist getLineStart:&lineStart
                                    end:&nextLineStart
                            contentsEnd:&lineEnd
                               forRange:NSMakeRange(lineStart, 1)];
                line = [myNetlist substringWithRange:NSMakeRange(lineStart, lineEnd - lineStart)];
                lineStart = nextLineStart;
                if ([line hasPrefix:@"*"])
                    [newNetlist appendFormat:@"%@\n", line];
            }
        }
        if ([newNetlist length] == 0)
            [newNetlist appendFormat:@"* %@\n", [[NSDate date] description]];
        [newNetlist appendString:[MI_Converter schematicToNetlist:_model]];
        [_model setSource:[NSString stringWithString:newNetlist]];
    }
    else
    {
        // Update netlist without modifying the control lines
        NSMutableString* newContent = [NSMutableString stringWithCapacity:200];
        
        if ([[_model source] length] > 0)
        {
            // Preserve all control lines
            while (lineStart < limit)
            {
                [myNetlist getLineStart:&lineStart
                                    end:&nextLineStart
                            contentsEnd:&lineEnd
                               forRange:NSMakeRange(lineStart, 1)];
                line = [myNetlist substringWithRange:NSMakeRange(lineStart, lineEnd - lineStart)];
                if (lineStart == 0)
                    [newContent appendFormat:@"%@\n", line];
                lineStart = nextLineStart;

                lcline = [line lowercaseString];
                if ([lcline hasPrefix:@".op"] ||
                    [lcline hasPrefix:@".dc"] ||
                    [lcline hasPrefix:@".nodeset"] ||
                    [lcline hasPrefix:@".ic"] ||
                    [lcline hasPrefix:@".tf"] ||
                    [lcline hasPrefix:@".ac"] ||
                    [lcline hasPrefix:@".disto"] ||
                    [lcline hasPrefix:@".noise"] ||
                    [lcline hasPrefix:@".tran"] ||
                    [lcline hasPrefix:@".pz"] ||
                    [lcline hasPrefix:@".four"] ||
                    [lcline hasPrefix:@".print"] ||
                    [lcline hasPrefix:@".include"]
                    )
                    [dotLines appendFormat:@"%@\n", line];
            }
            [newContent appendString:dotLines];
        }
        else
            [newContent appendFormat:@"* %@\n", [[NSDate date] description]];

        [newContent appendString:[MI_Converter schematicToNetlist:_model]];
        [newContent appendString:@"\n.end\n"];
        [_model setSource:newContent];
    }
    [inputView setString:[_model source]];
    [self highlightInput];
    textEdited = YES;
}


- (IBAction) runSimulator:(id)sender
{
  if ([_simulator isRunning])
  {
    [self abortSimulation:nil];
  }
  else
  {
    // Clearing the output view
    [shellOutputView setString:@""];

    let usingGnucap = NO;
    let defaults = [NSUserDefaults standardUserDefaults];
    let usingCustomSimulator = [[defaults objectForKey:MISUGAR_USE_CUSTOM_SIMULATOR] boolValue];

    // Determining the SPICE executable file
    NSString* simulatorPath = usingCustomSimulator ? (NSString*)[defaults objectForKey:MISUGAR_CUSTOM_SIMULATOR_PATH] : MISUGAR_BuiltinSPICEPath;

    let type = usingGnucap ? SpiceSimulatorTypeGnucap : (usingCustomSimulator ? SpiceSimulatorTypeNgspice : SpiceSimulatorTypeSpice);

    _simulator = [[SpiceSimulator alloc] initWithSimulatorType:type
                                                executablePath:simulatorPath];
    if (_simulator == nil) {
      if (usingCustomSimulator) {
        [self _showInformativeAlertWithTitle:@"Cannot launch simulator."
                                 description:[NSString stringWithFormat:@"Make sure the simulator executable file below exists and permissions are set.\n\n%@\n\nEdit the path in Settings.", simulatorPath]];
      }
      return;
    }

    if (!usingCustomSimulator && usingGnucap)
        _simulator.input = [_model gnucapFilteredSource];
    else
        _simulator.input = [_model spiceFilteredSource];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(simulationDidFinish:) name:SpiceSimulatorSimulationDidFinishNotification object:_simulator];

    // Start animating the progress indicator
    [NSThread detachNewThreadSelector:@selector(startAnimation)
                             toTarget:analysisProgressIndicator
                           withObject:nil];


    [_simulator launch];
  }
}


- (void) simulationDidFinish:(NSNotification*)notification
{
  if (_simulator != [notification object]) {
    return;
  }
  let output = (NSString*)[[notification userInfo] objectForKey:@"Output"];

  [[NSNotificationCenter defaultCenter] removeObserver:self name:SpiceSimulatorSimulationDidFinishNotification object:_simulator];

  [shellOutputView replaceCharactersInRange:NSMakeRange([[shellOutputView textStorage] length], 0)
                                 withString:output];

  [analysisProgressIndicator stopAnimation];
  [shellOutputView setString:output];
  [_model setOutput:output];
  [plot setEnabled:YES];
}


- (IBAction) abortSimulation:(id)sender
{
  [_simulator abort];

  // Stop animating the progress indicator
  [analysisProgressIndicator stopAnimation];
  [shellOutputView setString:@"Simulation aborted."];

  [plot setEnabled:NO];
}

- (void) _cleanUpSimulationInputFile
{
  NSString* inputFilePath = [NSString stringWithFormat:@"/tmp/%d.misugar.in", [[NSProcessInfo processInfo] processIdentifier]];
  [[NSFileManager defaultManager] removeItemAtPath:inputFilePath error:nil];
}


- (IBAction) plotResults:(id)sender
{
    SugarPlotter* plotter;
    NSUserDefaults* userdefs = [NSUserDefaults standardUserDefaults];
    int k;
#ifndef PLOT_CUSTOM_SIMULATOR_RESULTS
    if ([[userdefs objectForKey:MISUGAR_USE_CUSTOM_SIMULATOR] boolValue])
        return;
    else
#endif
    let modelOutput = [_model output];
    if (modelOutput == nil) {
      [self _showInformativeAlertWithTitle:@"Nothing to plot."
                               description:@"Open the plotter after adding an analysis command to the netlist and running the simulation."];
      return;
    }

    NSError* parsingError = nil;
    let analysisResults = [SpicePrintOutputParser parse:modelOutput error:&parsingError];
    if ([analysisResults count] == 0 && (parsingError != nil)) {
      [self _showInformativeAlertWithTitle:@"Analysis output parsing error" description:[parsingError localizedDescription]];
      return;
    }

    // Remove analyses with less than 2 points (operating points, for example)
    let filteredResults = [NSMutableArray<ResultsTable*> arrayWithCapacity:1];
    for (k = 0; k < (int)[analysisResults count]; k++)
    {
        if ([[[analysisResults objectAtIndex:k] variableAtIndex:0] numberOfValuesPerSet] > 1)
            [filteredResults addObject:[analysisResults objectAtIndex:k]];
    }

    if ( [filteredResults count] == 0 ) {
      [self _showInformativeAlertWithTitle:@"Nothing to plot."
                               description:@"The plotter needs at least two data points. For example, DC analysis results consist of a single value for which plotting makes no sense."];
      return;
    }

    // Create new SugarPlotter object with current results table array
    plotter = [[SugarPlotter alloc] initWithPlottingData:filteredResults];

    // Check settings to see if old plot windows must be removed
    if ([[userdefs objectForKey:MISUGAR_PLOTTER_CLOSES_OLD_WINDOW] boolValue])
    {
        let wcs = [self windowControllers];
        if ([wcs count] > 1)
        {
          for (long a = [wcs count] - 1; a > 0; --a)
          {
            [[[wcs objectAtIndex:a] window] performClose:self];
          }
        }
    }

#if 0 // plots should not be stored in the document - disabled since version 0.5.6
     NSWindowController* wcont = [[NSWindowController alloc] initWithWindow:[plotter window]];
     [self addWindowController:wcont];
     // The plotter is aware of this document window so we get notified
     // when the plot window is closed and can remove it from the window
     // controller list
     [plotter setWindowController:wcont];
#endif
}

- (void) _showInformativeAlertWithTitle:(NSString*)title
                            description:(NSString*)description
{
  NSAlert* alert = [[NSAlert alloc] init];
  alert.messageText = title;
  alert.informativeText = description;
  [alert addButtonWithTitle:@"OK"];
  NSWindow* hostWindow = [[[self windowControllers] objectAtIndex:0] window];
  [alert beginSheetModalForWindow:hostWindow completionHandler:nil];
}


- (void) setFont:(NSNotification*)notification
{
    NSUserDefaults* userdefs = [NSUserDefaults standardUserDefaults];
    if ([[notification object] boolValue])
    {
        // Set the font of the source view
        [inputView setFont:
            [NSFont fontWithName:[userdefs objectForKey:MISUGAR_SOURCE_VIEW_FONT_NAME]
                            size:[[userdefs objectForKey:MISUGAR_SOURCE_VIEW_FONT_SIZE] floatValue]]];
        [self highlightInput];
    }
    else
    {
        // Also set the font of the shell output view
        [shellOutputView setFont:
            [NSFont fontWithName:[userdefs objectForKey:MISUGAR_RAW_OUTPUT_VIEW_FONT_NAME]
                            size:[[userdefs objectForKey:MISUGAR_RAW_OUTPUT_VIEW_FONT_SIZE] floatValue]]];
    }
}


- (void) highlightInput
{
    NSString* tmp; // the input string
    NSString* line; // a single line
    NSUInteger lineStart = 0, nextLineStart, lineEnd;
    NSDictionary<NSAttributedStringKey, id>* previousAttributes = nil;
    if (!highlightingAttributesCreated)
    {
      //NSColor* darkGreen = [NSColor colorWithDeviceRed:0.0 green:0.4 blue:0.0 alpha:1.0];
      NSColor* darkRed = [NSColor colorWithDeviceRed:0.4 green:0.0 blue:0.0 alpha:1.0];
      NSColor* darkBlue = [NSColor colorWithDeviceRed:0.0 green:0.0 blue:0.4 alpha:1.0];
      NSColor* lightGrey = [NSColor colorWithDeviceRed:0.7 green:0.7 blue:0.7 alpha:1.0];
      NSColor* darkYellow = [NSColor colorWithDeviceRed:0.5 green:0.4 blue:0.0 alpha:1.0];
      NSColor* orange = [NSColor colorWithDeviceRed:0.81 green:0.45 blue:0.14 alpha:1.0];
      analysisCommandAttributes = @{ NSForegroundColorAttributeName: darkRed };
      commentAttributes = @{ NSForegroundColorAttributeName: lightGrey };
      modelAttributes = @{ NSForegroundColorAttributeName: darkYellow };
      printAttributes = @{ NSForegroundColorAttributeName: orange };
      subcircuitAttributes = @{ NSForegroundColorAttributeName: [NSColor blueColor] };
      defaultAttributes = @{ NSForegroundColorAttributeName: darkBlue };
      highlightingAttributesCreated = YES;
    }

    // Scan all lines
    tmp = [[inputView string] uppercaseString];
    [[inputView textStorage] beginEditing];
    while (lineStart < [tmp length])
    {
          [tmp getLineStart:&lineStart
                        end:&nextLineStart
                contentsEnd:&lineEnd
                   forRange:NSMakeRange(lineStart, 1)];
        line = [[tmp substringWithRange:NSMakeRange(lineStart, lineEnd - lineStart)]
            stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if ([line hasPrefix:@"*>"])
            line = [line substringFromIndex:2];
        
        if (lineStart == 0)
            [[inputView textStorage] setAttributes:commentAttributes
                                             range:NSMakeRange(lineStart, lineEnd - lineStart)];
        else if (([line hasPrefix:@".TRAN "] || [line isEqualToString:@".TRAN"]) ||
                 ([line hasPrefix:@".OP "] || [line isEqualToString:@".OP"]) ||
                 ([line hasPrefix:@".AC "] || [line isEqualToString:@".AC"]) ||
                 [line hasPrefix:@".NOISE "] ||
                 [line hasPrefix:@".DISTO "] ||
                 [line hasPrefix:@".FOUR "] ||
                 [line hasPrefix:@".DC "]  ||
                 [line hasPrefix:@".TF "] ||
                 [line hasPrefix:@".NODESET "] ||
                 [line hasPrefix:@".IC"] ||
                 [line hasPrefix:@".OPTIONS "])
        {
            [[inputView textStorage] setAttributes:analysisCommandAttributes
                                             range:NSMakeRange(lineStart, lineEnd - lineStart)];
            previousAttributes = analysisCommandAttributes;
        }
        else if ([line hasPrefix:@".MODEL"])
        {
            [[inputView textStorage] setAttributes:modelAttributes
                                             range:NSMakeRange(lineStart, lineEnd - lineStart)];
            previousAttributes = modelAttributes;
        }
        else if ([line hasPrefix:@".END"] || [line hasPrefix:@"*"])
        {
            [[inputView textStorage] setAttributes:commentAttributes
                                             range:NSMakeRange(lineStart, lineEnd - lineStart)];
        }
        else if ([line hasPrefix:@".PRINT"])
        {
            [[inputView textStorage] setAttributes:printAttributes
                                             range:NSMakeRange(lineStart, lineEnd - lineStart)];
            previousAttributes = printAttributes;
        }
        else if ([line hasPrefix:@".SUBCKT "])
        {
            [[inputView textStorage] setAttributes:subcircuitAttributes
                                             range:NSMakeRange(lineStart, lineEnd - lineStart)];
        }
        else if ([line hasPrefix:@"+"])
            [[inputView textStorage] setAttributes:previousAttributes
                                             range:NSMakeRange(lineStart, lineEnd - lineStart)];
        else
        {
            [[inputView textStorage] setAttributes:defaultAttributes
                                             range:NSMakeRange(lineStart, nextLineStart - lineStart)];
            previousAttributes = defaultAttributes;
        }
        lineStart = nextLineStart;
    }
    [[inputView textStorage] endEditing];
    [inputView setFont:
        [NSFont fontWithName:[[NSUserDefaults standardUserDefaults] objectForKey:MISUGAR_SOURCE_VIEW_FONT_NAME]
                        size:[[[NSUserDefaults standardUserDefaults] objectForKey:MISUGAR_SOURCE_VIEW_FONT_SIZE] floatValue]]];
}

// MARK: NSSplitViewDelegate conformance

- (CGFloat) splitView:(NSSplitView *)sender constrainMinCoordinate:(float)proposedMin ofSubviewAt:(int)offset
{
  if (hasVerticalLayout)
  {
    return 0.0;
  }
  if (sender == verticalSplitter)
  {
    return 50.0f;
  }
  else /* if (sender = horizontalSplitter) */
  {
    return 100.0f;
  }
}

- (CGFloat) splitView:(NSSplitView *)sender constrainMaxCoordinate:(float)proposedMax ofSubviewAt:(int)offset
{
  if (hasVerticalLayout)
  {
    return [sender frame].size.height;
  }
  if (sender == verticalSplitter)
  {
    return [sender bounds].size.width - 200.0f;
  }
  else /* if (sender == horizontalSplitter) */
  {
    return [sender bounds].size.height - 50.0f;
  }
}

- (BOOL) splitView:(NSSplitView*)sv canCollapseSubview:(NSView*)view
{
  return NO;
}


- (void) splitViewDidResizeSubviews:(NSNotification *)aNotification
{
  if ([aNotification object] == verticalSplitter && !hasVerticalLayout)
  {
    NSScrollView* s = [inputView enclosingScrollView];
    CGFloat newScrollerWidth = [verticalSplitter frame].size.width -
      [verticalSplitter dividerThickness] -
      [[inputView lineNumberingView] frame].size.width;
    if (![verticalSplitter isSubviewCollapsed:canvas])
    {
      newScrollerWidth -= [canvas frame].size.width;
    }
    [s setFrameSize:NSMakeSize(newScrollerWidth,[s frame].size.height)];
    [s setNeedsDisplay:YES];
  }
}

// MARK: NSToolbarDelegate conformance

- (NSArray<NSToolbarItemIdentifier>*) toolbarDefaultItemIdentifiers:(NSToolbar*)toolbar
{
    let toolbarLayout = [NSMutableArray<NSString*> arrayWithCapacity:10];
    [toolbarLayout addObject:MISUGAR_Schematic2NetlistItem];
    if (![_model isKindOfClass:[MI_SubcircuitDocumentModel class]])
    {
        [toolbarLayout addObject:MISUGAR_RunItem];
        [toolbarLayout addObject:MISUGAR_PlotItem];
    }
    [toolbarLayout addObject:MISUGAR_ElementsPanelItem];
    [toolbarLayout addObject:MISUGAR_InfoPanelItem];
    [toolbarLayout addObject:MISUGAR_SchematicVariantDisplayerItem];
    [toolbarLayout addObject:MISUGAR_CanvasZoomOutItem];
    [toolbarLayout addObject:MISUGAR_CanvasScaleSliderItem];
    [toolbarLayout addObject:MISUGAR_CanvasZoomInItem];
    [toolbarLayout addObject:MISUGAR_FitToViewItem];
//    [toolbarLayout addObject:MISUGAR_SchematicHomePositionItem];
    if ([_model isKindOfClass:[MI_SubcircuitDocumentModel class]])
    {
        [toolbarLayout addObject:NSToolbarFlexibleSpaceItemIdentifier];
        [toolbarLayout addObject:MISUGAR_SubcircuitIndicatorItem];
    }
    return toolbarLayout;
}

- (NSArray<NSToolbarItemIdentifier>*) toolbarAllowedItemIdentifiers:(NSToolbar*)toolbar
{
    return [self toolbarDefaultItemIdentifiers:toolbar];
}

- (NSToolbarItem *)toolbar:(NSToolbar*)toolbar
     itemForItemIdentifier:(NSString*)itemIdentifier
 willBeInsertedIntoToolbar:(BOOL)flag
{
  if ([itemIdentifier isEqualToString:MISUGAR_Schematic2NetlistItem])
  {
    if (schematic2Netlist == nil)
    {
      schematic2Netlist = [[NSToolbarItem alloc] initWithItemIdentifier:MISUGAR_Schematic2NetlistItem];
      [schematic2Netlist setLabel:@"Capture"];
      [schematic2Netlist setAction:@selector(convertSchematicToNetlist:)];
      [schematic2Netlist setTarget:self];
      [schematic2Netlist setToolTip:@"Convert Schematic to Netlist"];
      [schematic2Netlist setImage:[NSImage imageWithSystemSymbolName:@"arrow.down.square" accessibilityDescription:nil]];
    }
    return schematic2Netlist;
  }
  else if ([itemIdentifier isEqualToString:MISUGAR_RunItem])
  {
    if (run == nil)
    {
      run = [[MI_CustomViewToolbarItem alloc] initWithItemIdentifier:MISUGAR_RunItem];
      analysisProgressIndicator = [[MI_AnalysisButton alloc] initWithFrame:NSMakeRect(0, 0, 24, 24)];
      [analysisProgressIndicator setAction:@selector(runSimulator:)];
      [analysisProgressIndicator setTarget:self];
      [run setView:analysisProgressIndicator];
      [run setLabel:@"Analyze"];
      [run setToolTip:@"Analyze the Netlist"];
    }
    return run;
  }
  else if ([itemIdentifier isEqualToString:MISUGAR_PlotItem])
  {
    if (plot == nil)
    {
      plot = [[NSToolbarItem alloc] initWithItemIdentifier:MISUGAR_PlotItem];
      [plot setLabel:@"Plot"];
      [plot setAction:@selector(plotResults:)];
      [plot setTarget:self];
      [plot setToolTip:@"Plot Analysis Results"];
      [plot setImage:[NSImage imageWithSystemSymbolName:@"waveform.path.ecg.rectangle" accessibilityDescription:nil]];
    }
    return plot;
  }
  else if ([itemIdentifier isEqualToString:MISUGAR_SchematicVariantDisplayerItem])
  {
    if (variantDisplayer == nil)
    {
      variantDisplayer = [[NSToolbarItem alloc] initWithItemIdentifier:MISUGAR_SchematicVariantDisplayerItem];
      [variantDisplayer setLabel:@"Variants"];
      [variantDisplayer setToolTip:@"Press 1, 2, 3 or 4 to switch to variant. Press with the Command key to copy the current schematic."];
      [variantDisplayer setView:variantSelectionViewer];
      [variantDisplayer setTarget:self];
      [variantDisplayer setAction:@selector(switchToSchematicVariant:)];
      [variantSelectionViewer setTranslatesAutoresizingMaskIntoConstraints:NO];
      [variantSelectionViewer addConstraint:[variantSelectionViewer.widthAnchor constraintEqualToConstant:64.0]];
      [variantSelectionViewer addConstraint:[variantSelectionViewer.heightAnchor constraintGreaterThanOrEqualToConstant:20.0]];
    }
    return variantDisplayer;
  }
  else if ([itemIdentifier isEqualToString:MISUGAR_CanvasScaleSliderItem])
  {
    if (canvasScaler == nil)
    {
      canvasScaler = [[MI_CustomViewToolbarItem alloc] initWithItemIdentifier:MISUGAR_CanvasScaleSliderItem];
      [canvasScaler setLabel:@"Scale"];
      [canvasScaler setToolTip:@"set the view scale"];
      canvasScaleSlider = [[NSSlider alloc] init];
      [canvasScaleSlider setMaxValue:MI_SCHEMATIC_CANVAS_MAX_SCALE];
      [canvasScaleSlider setMinValue:MI_SCHEMATIC_CANVAS_MIN_SCALE];
      [canvasScaleSlider setAltIncrementValue:0.1];
      [canvasScaleSlider setDoubleValue:1.0];
      //[canvasScaleSlider setNumberOfTickMarks:5];
      //[canvasScaleSlider setTickMarkPosition:NSTickMarkAbove];
      [canvasScaleSlider setAction:@selector(setCanvasScale:)];
      [canvasScaleSlider setTarget:self];
      [canvasScaler setView:canvasScaleSlider];
      [canvasScaleSlider setTranslatesAutoresizingMaskIntoConstraints:NO];
      [canvasScaleSlider addConstraint:[canvasScaleSlider.widthAnchor constraintEqualToConstant:75.0]];
      [canvasScaleSlider addConstraint:[canvasScaleSlider.heightAnchor constraintEqualToConstant:20.0]];
    }
    return canvasScaler;
  }
  else if ([itemIdentifier isEqualToString:MISUGAR_CanvasZoomInItem])
  {
    if (zoomIn == nil)
    {
      zoomIn = [[NSToolbarItem alloc] initWithItemIdentifier:MISUGAR_CanvasZoomInItem];
      [zoomIn setLabel:@""];
      [zoomIn setAction:@selector(zoomInCanvas:)];
      [zoomIn setTarget:self];
      [zoomIn setToolTip:@"zoom in"];
      [zoomIn setImage:[NSImage imageWithSystemSymbolName:@"plus.magnifyingglass" accessibilityDescription:nil]];
    }
    return zoomIn;
  }
  else if ([itemIdentifier isEqualToString:MISUGAR_CanvasZoomOutItem])
  {
    if (zoomOut == nil)
    {
      zoomOut = [[NSToolbarItem alloc] initWithItemIdentifier:MISUGAR_CanvasZoomOutItem];
      [zoomOut setLabel:@""];
      [zoomOut setAction:@selector(zoomOutCanvas:)];
      [zoomOut setTarget:self];
      [zoomOut setToolTip:@"zoom out"];
      [zoomOut setImage:[NSImage imageWithSystemSymbolName:@"minus.magnifyingglass" accessibilityDescription:nil]];
    }
    return zoomOut;
  }
  else if ([itemIdentifier isEqualToString:MISUGAR_ElementsPanelItem])
  {
    if (elementsPanelShortcut == nil)
    {
      elementsPanelShortcut = [[NSToolbarItem alloc] initWithItemIdentifier:MISUGAR_ElementsPanelItem];
      [elementsPanelShortcut setLabel:@"Elements"];
      [elementsPanelShortcut setAction:@selector(showElementsPanel:)];
      [elementsPanelShortcut setTarget:[NSApp delegate]];
      [elementsPanelShortcut setToolTip:@"Show Elements"];
      [elementsPanelShortcut setImage:[NSImage imageWithSystemSymbolName:@"rectangle.grid.2x2" accessibilityDescription:nil]];
    }
    return elementsPanelShortcut;
  }
  else if ([itemIdentifier isEqualToString:MISUGAR_InfoPanelItem])
  {
    if (infoPanelShortcut == nil)
    {
      infoPanelShortcut = [[NSToolbarItem alloc] initWithItemIdentifier:MISUGAR_InfoPanelItem];
      [infoPanelShortcut setLabel:@"Info"];
      [infoPanelShortcut setAction:@selector(showInfoPanel:)];
      [infoPanelShortcut setTarget:[NSApp delegate]];
      [infoPanelShortcut setToolTip:@"Show Info"];
      [infoPanelShortcut setImage:[NSImage imageWithSystemSymbolName:@"info.circle" accessibilityDescription:nil]];
    }
    return infoPanelShortcut;
  }
  else if ([itemIdentifier isEqualToString:MISUGAR_SchematicHomePositionItem])
  {
    if (home == nil)
    {
      home = [[NSToolbarItem alloc] initWithItemIdentifier:MISUGAR_SchematicHomePositionItem];
      [home setLabel:@""];
      [home setAction:@selector(moveSchematicViewportToOrigin:)];
      [home setTarget:self];
      [home setToolTip:@"Home Position"];
      [home setImage:[NSImage imageWithSystemSymbolName:@"house" accessibilityDescription:nil]];
    }
    return home;
  }
  else if ([itemIdentifier isEqualToString:MISUGAR_FitToViewItem])
  {
    if (fitToView == nil)
    {
      fitToView = [[MI_CustomViewToolbarItem alloc] initWithItemIdentifier:MISUGAR_FitToViewItem];
      fitButton = [[MI_FitToViewButton alloc] initWithFrame:NSMakeRect(0, 0, 20, 20)];
      [fitButton setAction:@selector(fitSchematicToView:)];
      [fitButton setTarget:self];
      [fitToView setView:fitButton];
      [fitToView setLabel:@"Fit to View"];
      [fitToView setToolTip:@"scale and position the schematic so it fills the viewable area"];
    }
    return fitToView;
  }
  else if ([itemIdentifier isEqualToString:MISUGAR_SubcircuitIndicatorItem])
  {
    if (subcircuitIndicator == nil)
    {
      subcircuitIndicator = [[NSToolbarItem alloc] initWithItemIdentifier:MISUGAR_SubcircuitIndicatorItem];
      [subcircuitIndicator setLabel:(@"")];
      [subcircuitIndicator setTarget:[NSApp delegate]];
      [subcircuitIndicator setAction:@selector(goToSubcircuitsFolder:)];
      if ([_model isKindOfClass:[MI_SubcircuitDocumentModel class]])
      {
        [subcircuitIndicator setImage:[NSImage imageNamed:@"subcircuit_toolbar_item"]];
        [subcircuitIndicator setToolTip:@"This circuit is a subcircuit."];
      }
    }
    return subcircuitIndicator;
  }
  else
      return nil;
}

// MARK: NSToolbarItemValidation conformance

- (BOOL) validateToolbarItem:(NSToolbarItem *)theItem
{
    BOOL valid = NO;
    if ( theItem == schematic2Netlist ||
         theItem == infoPanelShortcut ||
         theItem == fitToView ||
         theItem == zoomIn ||
         theItem == zoomOut )
    {
        valid = [[_model schematic] numberOfElements] > 0;
        [canvasScaleSlider setEnabled:valid];
        [fitToView setEnabled:valid];
    }
    else if (theItem == run)
    {
        valid = [[inputView string] length] > 0;
        [run setEnabled:valid];
    }
    else if (theItem == plot)
        valid = (![analysisProgressIndicator isAnimating] && ([[_model output] length] > 0)
#ifndef PLOT_CUSTOM_SIMULATOR_RESULTS
                && ![[[NSUserDefaults standardUserDefaults] objectForKey:MISUGAR_USE_CUSTOM_SIMULATOR] boolValue]
#endif
            );
    else if ( theItem == subcircuitIndicator ||
         theItem == variantDisplayer )
        valid = YES;
    
    return valid;
}

// MARK: AppleScript support

- (id) analyze_scriptCommand:(NSScriptCommand*)command
{
    [self runSimulator:nil];
    return nil;
}

- (id) plot_scriptCommand:(NSScriptCommand*)command
{
    [self plotResults:nil];
    return nil;
}

- (id) export_scriptCommand:(NSScriptCommand*)command
{
    let argsDict = [command evaluatedArguments];
    [self export:(NSString*)[argsDict objectForKey:@"format"]
          toFile:(NSString*)[argsDict objectForKey:@"file"]];
    return nil;
}

- (void) export:(NSString*)format
         toFile:(NSString*)fileName
{
    let modelOutput = [_model output];
    if (modelOutput == nil) {
      return;
    }
    NSError* parsingError = nil;
    let results = [SpicePrintOutputParser parse:modelOutput error:&parsingError];
    if (([results count] == 0) && (parsingError != nil)) {
        NSLog(@"Error while parsing analysis results for exporting. %@", [parsingError localizedDescription]);
        return;
    }

    NSString* fileExtension = nil;
    if ([format isEqualToString:@"MathML"])
        fileExtension = @".mml";
    else if ([format isEqualToString:@"Matlab"])
        fileExtension = @".m";
    else if ([format isEqualToString:@"Tabular"])
        fileExtension = @".txt";

    for (int i = 0; i < (int)[results count]; i++)
    {
        let currentTable = [results objectAtIndex:i];
        
        NSString* output = nil;
        if ([format isEqualToString:@"MathML"])
            output = [MI_Converter resultsToMathML:currentTable];
        else if ([format isEqualToString:@"Matlab"])
            output = [MI_Converter resultsToMatlab:currentTable];
        else if ([format isEqualToString:@"Tabular"])
            output = [currentTable description];
        else
            return;
        
        if ([results count] > 1)
            fileName = [fileName stringByAppendingFormat:@"%d", i];
        fileName = [fileName stringByAppendingString:fileExtension];
        if (![output writeToFile:fileName atomically:YES encoding:NSASCIIStringEncoding error:nil])
        {
          NSLog(@"Error while writing to file \"%@\".", fileName);
        }
    }
}

- (void) makeSubcircuit
{
    [[MI_SubcircuitCreator sharedCreator]
        createSubcircuitForCircuitDocument:self];
}

- (BOOL) validateMenuItem:(NSMenuItem*) item
{
    if ([[item title] isEqualToString:MISUGAR_CAPTURE_ITEM] ||
        [[item title] isEqualToString:MISUGAR_SVG_EXPORT_ITEM] ||
        [[item title] isEqualToString:MISUGAR_MAKE_SUBCIRCUIT_ITEM]) // <- one can create subcircuits from existing ones
        return [[_model schematic] numberOfElements] > 0;
    else if ([[item title] isEqualToString:@"Save"])
        return [self isDocumentEdited];
    else if ( [[item title] isEqualToString:MISUGAR_MATHML_ITEM] ||
              [[item title] isEqualToString:MISUGAR_MATLAB_ITEM] ||
              [[item title] isEqualToString:MISUGAR_TABULAR_TEXT_ITEM])
        return ( [_model output] != nil ) &&
            ![_model isKindOfClass:[MI_SubcircuitDocumentModel class]];
    else if ( [[item title] isEqualToString:MISUGAR_PLOT_ITEM] )
        return ([_model output] != nil) &&
            ![_model isKindOfClass:[MI_SubcircuitDocumentModel class]];
    else if ([[item title] isEqualToString:MISUGAR_ANALYZE_ITEM])
        return ([[_model source] length] > 0) &&
            ![_model isKindOfClass:[MI_SubcircuitDocumentModel class]];
    else
        return [super validateMenuItem:item];
}

// MARK: Printing methods

- (void) printDocumentWithSettings:(NSDictionary<NSPrintInfoAttributeKey,id> *)printSettings
                    showPrintPanel:(BOOL)showPrintPanel
                          delegate:(id)delegate
                  didPrintSelector:(SEL)didPrintSelector
                       contextInfo:(void *)contextInfo
{
  [[MI_CircuitDocumentPrinter sharedPrinter] runPrintSheetForCircuit:self];
}


// MARK: NSWindowDelegate conformance

- (BOOL) windowShouldClose:(id)sender
{
    return YES;
}

- (void) windowWillClose:(NSNotification *)aNotification
{
    NSUserDefaults* userdefs = [NSUserDefaults standardUserDefaults];
    NSWindow* theWindow = [[[self windowControllers] objectAtIndex:0] window];
    float netHeight, netWidth, fraction;
    if (hasVerticalLayout)
    {
        netHeight = [splitter frame].size.height - 2 * [splitter dividerThickness];
        fraction = [canvas frame].size.height/netHeight;
        [userdefs setObject:[NSNumber numberWithFloat:fraction]
                    forKey:MISUGAR_DOCUMENT_FRACTIONAL_SIZE_OF_CANVAS];
    }
    else
    {
        netWidth = [verticalSplitter frame].size.width - [verticalSplitter dividerThickness];
        netHeight = [horizontalSplitter frame].size.height - [horizontalSplitter dividerThickness];
        fraction = [canvas frame].size.width/netWidth;
        [userdefs setObject:[NSNumber numberWithFloat:fraction]
                    forKey:MISUGAR_DOCUMENT_FRACTIONAL_SIZE_OF_CANVAS];
    }
    fraction = [[[inputView enclosingScrollView] superview] frame].size.height/netHeight;
    [userdefs setObject:[NSNumber numberWithFloat:fraction]
                 forKey:MISUGAR_DOCUMENT_FRACTIONAL_HEIGHT_OF_NETLIST_FIELD];
    fraction = [[shellOutputView enclosingScrollView] frame].size.height/netHeight;
    [userdefs setObject:[NSNumber numberWithFloat:fraction]
                 forKey:MISUGAR_DOCUMENT_FRACTIONAL_HEIGHT_OF_OUTPUT_FIELD];
    
    [theWindow saveFrameUsingName:MISUGAR_DOCUMENT_WINDOW_FRAME];
    /*if ([theWindow isMainWindow] || [theWindow isKeyWindow])*/
        [[MI_Inspector sharedInspector] inspectElement:nil];
}

- (NSUndoManager*) windowWillReturnUndoManager:(NSWindow *)sender
{
  return [self undoManager];
}

// MARK: Schematic variant

- (void) switchToSchematicVariant:(NSNumber*)variant
{
    [[self undoManager]
        registerUndoWithTarget:self
                      selector:@selector(switchToSchematicVariant:)
                        object:[NSNumber numberWithInt:[_model activeSchematicVariant]]];
    [[self undoManager] setActionName:@"Switch to Schematic Variant"];

    // Switch schematic
    [_model setActiveSchematicVariant:[variant intValue]];

    // Mark as modified so the switch counts as modifications
    [[_model schematic] markAsModified:YES];
    
    [variantSelectionViewer setSelectedVariant:[variant intValue]];
    [canvas setNeedsDisplay:YES];
}


- (void) copySchematic:(MI_CircuitSchematic*)schematic toVariant:(NSNumber*)variant
{
  [self _copySchematicToVariant:@[schematic, variant]];
}

// Because this action is undoable, it can have only one argument.
// That's why the schematic and variant are combined into an array.
// The array has two elements:
// 0: the MI_CircuitSchematic object that will be copied
// 1: the variant into which to copy
- (void) _copySchematicToVariant:(NSArray*)schematicAndVariant
{
    NSAssert([schematicAndVariant count] == 2, @"The argument array is expected to have 2 entries.");

    let currentVariant = [_model activeSchematicVariant];

    let pastedSchematic = (MI_CircuitSchematic*)[schematicAndVariant objectAtIndex:0];
    let targetVariant = [[schematicAndVariant objectAtIndex:1] intValue];
    [_model setActiveSchematicVariant:targetVariant];

    let existingSchematic = [_model schematic];

    [[self undoManager] registerUndoWithTarget:self
                                      selector:@selector(_copySchematicToVariant:)
                                        object:@[existingSchematic, [NSNumber numberWithInt:targetVariant]]];

    [[self undoManager] setActionName:@"Create schematic variant"];

    [_model setSchematic:[pastedSchematic copy]];

    // return to current variant
    [_model setActiveSchematicVariant:currentVariant];
    
    // visual feedback to the user
    [variantSelectionViewer flashVariant:targetVariant];
}

// MARK: Saving to file

- (IBAction) saveDocument:(id)sender
{
    [super saveDocument:sender];
    // If this is a subcircuit refresh the library 
    if ([_model isKindOfClass:[MI_SubcircuitDocumentModel class]])
        [[(SugarManager*)[NSApp delegate] subcircuitLibraryManager] refreshAll];
}

- (IBAction) saveDocumentAs:(id)sender
{
    [super saveDocumentAs:sender];
    // If this is a subcircuit refresh the library 
    if ([_model isKindOfClass:[MI_SubcircuitDocumentModel class]])
        [[(SugarManager*)[NSApp delegate] subcircuitLibraryManager] refreshAll];
}

- (NSTextView*) netlistEditor
{
  return inputView;
}

- (NSTextView*) analysisResultViewer
{
  return shellOutputView;
}

// MARK: Schematic related

- (MI_SchematicsCanvas*) canvas
{
  return canvas;
}

/// Called by the scale adjustment widget
- (IBAction) setCanvasScale:(id)sender
{
  float newScale = [sender floatValue];
  [self scaleShouldChange:[NSNumber numberWithFloat:newScale]];
}

/// Called by the canvas as a response to user interaction.
/// The actual canvas scaling is performed by this method.
- (void) scaleShouldChange:(NSNumber*)newScaleNumber
{
  let requestedScale = [newScaleNumber floatValue];
  let oldScale = [canvas scale];
  [canvas setScale:requestedScale];
  // The canvas object may have restrained the scale value when we set it.
  // That's why we ask the canvas for the actual applied scale value.
  let newScale = [canvas scale];
  [canvasScaleSlider setFloatValue:newScale];
  [_model setSchematicScale:newScale];
  scaleHasChanged = YES;
  [[self undoManager] registerUndoWithTarget:self
                                    selector:@selector(scaleShouldChange:)
                                      object:[NSNumber numberWithFloat:oldScale]];
}

- (IBAction) zoomInCanvas:(id)sender
{
  [self scaleShouldChange:[NSNumber numberWithFloat:[canvas scale] * 1.2f]];
}

- (IBAction) zoomOutCanvas:(id)sender
{
  [self scaleShouldChange:[NSNumber numberWithFloat:[canvas scale] / 1.2f]];
}

- (IBAction) showPlacementGuides:(id)sender
{
  canvas.showsGuides = ([sender state] == NSControlStateValueOn);
}

- (void) placementGuideVisibilityChanged:(NSNotification*)notif
{
  canvas.showsGuides = [[notif object] boolValue];
}

- (void) canvasBackgroundChanged:(NSNotification*)notif
{
  canvas.backgroundColor = [notif object];
}

- (IBAction) moveSchematicViewportToOrigin:(id)sender
{
  canvas.viewportOffset = NSZeroPoint;
  [canvas setNeedsDisplay:YES];
}

- (void) toggleDetailsView
{
  BOOL newState = ![[_model schematic] showsQuickInfo];
  [[_model schematic] setShowsQuickInfo:newState];
  [[_model schematic] setShowsNodeNumbers:newState];
  [canvas setNeedsDisplay:YES];
}

- (IBAction) fitSchematicToView:(id)sender
{
  NSRect const bbox = [[_model schematic] boundingBox];
  NSRect const viewRect = [canvas frame];
  NSPoint const center = NSMakePoint(
      -bbox.origin.x + -bbox.size.width/2.0f + viewRect.size.width/2.0f,
      -bbox.origin.y + -bbox.size.height/2.0f + viewRect.size.height/2.0f
  );
  canvas.viewportOffset = center;
  float newScale = 0.95f * fmin(viewRect.size.width / bbox.size.width,
                                viewRect.size.height / bbox.size.height);

  [self scaleShouldChange:[NSNumber numberWithFloat:newScale]];
}

- (void) createSchematicUndoPointForModificationType:(NSString*)type
{
    if ([[[self undoManager] undoActionName] isEqualToString:MI_SCHEMATIC_MOVE_CHANGE]
        && [type isEqualToString:MI_SCHEMATIC_MOVE_CHANGE])
        return;
    [[self undoManager]
        registerUndoWithTarget:self
                      selector:@selector(restoreSchematic:)
                        object:[NSKeyedArchiver archivedDataWithRootObject:[_model schematic] requiringSecureCoding:NO error:nil]];
    [[self undoManager] setActionName:type];
}

- (void) restoreSchematic:(NSData*)archivedSchematic
{
  if (![[self undoManager] canUndo])
    return;

  [[MI_Inspector sharedInspector] inspectElement:nil];

  // Set the undo of this undo - which is a redo
  [[self undoManager] registerUndoWithTarget:self
                                    selector:@selector(restoreSchematic:)
                                      object:[NSKeyedArchiver archivedDataWithRootObject:[_model schematic] requiringSecureCoding:NO error:nil]];
  @try
  {
    MI_CircuitSchematic* s = [NSKeyedUnarchiver unarchivedObjectOfClass:[MI_CircuitSchematic class] fromData:archivedSchematic error:nil];
    [_model setSchematic:s];
  }
  @catch (id)
  {
    NSLog(@"Undo to invalid state.");
  }
  @finally
  {
    //if ([s numberOfSelectedElements] == 1)
        //[[MI_Inspector sharedInspector] inspectElement:[s firstSelectedElement]];

    [canvas setNeedsDisplay:YES];
  }
}

@end

/**
*
*   Copyright Kai Özer, 2003-2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import "MI_SchematicElementChooser.h"

@interface MI_SchematicElementChooser (DragNDrop) <NSPasteboardItemDataProvider, NSDraggingSource>
@end


@implementation MI_SchematicElementChooser
{
@private
  NSArray<MI_SchematicElement*>* _schematicElementList; // fixed array of MI_SchematicElement objects
  MI_SchematicElement* _activeElement;
  NSMenu* _myContextMenu; // lists all elements
}

- (id)initWithFrame:(NSRect)frame
{
  if (self = [super initWithFrame:frame])
  {
    /* NSButton properties
    [self setImage:nil];
    [self setTitle:nil];
    [self setBezelStyle:NSRegularSquareBezelStyle];
    [self setButtonType:NSMomentaryChangeButton]; */

    _schematicElementList = nil;
    _activeElement = nil;
    _myContextMenu = nil;
  }
  return self;
}

- (void) drawRect:(NSRect)rect
{
  if (_activeElement != nil)
  {
    [_activeElement setPosition:NSMakePoint([self frame].size.width/2.0f, [self frame].size.height/2.0f)];
    [_activeElement draw];
  }
}

- (BOOL) acceptsFirstMouse:(NSEvent*)theEvent
{
  return YES; // for click-through behavior
}

- (void) mouseDragged:(NSEvent*)theEvent
{
  let p = [self convertPoint:[self frame].origin fromView:[self superview]];
  let size = _activeElement.size;
  let originX = p.x + ([self frame].size.width - size.width) / 2.0;
  let originY = p.y + ([self frame].size.height - size.height) / 2.0;
  [NSApp preventWindowOrdering];
  let pbItem = [[NSPasteboardItem alloc] init];
  [pbItem setDataProvider:self forTypes:@[MI_SchematicElementPboardType]];
  let draggingItem = [[NSDraggingItem alloc] initWithPasteboardWriter:pbItem];
  [draggingItem setDraggingFrame:NSMakeRect(originX, originY, size.width, size.height) contents: [_activeElement image]];
  [self beginDraggingSessionWithItems:@[draggingItem] event:theEvent source:self];
}

- (void) setSchematicElementList:(NSArray<MI_SchematicElement*>*)elements
{
  if (elements != nil && [elements count] > 0)
  {
    BOOL showsImageInMenu = ([elements count] <= 8);

    _myContextMenu = [[NSMenu alloc] initWithTitle:@""];
    _schematicElementList = elements;
    self.activeElement = [_schematicElementList firstObject];
    for (MI_SchematicElement* tmpElement in _schematicElementList)
    {
      [tmpElement setShowsLabel:NO]; // no annoying labels please

      let tmpItem = [[NSMenuItem alloc] initWithTitle:[tmpElement name]
                                               action:@selector(setActiveElementFromMenu:)
                                        keyEquivalent:@""];
      [tmpItem setTarget:self];
      [tmpItem setRepresentedObject:tmpElement];
      [tmpItem setImage:(showsImageInMenu ? [tmpElement image] : nil)];
      [_myContextMenu addItem:tmpItem];
      [_myContextMenu setAutoenablesItems:YES];
    }
    [self setMenu:_myContextMenu];
    [self setNeedsDisplay:YES];
  }
  else
  {
    _schematicElementList = nil;
    _activeElement = nil;
    _myContextMenu = nil;
  }
}


- (void) setSchematicElement:(MI_SchematicElement*)element
{
  [self setActiveElement:element];
  if (element == nil)
      return;
  _myContextMenu = nil;
  _schematicElementList = @[element];
  [element setShowsLabel:NO]; // no annoying labels please
}

- (MI_SchematicElement*) activeElement
{
  return _activeElement;
}

- (void) setActiveElement:(MI_SchematicElement*)element
{
  /* Note: No release & retain because we are
  only pointing to another element in the fixed-size list. */
  _activeElement = element;
  [self setToolTip: [_activeElement name]];
  [self setNeedsDisplay:YES];
}


- (void) setActiveElementFromMenu:(NSMenuItem*)menuItem
{
  self.activeElement = [menuItem representedObject];
}

@end


@implementation MI_SchematicElementChooser (DragNDrop)

- (void) pasteboard:(nullable NSPasteboard *)pasteboard item:(NSPasteboardItem *)item provideDataForType:(NSPasteboardType)type
{
    let copiedElement = (MI_SchematicElement*)[_activeElement mutableCopy];
    [copiedElement setShowsLabel:YES];
    NSError* archivingError = nil;
    let data = [NSKeyedArchiver archivedDataWithRootObject:copiedElement requiringSecureCoding:YES error:&archivingError];
    if (data != nil) {
      [pasteboard setData:data forType:type];
    }
#ifdef DEBUG
    else {
      NSLog(@"Failure to archive schematic element '%@' for drag operation.", copiedElement.fullyQualifiedName);
      if (archivingError != nil) {
        NSLog(@"%@", archivingError.localizedDescription);
      }
    }
#endif
}

- (NSDragOperation) draggingSession:(NSDraggingSession *)session sourceOperationMaskForDraggingContext:(NSDraggingContext)context
{
  return NSDragOperationGeneric;
}

@end

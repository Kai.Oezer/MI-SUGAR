/**
*
*   Copyright Kai Özer, 2003-2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import "MI_SchematicElement.h"
#import "MI_CircuitElementDeviceModel.h"

NS_ASSUME_NONNULL_BEGIN

/// Class of schematics elements that represents electrical circuit elements.
@interface MI_CircuitElement : MI_SchematicElement
    <NSSecureCoding, NSCopying, NSMutableCopying, MI_Inspectable>

/// The parameters of the circuit element.
///
/// The dictionary maps from parameter names to string representations of the parameter values.
@property NSMutableDictionary<NSString*,NSString*>* parameters;

/// - Returns: ``MI_DeviceModelTypeNone`` by default.
///
/// To be overriden by subclasses that represent devices with SPICE models.
- (MI_DeviceModelType) usedDeviceModelType;

- (instancetype) initWithSize:(NSSize)size;

@end

/// protocol for all elements that behave like a node, i.e., having no electrical porperties.
@protocol MI_ElectricallyTransparentElement
@end

/// protocol for all elements that represent electrical ground.
@protocol MI_ElectricallyGroundedElement
@end

NS_ASSUME_NONNULL_END

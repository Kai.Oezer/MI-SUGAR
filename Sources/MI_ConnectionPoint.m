/**
*
*   Copyright Kai Özer, 2003-2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/
#import "MI_ConnectionPoint.h"

// Redeclares `readonly` properties as `readwrite` to make them writable internally.
@interface MI_ConnectionPoint ()
@property (readwrite) NSString* name;
@property (readwrite) NSSize size;
@property (readwrite) MI_Direction preferredNodeNumberPlacement;
@end

@implementation MI_ConnectionPoint

- (instancetype) initWithPosition:(NSPoint)myRelativePos
                   size:(NSSize)theSize
                   name:(NSString*)myName
    nodeNumberPlacement:(MI_Direction)nodePlacement;
{
  if (self == [super init])
  {
    self.relativePosition = myRelativePos;
    self.size = theSize;
    self.name = [myName copy];
    self.preferredNodeNumberPlacement = nodePlacement;
  }
  return self;
}


- (instancetype) initWithPosition:(NSPoint)myRelativePos
                   size:(NSSize)theSize
                   name:(NSString*)myName
{
  return [self initWithPosition:myRelativePos
                           size:theSize
                           name:myName
            nodeNumberPlacement:MI_DirectionNone];
}

// MARK: NSSecureCoding

let ConnectionPointCodingKey_Name = @"Name";
let ConnectionPointCodingKey_Position = @"Position";
let ConnectionPointCodingKey_Size = @"Size";
let ConnectionPointCodingKey_NumberPlacement = @"NodeNumberPlacement";

- (id)initWithCoder:(NSCoder*)decoder
{
  if (self = [super init])
  {
    _name = [decoder decodeObjectOfClass:[NSString class] forKey:ConnectionPointCodingKey_Name];
    self.relativePosition = [decoder decodePointForKey:ConnectionPointCodingKey_Position];
    _size = [decoder decodeSizeForKey:ConnectionPointCodingKey_Size];
    _preferredNodeNumberPlacement = [decoder decodeIntegerForKey:ConnectionPointCodingKey_NumberPlacement];
  }
  return self;
}

- (void)encodeWithCoder:(NSCoder*)encoder
{
  [encoder encodeObject:_name forKey:ConnectionPointCodingKey_Name];
  [encoder encodePoint:self.relativePosition forKey:ConnectionPointCodingKey_Position];
  [encoder encodeSize:_size forKey:ConnectionPointCodingKey_Size];
  [encoder encodeInteger:_preferredNodeNumberPlacement forKey:ConnectionPointCodingKey_NumberPlacement];
}

+ (BOOL) supportsSecureCoding { return YES; }

// MARK: NSCopying

- (id) copyWithZone:(NSZone*) zone
{
    return [[[self class] allocWithZone:zone] initWithPosition:self.relativePosition
                                                          size:_size
                                                          name:_name
                                           nodeNumberPlacement:_preferredNodeNumberPlacement];
}


@end

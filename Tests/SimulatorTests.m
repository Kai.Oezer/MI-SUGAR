/**
*
*   Copyright Kai Özer, 2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/

#import <XCTest/XCTest.h>
#import "SpiceSimulator.h"
#import "SpicePrintOutputParser.h"
#import "ResultsTable.h"

@interface SimulatorTests : XCTestCase
@end

@implementation SimulatorTests
{
  XCTestExpectation* _expectation;
  SpiceSimulator* _spiceSimulator;
  NSString* _simulatorOutput;
}

- (void) setUp
{
  _spiceSimulator = [[SpiceSimulator alloc] initWithSimulatorType:SpiceSimulatorTypeNgspice
                                                   executablePath:@"/opt/homebrew/bin/ngspice"];
  _simulatorOutput = nil;
}

- (void) testSpiceSimulator
{
  XCTSkipIf(_spiceSimulator == nil);
  _spiceSimulator.input = simpleTransientAnalysisCircuit;
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(simulationDidFinish:)
                                               name:SpiceSimulatorSimulationDidFinishNotification
                                             object:_spiceSimulator];
	_expectation = [[XCTestExpectation alloc] initWithDescription:@"SPICE simulator finished successfully"];
	[_spiceSimulator launch];
	[self waitForExpectations:@[_expectation]];
  XCTSkipIf(_simulatorOutput == nil);
  NSError* parsingError = nil;
  let simulationResults = [SpicePrintOutputParser parse:_simulatorOutput error:&parsingError];
  XCTSkipIf((simulationResults == nil) || (parsingError != nil));
  XCTSkipIf([simulationResults count] == 0);
  XCTAssertEqual([simulationResults count], 1);
  let simulationResult = [simulationResults firstObject];
  XCTAssertTrue([[simulationResult circuitTitle] isEqualToString:@"rc with pulse input"]);
  XCTAssertEqual([[simulationResult variableAtIndex:0] numberOfValuesPerSet], 85);
}

- (void) simulationDidFinish:(NSNotification*)notification
{
  if ([notification object] == _spiceSimulator) {
    NSString* output = (NSString*)[notification.userInfo objectForKey:@"Output"];
    if ((output != nil) && ([output length] > 0)) {
      _simulatorOutput = output;
      [_expectation fulfill];
    }
  }
}

let simpleTransientAnalysisCircuit = @"RC with pulse input\n"
"VPulse1 1 0 PULSE(0 5 0 0 0 0.004 1)\n"
"C1 0 out 1u\n"
"R1 1 out 1k\n"
".tran 0.01 0.1\n"
".print tran v(out)\n"
".end\n";

let simpleACAnalysisCircuit = @"RC circuit with AC input\n"
"C1 0 out 1u\n"
"Vac1 1 0 AC 380 0\n"
"R1 1 out 1k\n"
".ac lin 50 100 10k\n"
".print ac v(out)\n"
".end\n";

let dualDCSweepCircuit = @"Dual DC sweep on BJT base\n"
"QNPN1 out base 2 DefaultBJT 1.0 OFF TEMP=27\n"
"Vin 3 0 DC 1\n"
"Rs 0 2 1k\n"
"Rd out 1 1k\n"
"Rb 3 base 220\n"
"Vcc 1 0 DC 12\n"
".DC Vin 0.5 2.0 0.02\n"
".DC Vin 0.0 7.0 0.2\n"
".PRINT DC v(out) v(base)\n"
".MODEL DefaultBJT NPN (\n"
"+ ISE = 0.            XTF = 1.0\n"
"+ CJS = 0.            VJS = 0.50000       PTF = 0.\n"
"+ MJS = 0.            EG  = 1.10000       AF  = 1.\n"
"+ ITF = 0.50000       VTF = 1.00000       BR  = 40.00000\n"
"+  IS = 1.6339e-14    VAF = 103.40529\n"
"+ VAR = 17.77498      IKF = 1.00000\n"
"+  NE = 1.31919       IKR = 1.00000       ISC =   3.6856e-13\n"
"+  NC = 1.10024       IRB = 4.3646e-05    NF  =   1.00531\n"
"+  NR = 1.00688       RBM = 1.0000e-02    RB  =  71.82988\n"
"+  RC = 0.42753       RE  = 3.0503e-03    MJE =   0.32339\n"
"+ MJC = 0.34700       VJE = 0.67373       VJC =   0.47372\n"
"+  TF = 9.693e-10     TR  = 380.00e-9     CJE =   2.6734e-11\n"
"+ CJC = 1.4040e-11    FC  = 0.95000      XCJC =   0.94518\n"
"+)\n"
"\n"
".end\n";

@end

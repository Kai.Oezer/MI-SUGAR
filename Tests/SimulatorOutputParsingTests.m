/**
*
*   Copyright Kai Özer, 2024
*
*   This file is part of MI-SUGAR.
*
*   MI-SUGAR is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   MI-SUGAR is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with MI-SUGAR; if not, write to the Free Software Foundation, Inc.,
*   51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*
*/

#import <Foundation/Foundation.h>
#import <XCTest/XCTest.h>
#import "SpicePrintOutputParser.h"
#import "ResultsTable.h"
#import "Utils.h"

@interface SimulatorOutputParsingTests : XCTestCase
@end

@implementation SimulatorOutputParsingTests

- (void) testParsingSpiceTransientAnalysisOutput
{
	let ngspiceOutput = [Utils contentsOfResourceFile:@"ngspice_output_tran.txt"];
	XCTSkipIf(ngspiceOutput == nil);
	NSError* parsingError = nil;
	let tables = [SpicePrintOutputParser parse:ngspiceOutput error:&parsingError];
	XCTSkipIf(parsingError != nil);
	XCTSkipIf(tables == nil);
	XCTSkipIf([tables count] == 0);
	XCTAssertEqual([tables count], 1);
	let table = [tables firstObject];
	XCTAssertTrue([table.circuitTitle isEqualToString:@"rc with pulse input"]);
	XCTAssertTrue([table.analysisTitle isEqualToString:@"Transient Analysis  Thu Dec 12 21:21:56  2024"]);
	XCTSkipIf(table.variables.count < 2);
	XCTAssertEqual(table.variables.count, 2);
	let timeVariable = [table variableAtIndex:0];
	XCTAssertFalse([timeVariable isComplex]);
	XCTAssertEqual([timeVariable numberOfValuesPerSet], 87);
	XCTAssertEqualWithAccuracy([timeVariable doubleValueAtIndex:27 forSet:0], 0.015480, 0.00000001);
	let voltageVariable = [table variableAtIndex:1];
	XCTAssertFalse([voltageVariable isComplex]);
	XCTAssertEqual([voltageVariable numberOfValuesPerSet], 87);
	XCTAssertEqualWithAccuracy([voltageVariable doubleValueAtIndex:27 forSet:0], 3.26, 0.00000001);
}

- (void) testParsingSpiceACAnalysisOutput
{
	let ngspiceOutput = [Utils contentsOfResourceFile:@"ngspice_output_ac.txt"];
	XCTSkipIf(ngspiceOutput == nil);
	NSError* parsingError = nil;
	let tables = [SpicePrintOutputParser parse:ngspiceOutput error:&parsingError];
	XCTSkipIf(parsingError != nil);
	XCTSkipIf(tables == nil);
	XCTSkipIf(tables.count == 0);
	XCTAssertEqual(tables.count, 1);
	let table = [tables firstObject];
	XCTAssertTrue([table.circuitTitle isEqualToString:@"rc circuit with ac input"]);
	XCTAssertTrue([table.analysisTitle isEqualToString:@"AC Analysis  Sat Dec 14 14:01:03  2024"]);
	XCTSkipIf(table.variables.count < 2);
	XCTAssertEqual(table.variables.count, 2);
	let frequencyVariable = [table variableAtIndex:0];
	XCTAssertFalse(frequencyVariable.isComplex);
	XCTAssertEqual(frequencyVariable.numberOfValuesPerSet, 50);
	XCTAssertEqualWithAccuracy([frequencyVariable doubleValueAtIndex:38 forSet:0], 7777.551, 0.0001);
	let voltageVariable = [table variableAtIndex:1];
	XCTAssertTrue([voltageVariable isComplex]);
	XCTAssertEqual([voltageVariable numberOfValuesPerSet], 50);
	voltageVariable.floatRepresentation = ComplexNumberFloatRepresentationReal;
	XCTAssertEqualWithAccuracy([voltageVariable doubleValueAtIndex:38 forSet:0], 0.1590583, 0.00000001);
	voltageVariable.floatRepresentation = ComplexNumberFloatRepresentationImaginary;
	XCTAssertEqualWithAccuracy([voltageVariable doubleValueAtIndex:38 forSet:0], -7.77283, 0.000001);
}

- (void) testParsingSpiceDualDCSweepOutput
{
	let ngspiceOutput = [Utils contentsOfResourceFile:@"ngspice_output_dual_dc_sweep.txt"];
	XCTSkipIf(ngspiceOutput == nil);
	NSError* parsingError = nil;
	let tables = [SpicePrintOutputParser parse:ngspiceOutput error:&parsingError];
	XCTSkipIf(parsingError != nil);
	XCTSkipIf(tables == nil);
	XCTSkipIf(tables.count == 0);
	XCTAssertEqual(tables.count, 2);

	let table1 = [tables firstObject];
	XCTAssertTrue([table1.circuitTitle isEqualToString:@"dual dc sweep on bjt base"]);
	XCTAssertTrue([table1.analysisTitle isEqualToString:@"DC transfer characteristic  Mon Dec 16 10:13:44  2024"]);
	XCTSkipIf(table1.variables.count < 2);
	XCTAssertEqual(table1.variables.count, 3);
	let table1SweepVoltageVariable = [table1 variableAtIndex:0];
	XCTAssertFalse(table1SweepVoltageVariable.isComplex);
	XCTAssertEqual(table1SweepVoltageVariable.numberOfValuesPerSet, 76);
	XCTAssertEqualWithAccuracy([table1SweepVoltageVariable doubleValueAtIndex:40 forSet:0], 1.3, 0.0000001);
	let table1OutputVoltageVariable = [table1 variableAtIndex:1];
	XCTAssertFalse([table1OutputVoltageVariable isComplex]);
	XCTAssertEqual([table1OutputVoltageVariable numberOfValuesPerSet], 76);
	XCTAssertEqualWithAccuracy([table1OutputVoltageVariable doubleValueAtIndex:40 forSet:0], 11.34137, 0.00000001);
	let table1BaseVoltageVariable = [table1 variableAtIndex:2];
	XCTAssertFalse([table1BaseVoltageVariable isComplex]);
	XCTAssertEqual([table1BaseVoltageVariable numberOfValuesPerSet], 76);
	XCTAssertEqualWithAccuracy([table1BaseVoltageVariable doubleValueAtIndex:40 forSet:0], 1.298634, 0.00000001);

	let table2 = [tables objectAtIndex:1];
	XCTAssertTrue([table2.circuitTitle isEqualToString:@"dual dc sweep on bjt base"]);
	XCTAssertTrue([table2.analysisTitle isEqualToString:@"DC transfer characteristic  Mon Dec 16 10:13:44  2024"]);
	XCTSkipIf(table2.variables.count < 2);
	XCTAssertEqual(table2.variables.count, 3);
	let table2SweepVoltageVariable = [table2 variableAtIndex:0];
	XCTAssertFalse(table2SweepVoltageVariable.isComplex);
	XCTAssertEqual(table2SweepVoltageVariable.numberOfValuesPerSet, 36);
	XCTAssertEqualWithAccuracy([table2SweepVoltageVariable doubleValueAtIndex:18 forSet:0], 3.6, 0.0000001);
	let table2OutputVoltageVariable = [table2 variableAtIndex:1];
	XCTAssertFalse([table2OutputVoltageVariable isComplex]);
	XCTAssertEqual([table2OutputVoltageVariable numberOfValuesPerSet], 36);
	XCTAssertEqualWithAccuracy([table2OutputVoltageVariable doubleValueAtIndex:18 forSet:0], 9.109122, 0.00000001);
	let table2BaseVoltageVariable = [table2 variableAtIndex:2];
	XCTAssertFalse([table2BaseVoltageVariable isComplex]);
	XCTAssertEqual([table2BaseVoltageVariable numberOfValuesPerSet], 36);
	XCTAssertEqualWithAccuracy([table2BaseVoltageVariable doubleValueAtIndex:18 forSet:0], 3.593719, 0.00000001);
}

@end
